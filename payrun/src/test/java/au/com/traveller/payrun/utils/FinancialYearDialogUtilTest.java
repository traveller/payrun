package au.com.traveller.payrun.utils;

import junit.framework.TestCase;

import au.com.traveller.payrun.enums.FinancialYear;
import au.com.traveller.payrun.repositories.FinancialYearRepo;
import au.com.traveller.payrun.repositories.FinancialYearRepoImpl;

public class FinancialYearDialogUtilTest extends TestCase {

    private FinancialYearRepo financialYearRepo;
    private String[] financialYears;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        financialYearRepo = new FinancialYearRepoImpl();
        financialYears = financialYearRepo.getSupportedYearsKeys();
    }

    @Override
    public void tearDown() throws Exception {
        financialYearRepo = null;
        financialYears = null;
        super.tearDown();
    }

    public void testFinYearCount() {
        // IF length FAILS, ADD MORE CHECK TO BELOW TESTS
        assertEquals(5, financialYears.length);
    }

    public void testIndexToFinYear() {
        assertEquals(FinancialYear.NONE, FinancialYearDialogUtil.INSTANCE.indexToFinYear(-1));
        assertEquals(FinancialYear.getByKey(financialYears[0]), FinancialYearDialogUtil.INSTANCE.indexToFinYear(0));
        assertEquals(FinancialYear.getByKey(financialYears[1]), FinancialYearDialogUtil.INSTANCE.indexToFinYear(1));
        assertEquals(FinancialYear.getByKey(financialYears[2]), FinancialYearDialogUtil.INSTANCE.indexToFinYear(2));
        assertEquals(FinancialYear.getByKey(financialYears[3]), FinancialYearDialogUtil.INSTANCE.indexToFinYear(3));
        assertEquals(FinancialYear.getByKey(financialYears[4]), FinancialYearDialogUtil.INSTANCE.indexToFinYear(4));
        assertEquals(FinancialYear.NONE, FinancialYearDialogUtil.INSTANCE.indexToFinYear(5));
    }

    public void testFinYearToIndex() {
        assertEquals(-1, FinancialYearDialogUtil.INSTANCE.finYearToIndex(FinancialYear.NONE));
        assertEquals(0, FinancialYearDialogUtil.INSTANCE.finYearToIndex(FinancialYear.getByKey(financialYears[0])));
        assertEquals(1, FinancialYearDialogUtil.INSTANCE.finYearToIndex(FinancialYear.getByKey(financialYears[1])));
        assertEquals(2, FinancialYearDialogUtil.INSTANCE.finYearToIndex(FinancialYear.getByKey(financialYears[2])));
        assertEquals(3, FinancialYearDialogUtil.INSTANCE.finYearToIndex(FinancialYear.getByKey(financialYears[3])));
        assertEquals(4, FinancialYearDialogUtil.INSTANCE.finYearToIndex(FinancialYear.getByKey(financialYears[4])));
    }
}