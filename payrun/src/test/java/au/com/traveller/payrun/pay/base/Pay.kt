package au.com.traveller.payrun.pay.base

enum class Pay {
    WITH_SUPER,
    WITHOUT_SUPER,
    AFTER_TAX,
    SUPER_CONTRIBUTION,
    PAYG,
    MEDICARE_LEVY,
    LOW_INCOME_TAX_OFFSET,
    TAX_TOTAL,
    INVOICE_GST,
    INVOICE_TOTAL
}