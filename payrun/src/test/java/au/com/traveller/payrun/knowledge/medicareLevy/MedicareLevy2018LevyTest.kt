package au.com.traveller.payrun.knowledge.medicareLevy

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.repositories.MedicareLevyRepoImpl
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal

class MedicareLevy2018LevyTest {
    @Test
    @Throws(Exception::class)
    fun getAnnualLevy() {
        val levy = MedicareLevyRepoImpl().getMedicareLevy(FinancialYear.FY_2017_18)

        // 0 .. 20896     => 0.00
        // 20896 .. 26121 => 0.01
        // 26121 .. +     => 0.02

        val tests = HashMap<String, String>()
        tests.put("1000.00", "0.00")
        tests.put("20896.99", "0.00")
        tests.put("20897.00", "0.01")
        tests.put("26121.00", "52.25")
        tests.put("26121.01", "522.42")
        tests.put("26121.49", "522.42")
        tests.put("26121.50", "522.43")
        tests.put("60000.00", "1200.00")

        tests.forEach {
            val message = "%s %s".format(it.key, it.value)
            Assert.assertEquals(
                    message,
                    normalise(BigDecimal(it.value)),
                    normalise(levy.getAnnualLevy(BigDecimal(it.key)))
            )
        }
    }

    @Test
    @Throws(Exception::class)
    fun getEmptyLevy() {
        val levy = MedicareLevyRepoImpl().getMedicareLevy(FinancialYear.NONE)

        // 0 .. +     => 0.00

        val tests = HashMap<String, String>()
        tests.put("0.00", "0.00")
        tests.put("1000.00", "0.00")
        tests.put("20896.99", "0.00")
        tests.put("20897.00", "0.00")
        tests.put("26121.00", "0.00")
        tests.put("26121.01", "0.00")
        tests.put("26121.49", "0.00")
        tests.put("26121.50", "0.00")
        tests.put("60000.00", "0.00")

        tests.forEach {
            val message = "%s %s".format(it.key, it.value)
            Assert.assertEquals(
                    message,
                    normalise(BigDecimal(it.value)),
                    normalise(levy.getAnnualLevy(BigDecimal(it.key)))
            )
        }
    }

    private fun normalise(x: BigDecimal): BigDecimal {
        return x.setScale(2, BigDecimal.ROUND_DOWN)
    }
}