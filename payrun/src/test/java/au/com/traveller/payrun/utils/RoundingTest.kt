package au.com.traveller.payrun.utils

import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

class RoundingTest {

    @Test
    fun roundHalfUp() {
        val before = BigDecimal("29.70")
        val after = Rounding.roundHalfUp(before)
        assertEquals("30", after.toString())
        assertEquals("30.00", after.setScale(2).toString())
    }
}