package au.com.traveller.payrun.pay.base

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.models.PayrunOutput
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

abstract class BasePayrunTest {

    abstract val financialYear: FinancialYear
    protected lateinit var payrunCalc: PayrunOutput
    protected val results = HashMap<Pay, BigDecimal>()

    @Before
    fun setUp() {
        initInput()
        initExpectedValues()
    }

    protected abstract fun initInput()

    protected abstract fun initExpectedValues()

    @Test
    @Throws(Exception::class)
    fun testGetPayrunWithSuper() {
        assertValue(Pay.WITH_SUPER, payrunCalc.incomePackage)
    }

    @Test
    @Throws(Exception::class)
    fun testGetPayrunTaxableIncome() {
        assertValue(Pay.WITHOUT_SUPER, payrunCalc.incomeBase)
    }

    @Test
    @Throws(Exception::class)
    fun testGetPayrunNetPay() {
        assertValue(Pay.AFTER_TAX, payrunCalc.netPay)
    }

    @Test
    @Throws(Exception::class)
    fun testGetPayrunSuperContribution() {
        assertValue(Pay.SUPER_CONTRIBUTION, payrunCalc.superContribution)
    }

    @Test
    @Throws(Exception::class)
    fun testGetPayrunPaygContribution() {
        assertValue(Pay.PAYG, payrunCalc.taxPaygContribution)
    }

    @Test
    @Throws(Exception::class)
    fun testGetPayrunMedicareLevy() {
        assertValue(Pay.MEDICARE_LEVY, payrunCalc.taxMedicareLevy)
    }

    @Test
    @Throws(Exception::class)
    fun testGetPayrunTotalTax() {
        assertValue(Pay.TAX_TOTAL, payrunCalc.taxTotal)
    }

    @Test
    @Throws(Exception::class)
    fun testGetPayrunInvoiceGst() {
        assertValue(Pay.INVOICE_GST, payrunCalc.gstAmount)
    }

    @Test
    @Throws(Exception::class)
    fun testGetPayrunInvoiceTotal() {
        assertValue(Pay.INVOICE_TOTAL, payrunCalc.invoiceAmountWithGst)
    }

    @Test
    fun testGetLowIncomeTaxOffset() {
        assertValue(Pay.LOW_INCOME_TAX_OFFSET, payrunCalc.lowIncomeTaxOffset)
    }

    /* ************** */
    /* HELPER METHODS */
    /* ************** */
    protected fun formatExpectedValue(value: Float): BigDecimal {
        return BigDecimal(value.toString()).setScale(2)
    }

    private fun assertValue(pay: Pay, actual: BigDecimal?) {
        val expectedValue: BigDecimal? = if (results.containsKey(pay)) results.getValue(pay) else null
        if (expectedValue != null) {
            val message = "[%s] %s".format(javaClass.simpleName, pay)
            Assert.assertEquals(message, expectedValue, actual!!.setScale(2, RoundingMode.HALF_UP))
        }
    }
}