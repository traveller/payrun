package au.com.traveller.payrun.pay.fy20182019

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseAnnually

private val finYear = FinancialYear.FY_2018_19

/**
 * 30,000 per annum (base)
 */
class Annually2019Base30kTest : BaseAnnually() {

    override val financialYear = finYear

    override fun initInput() {
        initInputs(0.095, 0.10, 30000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                32850.00f,
                30000.00f,
                27803.00f,
                2850.00f,
                2242.00f,
                600.00f,
                645.00f) //LITO=200.00 + LAMITO=445.00
    }
}

/**
 * 55,000 per annum (base)
 */
class Annually2019Base55kTest : BaseAnnually() {

    override val financialYear = finYear

    override fun initInput() {
        initInputs(0.095, 0.10, 55000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                60225f,
                55000f,
                45183.00f,
                5225f,
                9422f,
                1100f,
                705.00f) //LITO=175.00 + LAMITO=530.00
    }
}

/**
 * 90,000 per annum (base)
 */
class Annually2019Base90kTest : BaseAnnually() {

    override val financialYear = finYear

    override fun initInput() {
        initInputs(0.095, 0.10, 90000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                98550.00f,
                90000.00f,
                67933.00f,
                8550.00f,
                20797.00f,
                1800.00f,
                530.00f)
    }
}

/**
 * 100,000 per annum (base)
 */
class Annually2019Base100kTest : BaseAnnually() {

    override val financialYear = finYear

    override fun initInput() {
        initInputs(0.095, 0.10, 100000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                109500.00f,
                100000.00f,
                73883.00f,
                9500.00f,
                24497.00f,
                2000.00f,
                380.00f)
    }
}