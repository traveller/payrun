package au.com.traveller.payrun.policies

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.repositories.FinancialYearRepoImpl
import junit.framework.TestCase
import java.util.Calendar
import java.util.TimeZone
import kotlin.collections.ArrayList
import kotlin.collections.List

class PaymentInputPolicyTest : TestCase() {

    private val testCases: List<Pair<FinancialYear, Calendar>>
        get() {
            val repo = FinancialYearRepoImpl()
            val latestYear = repo.latestSupportedYear

            val cases = ArrayList<Pair<FinancialYear, Calendar>>()

            // IF THE FINANCIAL YEAR IS BEFORE EARLIEST SUPPORTED YEAR, RETURN LATEST SUPPORTED YEAR
            cases.add(Pair(latestYear, build(2014, 1)))
            cases.add(Pair(latestYear, build(2014, 6)))
            // FY_2014_15
            cases.add(Pair(FinancialYear.FY_2014_15, build(2014, 7)))
            cases.add(Pair(FinancialYear.FY_2014_15, build(2014, 12)))
            cases.add(Pair(FinancialYear.FY_2014_15, build(2015, 1)))
            cases.add(Pair(FinancialYear.FY_2014_15, build(2015, 6)))
            // FY_2015_16
            cases.add(Pair(FinancialYear.FY_2015_16, build(2015, 7)))
            cases.add(Pair(FinancialYear.FY_2015_16, build(2015, 12)))
            cases.add(Pair(FinancialYear.FY_2015_16, build(2016, 1)))
            cases.add(Pair(FinancialYear.FY_2015_16, build(2016, 6)))
            // FY_2016_17
            cases.add(Pair(FinancialYear.FY_2016_17, build(2016, 7)))
            cases.add(Pair(FinancialYear.FY_2016_17, build(2016, 12)))
            cases.add(Pair(FinancialYear.FY_2016_17, build(2017, 1)))
            cases.add(Pair(FinancialYear.FY_2016_17, build(2017, 6)))
            // FY_2017_18
            cases.add(Pair(FinancialYear.FY_2017_18, build(2017, 7)))
            cases.add(Pair(FinancialYear.FY_2017_18, build(2017, 12)))
            cases.add(Pair(FinancialYear.FY_2017_18, build(2018, 1)))
            cases.add(Pair(FinancialYear.FY_2017_18, build(2018, 6)))
            // FY_2018_19
            cases.add(Pair(FinancialYear.FY_2018_19, build(2018, 7)))
            cases.add(Pair(FinancialYear.FY_2018_19, build(2018, 12)))
            cases.add(Pair(FinancialYear.FY_2018_19, build(2019, 1)))
            cases.add(Pair(FinancialYear.FY_2018_19, build(2019, 6)))

            // IF THE FINANCIAL YEAR IS AFTER LATEST SUPPORTED YEAR, RETURN LATEST SUPPORTED YEAR
            cases.add(Pair(latestYear, build(2018, 7)))
            cases.add(Pair(latestYear, build(2019, 1)))
            cases.add(Pair(latestYear, build(2019, 6)))
            cases.add(Pair(latestYear, build(2019, 12)))

            return cases
        }

    fun testRunTestCases() {
        for ((finYear, calendar) in testCases) {
            TestCase.assertEquals(
                    String.format("%s/%s", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1),
                    finYear,
                    PaymentInputPolicy.getDefaultFinancialYear(calendar))
        }
    }

    private fun build(year: Int, month: Int): Calendar {
        val result = Calendar.getInstance(TimeZone.getDefault())
        result.set(Calendar.YEAR, year)
        result.set(Calendar.MONTH, month - 1)
        return result
    }
}
