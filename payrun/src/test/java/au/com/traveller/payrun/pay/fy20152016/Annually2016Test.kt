package au.com.traveller.payrun.pay.fy20152016

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseAnnually


/**
 * 55,000 per annum (base)
 */
class Annually2016Base55kTest : BaseAnnually() {

    override val financialYear = FinancialYear.FY_2015_16

    override fun initInput() {
        initInputs(0.095, 0.10, 55000.0)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                60225f,
                55000f,
                44653f,
                5225f,
                9422f,
                1100f,
                175.00f)
    }
}

/**
 * 80,000 per annum (base)
 */
class Annually2016Base80kTest : BaseAnnually() {

    override val financialYear = FinancialYear.FY_2015_16

    override fun initInput() {
        initInputs(0.095, 0.10, 80000.0)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                87600f,
                80000f,
                60853f,
                7600f,
                17547f,
                1600f)
    }
}

