package au.com.traveller.payrun.repositories.taxSchedule

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.TaxScheduleScales
import au.com.traveller.payrun.knowledge.taxScheduleScale.Fy20182019Scales
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class PaygWeeklyScheduleRepo2019Test {

    private lateinit var taxScales: TaxScheduleScales

    @Before
    fun setUp() {
        taxScales = Fy20182019Scales()
    }

    @Test
    fun testScale2MarginAbove3461() {
        val taxScales = taxScales.getSchedule()
        val margins = taxScales.taxScales[0].margins

        val margin0 = margins[0]
        assertEquals(margin0.columnA.toString(), "0.0000")
        assertEquals(margin0.columnB.toString(), "0.0000")

        val margin1 = margins[1]
        assertEquals(margin1.columnA.toString(), "0.1900")
        assertEquals(margin1.columnB.toString(), "67.4635")

        val margin2 = margins[2]
        assertEquals(margin2.columnA.toString(), "0.2900")
        assertEquals(margin2.columnB.toString(), "109.7327")

        val margin3 = margins[3]
        assertEquals(margin3.columnA.toString(), "0.2100")
        assertEquals(margin3.columnB.toString(), "67.4635")

        val margin4 = margins[4]
        assertEquals(margin4.columnA.toString(), "0.3477")
        assertEquals(margin4.columnB.toString(), "165.4423")

        val margin5 = margins[5]
        assertEquals(margin5.columnA.toString(), "0.3450")
        assertEquals(margin5.columnB.toString(), "161.9808")

        val margin6 = margins[6]
        assertEquals(margin6.columnA.toString(), "0.3900")
        assertEquals(margin6.columnB.toString(), "239.8654")

        val margin7 = margins[7]
        assertEquals(margin7.columnA.toString(), "0.4700")
        assertEquals(margin7.columnB.toString(), "516.7885")

    }
}