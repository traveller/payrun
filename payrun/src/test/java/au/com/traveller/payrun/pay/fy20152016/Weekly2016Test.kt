package au.com.traveller.payrun.pay.fy20152016

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseWeekly

class Weekly2016Daily550Test : BaseWeekly() {
    override val financialYear = FinancialYear.FY_2015_16

    override fun initInput() {
        initInputs(0.095, 0.10, 5, 550.0)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                2750.00f,
                2511.42f,
                1763.42f,
                238.58f,
                697.77f,
                50.23f,
                748.00f,
                275.00f,
                3025.00f)
    }
}

class Weekly2016Daily700Test : BaseWeekly() {
    override val financialYear = FinancialYear.FY_2015_16

    override fun initInput() {
        initInputs(0.095, 0.10, 5, 700.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                3500.00f,
                3196.35f,
                2180.35f,
                303.65f,
                952.07f,
                63.93f,
                1016.00f,
                350.00f,
                3850.00f)
    }
}