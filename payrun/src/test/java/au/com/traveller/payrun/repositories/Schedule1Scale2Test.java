package au.com.traveller.payrun.repositories;

import junit.framework.TestCase;

import java.math.BigDecimal;
import java.math.RoundingMode;

import au.com.traveller.payrun.enums.FinancialYear;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.knowledge.PaygWeeklySchedule;

public class Schedule1Scale2Test extends TestCase
{
    public void testInput563() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxForScaleFrequencyAndAmount(2, PaymentFrequency.WEEKLY, build(563));
        assertEquals(build(51), tax);
    }

    public void testInput1598() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxForScaleFrequencyAndAmount(2, PaymentFrequency.WEEKLY, build(1598));
        assertEquals(build(392), tax);
    }

    public void testInput2511() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxForScaleFrequencyAndAmount(2, PaymentFrequency.WEEKLY, build(2511));
        assertEquals(build(748), tax);
    }

    public void testInput2750() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxForScaleFrequencyAndAmount(2, PaymentFrequency.WEEKLY, build(2750));
        assertEquals(build(842), tax);
    }

    public void testInput3196() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxForScaleFrequencyAndAmount(2, PaymentFrequency.WEEKLY, build(3196));
        assertEquals(build(1016), tax);
    }

    public void testEmptyInput() throws Exception {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.NONE);
        BigDecimal tax = payg.getTaxForScaleFrequencyAndAmount(2, PaymentFrequency.WEEKLY, build(0));
        assertEquals(build(0), tax);
    }

    /* ************** */
    /* HELPER METHODS */
    /* ************** */
    private BigDecimal build(int value)
    {
        return new BigDecimal(value).setScale(0, RoundingMode.DOWN);
    }
}