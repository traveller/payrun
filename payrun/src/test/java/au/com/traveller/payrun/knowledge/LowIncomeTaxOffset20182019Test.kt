package au.com.traveller.payrun.knowledge

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.repositories.LowIncomeTaxOffsetRepoImpl
import au.com.traveller.payrun.utils.Rounding
import junit.framework.TestCase
import org.junit.Assert
import java.math.BigDecimal


class LowIncomeTaxOffset20182019Test : TestCase() {

    fun testRunTestCases() {
        val litoCalc: List<LowIncomeTaxOffset> = LowIncomeTaxOffsetRepoImpl().getIncomeOffset(FinancialYear.FY_2018_19)
        for ((expected, input) in getTestCases()) {
            Assert.assertEquals("%s (%s)".format(expected, input), Rounding.roundHalfUp(expected, 2), litoCalc[1].getAmount(input))
        }
    }

    private fun getTestCases(): List<Pair<BigDecimal, BigDecimal>> {
        val cases = mutableListOf<Pair<BigDecimal, BigDecimal>>()
        cases.add(Pair(BigDecimal(200), BigDecimal(30000)))
        cases.add(Pair(BigDecimal(200), BigDecimal(37000)))
        cases.add(Pair(BigDecimal(203), BigDecimal(37100)))
        cases.add(Pair(BigDecimal(527), BigDecimal(47900)))

        cases.add(Pair(BigDecimal(529.97), BigDecimal(47999)))
        cases.add(Pair(BigDecimal(529.98), BigDecimal(47999.50)))
        cases.add(Pair(BigDecimal(529.99), BigDecimal(47999.51)))
        cases.add(Pair(BigDecimal(530), BigDecimal(48000)))

        cases.add(Pair(BigDecimal(530), BigDecimal(48001)))
        cases.add(Pair(BigDecimal(530), BigDecimal(55000)))
        cases.add(Pair(BigDecimal(530), BigDecimal(71234)))
        cases.add(Pair(BigDecimal(530), BigDecimal(90000)))

        cases.add(Pair(BigDecimal(380), BigDecimal(100000)))

        cases.add(Pair(BigDecimal(5.00), BigDecimal("125000.00")))
        cases.add(Pair(BigDecimal(2.00), BigDecimal("125200.00")))
        cases.add(Pair(BigDecimal(0.50), BigDecimal("125300.00")))
        cases.add(Pair(BigDecimal(0.05), BigDecimal("125330.00")))
        cases.add(Pair(BigDecimal(0.02), BigDecimal("125332.00")))
        cases.add(Pair(BigDecimal(0.01), BigDecimal("125333.00")))
        cases.add(Pair(BigDecimal(0.00), BigDecimal("125333.01")))  //(35333 * 0.015 = 530.00 => NO MORE OFFSET)

        cases.add(Pair(BigDecimal(0.00), BigDecimal("125400.00")))

        return cases
    }
}