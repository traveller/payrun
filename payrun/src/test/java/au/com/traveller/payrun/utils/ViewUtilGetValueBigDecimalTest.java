package au.com.traveller.payrun.utils;

import junit.framework.TestCase;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

public class ViewUtilGetValueBigDecimalTest extends TestCase
{
    public void testRunTestCases() throws Exception
    {
        for (Map.Entry<String, BigDecimal> testCase : getTestCases().entrySet())
        {
            BigDecimal value = ViewUtil.getValueAsBigDecimal(testCase.getKey());
            assertEquals(testCase.getKey(), testCase.getValue(), value);
        }
    }

    private Map<String, BigDecimal> getTestCases()
    {
        Map<String, BigDecimal> cases = new HashMap<String, BigDecimal>();
        cases.put(        "100", build(100));
        cases.put(     "100.00", build(100));
        cases.put(       "1000", build(1000));
        cases.put(    "1000.00", build(1000));
        cases.put(      "$1000", build(1000));
        cases.put(  "$1,000.00", build(1000));
        cases.put(          "-", build(0));
        cases.put(         "$-", build(0));
        cases.put(         "-$", build(0));
        cases.put(     "-$1000", build(1000));
        cases.put("-$1,234,567", build(1234567));
        cases.put(           "", build(0));
        cases.put(         null, build(0));
        return cases;
    }

    private BigDecimal build(double val)
    {
        BigDecimal result = new BigDecimal(val);
        result = result.setScale(2, RoundingMode.HALF_UP);
        return result;
    }
}