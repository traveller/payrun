package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import junit.framework.TestCase
import java.math.BigDecimal
import java.math.RoundingMode


class AnnualPaygScheduleTest : TestCase() {

    @Throws(Exception::class)
    fun testEmptyInput() {
        val payg = PaygAnnualScheduleRepoImpl().getPaygSchedule(FinancialYear.NONE)
        val tax = payg.getTaxForAmount(build(0))
        TestCase.assertEquals(build(0), tax)
    }

    /* ************** */
    /* HELPER METHODS */
    /* ************** */
    private fun build(value: Int): BigDecimal {
        return BigDecimal(value).setScale(0, RoundingMode.DOWN)
    }
}