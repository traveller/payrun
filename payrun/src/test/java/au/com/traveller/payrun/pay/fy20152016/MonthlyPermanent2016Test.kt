package au.com.traveller.payrun.pay.fy20152016

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BasePermanentMonthly

/**
 * Monthly pay (55,000 per annum (base))
 */
class MonthlyPermanent2016Base55kTest : BasePermanentMonthly() {

    override val financialYear = FinancialYear.FY_2015_16

    override fun initInput() {
        initInputs(0.095, 0.10, 55000.0)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                5018.75f,
                4583.33f,
                3708.33f,
                435.42f,
                783.33f,        // PAY CALCULATOR SAYS 783.67f
                91.67f,
                875.00f,
                501.87f,
                5520.62f)
    }
}