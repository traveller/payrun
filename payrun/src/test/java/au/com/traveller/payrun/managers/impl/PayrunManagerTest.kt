package au.com.traveller.payrun.managers.impl

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.enums.PaymentFrequency
import org.junit.Assert
import org.junit.Test

class PayrunManagerTest {
    @Test
    @Throws(Exception::class)
    fun getPayrunCalculator() {
        val manager = PayrunKnowledgeManagerImpl()

        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2014_15, PaymentFrequency.WEEKLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2014_15, PaymentFrequency.FORTNIGHTLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2014_15, PaymentFrequency.QUARTERLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2014_15, PaymentFrequency.ANNUALLY))

        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2015_16, PaymentFrequency.WEEKLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2015_16, PaymentFrequency.FORTNIGHTLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2015_16, PaymentFrequency.QUARTERLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2015_16, PaymentFrequency.ANNUALLY))

        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2016_17, PaymentFrequency.WEEKLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2016_17, PaymentFrequency.FORTNIGHTLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2016_17, PaymentFrequency.QUARTERLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2016_17, PaymentFrequency.ANNUALLY))

        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2017_18, PaymentFrequency.WEEKLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2017_18, PaymentFrequency.FORTNIGHTLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2017_18, PaymentFrequency.QUARTERLY))
        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.FY_2017_18, PaymentFrequency.ANNUALLY))

        Assert.assertNotNull(manager.getPayrunCalculator(FinancialYear.NONE, PaymentFrequency.NONE))

    }

}