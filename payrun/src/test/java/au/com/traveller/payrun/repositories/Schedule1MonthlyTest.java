package au.com.traveller.payrun.repositories;

import junit.framework.TestCase;

import java.math.BigDecimal;
import java.math.RoundingMode;

import au.com.traveller.payrun.enums.FinancialYear;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.knowledge.PaygWeeklySchedule;

public class Schedule1MonthlyTest extends TestCase
{
    public void testInput6666p67() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxableIncome(PaymentFrequency.MONTHLY, build(6666.67));
        assertEquals(build(1538.99), tax);
    }

    public void testTotalTaxFor6666p67() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxForScaleFrequencyAndAmount(2, PaymentFrequency.MONTHLY, build(6666.67));
        assertEquals(build(1599.00), tax);
    }

    public void testInput5338p33() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxableIncome(PaymentFrequency.MONTHLY, build(5338.33));
        assertEquals(build(1231.99), tax);
    }

    public void testTotalTaxFor5338p33() throws Exception
    {
        PaygWeeklySchedule payg = new PaygWeeklyScheduleRepoImpl().getPaygTaxSchedule(FinancialYear.FY_2015_16);
        BigDecimal tax = payg.getTaxForScaleFrequencyAndAmount(2, PaymentFrequency.MONTHLY, build(5338.33));
        assertEquals(build(1140.00), tax);
    }

    /* ************** */
    /* HELPER METHODS */
    /* ************** */
    private BigDecimal build(double value)
    {
        return new BigDecimal(value).setScale(2, RoundingMode.FLOOR);
    }
}