package au.com.traveller.payrun.pay.fy20142015

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseWeekly

class Weekly2015Daily200Test : BaseWeekly() {
    override val financialYear = FinancialYear.FY_2014_15

    override fun initInput() {
        initInputs(0.090, 0.10, 5, 200.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                1000.00f,
                917.43f,
                763.43f,
                82.57f,
                135.65f,
                18.35f,
                154.00f,
                100.00f,
                1100.00f)
    }
}