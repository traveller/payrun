package au.com.traveller.payrun.knowledge

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.repositories.LowIncomeTaxOffsetRepoImpl
import au.com.traveller.payrun.utils.Rounding
import junit.framework.TestCase
import org.junit.Assert
import java.math.BigDecimal
import java.util.*


class LowIncomeTaxOffset20152016Test : TestCase() {

    @Throws(Exception::class)
    fun testRunTestCases() {
        val litoCalc: List<LowIncomeTaxOffset> = LowIncomeTaxOffsetRepoImpl().getIncomeOffset(FinancialYear.FY_2015_16)
        for ((expected, input) in getTestCases()) {
            Assert.assertEquals("%s (%s)".format(expected, input), Rounding.roundHalfUp(expected, 2), litoCalc[0].getAmount(input))
        }
    }

    private fun getTestCases(): List<Pair<BigDecimal, BigDecimal>> {
        val cases = mutableListOf<Pair<BigDecimal, BigDecimal>>()
        cases.add(Pair(BigDecimal(445), BigDecimal(10000)))
        cases.add(Pair(BigDecimal(400), BigDecimal(40000)))
        cases.add(Pair(BigDecimal(0), BigDecimal(66667)))
        return cases
    }
}