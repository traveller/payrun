package au.com.traveller.payrun.knowledge.payg

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.enums.PaymentFrequency
import au.com.traveller.payrun.repositories.PaygWeeklyScheduleRepoImpl
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal

class PaygWeeklySchedule2015IncomeTest {

    @Test
    @Throws(Exception::class)
    fun getTaxableIncomeWeekly() {
        val weekly = PaygWeeklyScheduleRepoImpl()
        val schedule = weekly.getPaygTaxSchedule(FinancialYear.FY_2017_18)

        val tests = HashMap<String, String>()
        tests["1000.00"] = "1000.99"
        tests["1000.01"] = "1000.99"
        tests["1000.49"] = "1000.99"
        tests["1000.50"] = "1000.99"
        tests["1000.51"] = "1000.99"
        tests["1000.98"] = "1000.99"
        tests["1000.99"] = "1000.99"

        tests.forEach {
            val message = "%s %s".format(it.key, it.value)
            Assert.assertEquals(
                    message,
                    it.value
                    ,
                    schedule.getTaxableIncome(PaymentFrequency.WEEKLY, BigDecimal(it.key).setScale(2, BigDecimal.ROUND_DOWN)).toPlainString()
            )
        }
    }

    @Test
    @Throws(Exception::class)
    fun getTaxableIncomeFortnightly() {
        val weekly = PaygWeeklyScheduleRepoImpl()
        val schedule = weekly.getPaygTaxSchedule(FinancialYear.FY_2017_18)

        val tests = HashMap<String, String>()
        tests["1000.00"] = "500.99"
        tests["1000.01"] = "500.99"
        tests["1000.49"] = "500.99"
        tests["1000.50"] = "500.99"
        tests["1000.51"] = "500.99"
        tests["1000.98"] = "500.99"
        tests["1000.99"] = "500.99"

        tests.forEach {
            val message = "%s %s".format(it.key, it.value)
            Assert.assertEquals(
                    message,
                    it.value
                    ,
                    schedule.getTaxableIncome(PaymentFrequency.FORTNIGHTLY,
                            BigDecimal(it.key).setScale(2, BigDecimal.ROUND_DOWN)).toPlainString()
            )
        }
    }

    @Test
    @Throws(Exception::class)
    fun getTaxableIncomeMonthly() {
        val weekly = PaygWeeklyScheduleRepoImpl()
        val schedule = weekly.getPaygTaxSchedule(FinancialYear.FY_2017_18)

        val tests = HashMap<String, String>()
        tests["1000.00"] = "230.99"
        tests["1000.01"] = "230.99"
        tests["1000.33"] = "230.99"
        tests["1000.49"] = "230.99"
        tests["1000.50"] = "230.99"
        tests["1000.51"] = "230.99"
        tests["1000.97"] = "230.99"
        tests["1000.98"] = "231.99"
        tests["1000.99"] = "231.99"

        tests.forEach {
            val message = "%s %s".format(it.key, it.value)
            Assert.assertEquals(
                    message,
                    it.value
                    ,
                    schedule.getTaxableIncome(PaymentFrequency.MONTHLY,
                            BigDecimal(it.key).setScale(2, BigDecimal.ROUND_DOWN)).toPlainString()
            )
        }
    }

    @Test
    @Throws(Exception::class)
    fun getTaxableIncomeQuarterly() {
        val weekly = PaygWeeklyScheduleRepoImpl()
        val schedule = weekly.getPaygTaxSchedule(FinancialYear.FY_2017_18)

        val tests = HashMap<String, String>()
        tests["1000.00"] = "76.99"
        tests["1000.01"] = "76.99"
        tests["1000.32"] = "76.99"
        tests["1000.33"] = "76.99"
        tests["1000.34"] = "76.99"
        tests["1000.49"] = "76.99"
        tests["1000.50"] = "76.99"
        tests["1000.51"] = "76.99"
        tests["1000.93"] = "76.99"
        tests["1000.94"] = "77.99"
        tests["1000.97"] = "77.99"
        tests["1000.98"] = "77.99"
        tests["1000.99"] = "77.99"

        tests.forEach {
            val message = "%s %s".format(it.key, it.value)
            Assert.assertEquals(
                    message,
                    it.value
                    ,
                    schedule.getTaxableIncome(PaymentFrequency.QUARTERLY,
                            BigDecimal(it.key).setScale(2, BigDecimal.ROUND_DOWN)).toPlainString()
            )
        }
    }

    @Test
    @Throws(Exception::class)
    fun getTaxableIncomeAnnually() {
        val weekly = PaygWeeklyScheduleRepoImpl()
        val schedule = weekly.getPaygTaxSchedule(FinancialYear.FY_2017_18)

        val tests = HashMap<String, String>()
        tests["1000.00"] = "1000.00"
        tests["1000.01"] = "1000.01"
        tests["1000.32"] = "1000.32"
        tests["1000.33"] = "1000.33"
        tests["1000.34"] = "1000.34"
        tests["1000.49"] = "1000.49"
        tests["1000.50"] = "1000.50"
        tests["1000.51"] = "1000.51"
        tests["1000.93"] = "1000.93"
        tests["1000.94"] = "1000.94"
        tests["1000.97"] = "1000.97"
        tests["1000.98"] = "1000.98"
        tests["1000.99"] = "1000.99"

        tests.forEach {
            val message = "%s %s".format(it.key, it.value)
            Assert.assertEquals(
                    message,
                    it.value
                    ,
                    schedule.getTaxableIncome(PaymentFrequency.ANNUALLY,
                            BigDecimal(it.key).setScale(2, BigDecimal.ROUND_DOWN)).toPlainString()
            )
        }
    }
}