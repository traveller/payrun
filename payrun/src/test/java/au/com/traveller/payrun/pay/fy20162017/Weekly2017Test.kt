package au.com.traveller.payrun.pay.fy20162017

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseWeekly

class Weekly2017Test : BaseWeekly() {
    override val financialYear = FinancialYear.FY_2016_17

    override fun initInput() {
        initInputs(0.095, 0.10, 5, 200.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                1000.00f,
                913.24f,
                761.24f,
                86.76f,
                133.74f,
                18.26f,
                152.00f,
                100.00f,
                1100.00f)
    }
}

class Weekly2017_550Test : BaseWeekly() {
    override val financialYear = FinancialYear.FY_2016_17

    override fun initInput() {
        initInputs(0.095, 0.10, 5, 550.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                2750.00f,
                2511.42f,
                1769.42f,
                238.58f,
                691.77f,
                50.23f,
                742.00f,
                275.00f,
                3025.00f)
    }
}

class Weekly2017_700Test : BaseWeekly() {
    override val financialYear = FinancialYear.FY_2016_17

    override fun initInput() {
        initInputs(0.095, 0.10, 5, 700.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                3500.00f,
                3196.35f,
                2186.35f,
                303.65f,
                946.07f,
                63.93f,
                1010.00f,
                350.00f,
                3850.00f)
    }
}

/**
 * 1,000.00/day (Taxable Income $228,310.50)
 */
class Weekly2017_1000Test : BaseWeekly() {
    override val financialYear = FinancialYear.FY_2016_17

    override fun initInput() {
        initInputs(0.095, 0.10, 5, 1000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                5000.00f,
                4566.21f,
                2912.21f,
                433.79f,
                1562.68f,
                91.32f,
                1654.00f,
                500.00f,
                5500.00f)
    }
}