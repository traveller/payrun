package au.com.traveller.payrun.pay.fy20172018

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BasePermanentMonthly

/**
 * Monthly pay for 60,000/annum (base)
 */
class MonthlyPermanent2018_60kTest : BasePermanentMonthly() {

    override val financialYear = FinancialYear.FY_2017_18

    override fun initInput() {
        initInputs(0.095, 0.10, 60000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                5475.00f,
                5000.00f,
                3977.00f,
                475.00f,
                923.00f,
                100.00f,
                1023.00f,
                547.50f,
                6022.50f)
    }
}