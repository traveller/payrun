package au.com.traveller.payrun.pay.fy20162017

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseAnnually

/**
 * 30,000 per annum (base)
 */
class Annually2017Base30kTest : BaseAnnually() {

    override val financialYear = FinancialYear.FY_2016_17

    override fun initInput() {
        initInputs(0.095, 0.10, 30000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                32850.00f,
                30000.00f,
                27603.00f,
                2850.00f,
                2242.00f,
                600.00f,
                445.00f)
    }
}

/**
 * 55,000 per annum (base)
 */
class Annually2017Base55kTest : BaseAnnually() {

    override val financialYear = FinancialYear.FY_2016_17

    override fun initInput() {
        initInputs(0.095, 0.10, 55000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                60225f,
                55000f,
                44653f,
                5225f,
                9422f,
                1100f,
                175.00f)
    }
}

/**
 * 90,000 per annum (base)
 */
class Annually2017Base90kTest : BaseAnnually() {

    override val financialYear = FinancialYear.FY_2016_17

    override fun initInput() {
        initInputs(0.095, 0.10, 90000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                98550.00f,
                90000.00f,
                67268.00f,
                8550.00f,
                20932.00f,
                1800.00f)
    }
}