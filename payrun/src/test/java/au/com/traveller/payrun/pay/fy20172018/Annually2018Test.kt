package au.com.traveller.payrun.pay.fy20172018

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseAnnually

private val finYear = FinancialYear.FY_2017_18

/**
 * 30,000 per annum (base)
 */
class Annually2018Base30kTest : BaseAnnually() {

    override val financialYear = finYear

    override fun initInput() {
        initInputs(0.095, 0.10, 30000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                32850.00f,
                30000.00f,
                27603.00f,
                2850.00f,
                2242.00f,
                600.00f,
                445.00f)
    }
}

/**
 * 55,000 per annum (base)
 */
class Annually2018Base55kTest : BaseAnnually() {

    override val financialYear = finYear

    override fun initInput() {
        initInputs(0.095, 0.10, 55000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                60225f,
                55000f,
                44653f,
                5225f,
                9422f,
                1100f,
                175.00f)
    }
}

/**
 * 90,000 per annum (base)
 */
class Annually2018Base90kTest : BaseAnnually() {

    override val financialYear = finYear

    override fun initInput() {
        initInputs(0.095, 0.10, 90000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                98550.00f,
                90000.00f,
                67268.00f,
                8550.00f,
                20932.00f,
                1800.00f)
    }
}

/**
 * 100,000 per annum (base)
 */
class Annually2018Base100kTest : BaseAnnually() {

    override val financialYear = finYear

    override fun initInput() {
        initInputs(0.095, 0.10, 100000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                109500.00f,
                100000.00f,
                73368.00f,  // FAIL: 73368.00
                9500.00f,
                24632.00f,
                2000.00f,
                0.00f)
    }
}