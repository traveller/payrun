package au.com.traveller.payrun.pay.fy20172018

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseQuarterly

/**
 * Quarterly for 60,000 per annum (base)
 */
class Quarterly2018_60kTest : BaseQuarterly() {

    override val financialYear = FinancialYear.FY_2017_18

    override fun initInput() {
        initInputs(0.095, 0.10, 60000.00)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                16425.00f,
                15000.00f,
                11932.00f,
                1425.00f,
                2768.00f,
                300.00f,
                3068.00f,
                1642.50f,
                18067.50f)
    }
}
