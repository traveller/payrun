package au.com.traveller.payrun.pay.fy20152016

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.pay.base.BaseMonthlyCasual

class MonthlyCasual2016Base6666Test : BaseMonthlyCasual() {

    override val financialYear = FinancialYear.FY_2015_16

    override fun initInput() {
        // 80000 (Annually) /12 = 6666.67 (Monthly)
        initInputs(0.095, 0.10, 20, 6666.67)
    }

    override fun initExpectedValues() {
        setExpectedValues(
                7300.00f,
                6666.67f,
                5067.67f,
                633.33f,
                1465.67f,
                133.33f,
                1599.00f,
                730.00f,
                8030.00f)
    }
}