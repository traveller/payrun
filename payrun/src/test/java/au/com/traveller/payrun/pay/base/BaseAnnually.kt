package au.com.traveller.payrun.pay.base

import au.com.traveller.payrun.enums.PaymentFrequency
import au.com.traveller.payrun.managers.PayrunManager
import au.com.traveller.payrun.models.PayrunInput
import java.math.BigDecimal

@Suppress
abstract class BaseAnnually : BasePayrunTest() {

    protected fun initInputs(superRate: Double, gstRate: Double, withoutSuper: Double) {
        val payrunInput = PayrunInput(financialYear, PaymentFrequency.ANNUALLY, BigDecimal(superRate), BigDecimal(gstRate), 2)
        payrunInput.setPermanentValues(false, BigDecimal(withoutSuper))

        val pm = PayrunManager()
        payrunCalc = pm.getPayrunOutput(payrunInput)
    }

    protected fun setExpectedValues(withSuper: Float, withoutSuper: Float, afterTax: Float, superContribution: Float, payg: Float, medicareLevy: Float, lowIncomeTaxOffset: Float = 0f) {
        results[Pay.WITH_SUPER] = formatExpectedValue(withSuper)
        results[Pay.WITHOUT_SUPER] = formatExpectedValue(withoutSuper)
        results[Pay.AFTER_TAX] = formatExpectedValue(afterTax)
        results[Pay.SUPER_CONTRIBUTION] = formatExpectedValue(superContribution)
        results[Pay.PAYG] = formatExpectedValue(payg)
        results[Pay.MEDICARE_LEVY] = formatExpectedValue(medicareLevy)
        results[Pay.LOW_INCOME_TAX_OFFSET] = formatExpectedValue(lowIncomeTaxOffset)
    }
}