package au.com.traveller.payrun.pay.base

import au.com.traveller.payrun.enums.PaymentFrequency
import au.com.traveller.payrun.managers.PayrunManager
import au.com.traveller.payrun.models.PayrunInput
import java.math.BigDecimal

@Suppress
abstract class BasePermanentMonthly : BasePayrunTest() {

    protected fun initInputs(superRate: Double, gstRate: Double, withoutSuper: Double) {
        val payrunInput = PayrunInput(financialYear, PaymentFrequency.MONTHLY, BigDecimal(superRate), BigDecimal(gstRate), 2)
        payrunInput.setPermanentValues(false, BigDecimal(withoutSuper))

        val pm = PayrunManager()
        payrunCalc = pm.getPayrunOutput(payrunInput)
    }

    protected fun setExpectedValues(withSuper: Float, withoutSuper: Float, afterTax: Float, superContribution: Float, payg: Float, medicareLevy: Float, taxTotal: Float, invoiceGst: Float, invoiceTotal: Float) {
        results[Pay.WITH_SUPER] = formatExpectedValue(withSuper)
        results[Pay.WITHOUT_SUPER] = formatExpectedValue(withoutSuper)
        results[Pay.AFTER_TAX] = formatExpectedValue(afterTax)
        results[Pay.SUPER_CONTRIBUTION] = formatExpectedValue(superContribution)
        results[Pay.PAYG] = formatExpectedValue(payg)
        results[Pay.MEDICARE_LEVY] = formatExpectedValue(medicareLevy)
        results[Pay.TAX_TOTAL] = formatExpectedValue(taxTotal)
        results[Pay.INVOICE_GST] = formatExpectedValue(invoiceGst)
        results[Pay.INVOICE_TOTAL] = formatExpectedValue(invoiceTotal)
    }
}