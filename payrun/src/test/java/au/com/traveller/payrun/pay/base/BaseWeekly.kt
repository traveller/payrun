package au.com.traveller.payrun.pay.base

import au.com.traveller.payrun.enums.PaymentFrequency
import au.com.traveller.payrun.managers.PayrunManager
import au.com.traveller.payrun.models.PayrunInput
import java.math.BigDecimal


abstract class BaseWeekly : BasePayrunTest() {

    protected fun initInputs(superRate: Double, gstRate: Double, days: Int, rateWithSuper: Double) {
        val payrunInput = PayrunInput(financialYear, PaymentFrequency.WEEKLY, BigDecimal(superRate), BigDecimal(gstRate), 2)
        payrunInput.setCasualValues(true, BigDecimal(days), BigDecimal(days).multiply(BigDecimal(rateWithSuper)))

        val pm = PayrunManager()
        payrunCalc = pm.getPayrunOutput(payrunInput)
    }

    protected fun setExpectedValues(withSuper: Float, withoutSuper: Float, afterTax: Float, superContribution: Float, payg: Float, medicareLevy: Float, taxTotal: Float, invoiceGst: Float, invoiceTotal: Float) {
        results[Pay.WITH_SUPER] = formatExpectedValue(withSuper)
        results[Pay.WITHOUT_SUPER] = formatExpectedValue(withoutSuper)
        results[Pay.AFTER_TAX] = formatExpectedValue(afterTax)
        results[Pay.SUPER_CONTRIBUTION] = formatExpectedValue(superContribution)
        results[Pay.PAYG] = formatExpectedValue(payg)
        results[Pay.MEDICARE_LEVY] = formatExpectedValue(medicareLevy)
        results[Pay.TAX_TOTAL] = formatExpectedValue(taxTotal)
        results[Pay.INVOICE_GST] = formatExpectedValue(invoiceGst)
        results[Pay.INVOICE_TOTAL] = formatExpectedValue(invoiceTotal)
    }
}