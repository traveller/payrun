package au.com.traveller.payrun.models;

import java.util.ArrayList;
import java.util.List;

public class PaymentInputs
{
    private List<PaymentInput> items;

    public PaymentInputs(List<PaymentInput> value)
    {
        this.items = value;
    }

    public List<PaymentInput> getItems()
    {
        return items;
    }

    public void addItem(PaymentInput value)
    {
        if (this.items == null)
        {
            this.items = new ArrayList<PaymentInput>();
        }

        this.items.add(value);
    }

    public void removeItem(PaymentInput item)
    {
        if (this.items != null)
        {
            this.items.remove(item);
        }
    }

    public void updateItem(PaymentInput updated)
    {
        int index = -1;

        for (int i=0; i < this.items.size(); i++)
        {
            PaymentInput item = this.items.get(i);

            if (item.getId().equals(updated.getId()))
            {
                index = i;
                break;
            }
        }

        if (index > -1 && index <= this.items.size()-1)
        {
            this.items.set(index, updated);
        }
    }
}
