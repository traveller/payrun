package au.com.traveller.payrun.enums;

public enum DashboardPageType
{
    NONE,
    FAVORITES,
    CASUAL,
    PERMANENT
}
