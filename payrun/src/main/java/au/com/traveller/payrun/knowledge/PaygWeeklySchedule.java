package au.com.traveller.payrun.knowledge;

import java.math.BigDecimal;

import au.com.traveller.payrun.enums.PaymentFrequency;

public interface PaygWeeklySchedule
{
    BigDecimal getTaxableIncome(PaymentFrequency frequency, BigDecimal taxableAmount);
    BigDecimal getTaxForScaleFrequencyAndAmount(int scaleNumber, PaymentFrequency frequency, BigDecimal amount);
}
