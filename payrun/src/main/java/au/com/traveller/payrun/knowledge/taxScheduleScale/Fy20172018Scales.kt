package au.com.traveller.payrun.knowledge.taxScheduleScale

import au.com.traveller.payrun.models.TaxScaleMargin


/**
 * Uses ATO for 2017/18
 * Rates changed for 2017/18 effective on 1st July 2017.
 */
class Fy20172018Scales : TaxScheduleScalesFactory() {

    override val scale2 = listOf(
            TaxScaleMargin(355.0f, 0.0000f, 0.0000f),
            TaxScaleMargin(416.0f, 0.1900f, 67.4635f),
            TaxScaleMargin(520.0f, 0.2900f, 109.1077f),
            TaxScaleMargin(711.0f, 0.2100f, 67.4646f),
            TaxScaleMargin(1282.0f, 0.3477f, 165.4435f),
            TaxScaleMargin(1673.0f, 0.3450f, 161.9819f),
            TaxScaleMargin(3461.0f, 0.3900f, 237.2704f),
            TaxScaleMargin(3461.0f, 0.4700f, 514.1935f)
    )

    override val scale6 = listOf(
            TaxScaleMargin(355.0f, 0.0000f, 0.0000f),
            TaxScaleMargin(702.0f, 0.1900f, 67.4635f),
            TaxScaleMargin(711.0f, 0.2400f, 102.5990f),
            TaxScaleMargin(878.0f, 0.3777f, 200.5779f),
            TaxScaleMargin(1282.0f, 0.3377f, 165.4425f),
            TaxScaleMargin(1673.0f, 0.3350f, 161.9810f),
            TaxScaleMargin(3461.0f, 0.3800f, 237.2694f),
            TaxScaleMargin(3461.0f, 0.4800f, 514.1925f)
    )

}