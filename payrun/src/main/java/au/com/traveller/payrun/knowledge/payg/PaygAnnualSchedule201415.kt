package au.com.traveller.payrun.knowledge.payg

import au.com.traveller.payrun.knowledge.PaygAnnualSchedule
import au.com.traveller.payrun.models.AnnualPaygSchedule
import org.jetbrains.anko.collections.forEachWithIndex
import java.math.BigDecimal


class PaygAnnualSchedule201415(private val taxSchedule: AnnualPaygSchedule) : PaygAnnualSchedule {

    override fun getTaxForAmount(amount: BigDecimal): BigDecimal {
        var netIncomeLeft = amount
        var taxOwed = BigDecimal.ZERO

        val thresholds = taxSchedule.thresholds.reversed()
        val rates = taxSchedule.rates.reversed()

        thresholds.forEachWithIndex { i, it ->
            if (netIncomeLeft > it) {
                taxOwed = taxOwed.add(netIncomeLeft.subtract(it).multiply(rates[i]))
                netIncomeLeft = it
            } else {
                return@forEachWithIndex
            }
        }

        return taxOwed
    }
}
