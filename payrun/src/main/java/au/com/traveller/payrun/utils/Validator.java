package au.com.traveller.payrun.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Validator
{
    private List<ValidationError> errors;

    public static Validator Build()
    {
        return new Validator();
    }

    private Validator()
    {
        this.errors = new ArrayList<ValidationError>();
    }

    public List<ValidationError> getErrors()
    {
        return errors;
    }

    public boolean hasErrors()
    {
        return errors.size() > 0;
    }

    public boolean isValid()
    {
        return errors.size() == 0;
    }

    public Validator notZero(BigDecimal value)
    {
        if (value != null)
        {
            if (value.compareTo(BigDecimal.ZERO) > 0)
            {
                return this;
            }
        }

        this.errors.add(new ValidationError());
        return this;
    }

    public Validator zeroOrMore(BigDecimal value)
    {
        if (value != null)
        {
            if (value.compareTo(BigDecimal.ZERO) >= 0)
            {
                return this;
            }
        }

        this.errors.add(new ValidationError());
        return this;
    }

    public Validator notEmpty(String value)
    {
        if (value != null)
        {
            if (value.trim().length() > 0)
            {
                return this;
            }
        }

        this.errors.add(new ValidationError());
        return this;
    }

    public Validator notNull(BigDecimal value)
    {
        if (value != null)
        {
            return this;
        }

        this.errors.add(new ValidationError());
        return this;
    }

    public Validator notEmptyNorValue(String value, String notAllowedValue)
    {
        if (value != null)
        {
            if (value.trim().length() > 0)
            {
                if (!value.equalsIgnoreCase(notAllowedValue))
                {
                    return this;
                }
            }
        }

        this.errors.add(new ValidationError());
        return this;
    }
}
