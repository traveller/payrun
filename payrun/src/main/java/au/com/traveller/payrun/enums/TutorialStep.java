package au.com.traveller.payrun.enums;

public enum TutorialStep
{
    NONE,
    TAB_CASUAL,
    TAB_PERMANENT,
    TAB_FAVOURITES,
    PAYMENT_EDIT,
    PAYMENT_FAVOURITE,
    PAYRUN_DETAILS,
    APP_SETTINGS
}
