package au.com.traveller.payrun.utils

import au.com.traveller.payrun.enums.FinancialYear

object FinancialYearDialogUtil {
    fun indexToFinYear(selectedIndex: Int): FinancialYear {
        return when (selectedIndex) {
            0 -> FinancialYear.FY_2014_15
            1 -> FinancialYear.FY_2015_16
            2 -> FinancialYear.FY_2016_17
            3 -> FinancialYear.FY_2017_18
            4 -> FinancialYear.FY_2018_19
            else -> FinancialYear.NONE
        }
    }

    fun finYearToIndex(selectedFinancialYear: FinancialYear): Int {
        return when (selectedFinancialYear) {
            FinancialYear.FY_2014_15 -> 0
            FinancialYear.FY_2015_16 -> 1
            FinancialYear.FY_2016_17 -> 2
            FinancialYear.FY_2017_18 -> 3
            FinancialYear.FY_2018_19 -> 4
            else -> -1
        }
    }
}
