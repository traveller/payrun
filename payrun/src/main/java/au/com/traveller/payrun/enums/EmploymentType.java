package au.com.traveller.payrun.enums;

public enum EmploymentType
{
    CASUAL(1),
    PERMANENT(2);

    private int id;

    EmploymentType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static EmploymentType getById(int id)
    {
        switch (id)
        {
            case 1:
                return CASUAL;
            case 2:
                return PERMANENT;
        }
        return null;
    }
}
