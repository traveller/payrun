package au.com.traveller.payrun.knowledge;

import java.math.BigDecimal;

public interface PaygAnnualSchedule
{
    BigDecimal getTaxForAmount(BigDecimal amount);
}
