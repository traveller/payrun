package au.com.traveller.payrun.models;

import java.math.BigDecimal;

public class AnnualPaygSchedule
{
    private BigDecimal[] thresholds;
    private BigDecimal[] rates;

    public AnnualPaygSchedule(BigDecimal[] thresholds, BigDecimal[] rates)
    {
        this.thresholds = thresholds;
        this.rates      = rates;
    }

    public BigDecimal[] getThresholds()
    {
        return this.thresholds;
    }

    public BigDecimal[] getRates()
    {
        return this.rates;
    }
}
