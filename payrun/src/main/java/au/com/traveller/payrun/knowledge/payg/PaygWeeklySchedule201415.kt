package au.com.traveller.payrun.knowledge.payg

import au.com.traveller.payrun.enums.PaymentFrequency
import au.com.traveller.payrun.knowledge.PaygWeeklySchedule
import au.com.traveller.payrun.models.TaxSchedule
import au.com.traveller.payrun.policies.KnowledgePaygPolicy
import au.com.traveller.payrun.utils.Rounding
import java.math.BigDecimal
import java.math.RoundingMode


class PaygWeeklySchedule201415(private val taxSchedule: TaxSchedule) : PaygWeeklySchedule {

    override fun getTaxableIncome(frequency: PaymentFrequency, taxableAmount: BigDecimal): BigDecimal {
        var taxablePortion: BigDecimal
        return when (frequency) {
            PaymentFrequency.WEEKLY ->
                // CENTS ADJUSTMENTS
                KnowledgePaygPolicy.ignoreCentsAndAdd99cents(taxableAmount)
            PaymentFrequency.FORTNIGHTLY -> {
                /*
                 * Divide the sum of the fortnightly earnings and the amount of any allowances subject to withholding by two.
                 * Ignore any cents in the result and then add 99 cents.
                 */
                // DIVIDE BY 2
                taxablePortion = taxableAmount.divide(BigDecimal(2), BigDecimal.ROUND_HALF_UP)

                // CENTS ADJUSTMENTS
                KnowledgePaygPolicy.ignoreCentsAndAdd99cents(taxablePortion)
            }
            PaymentFrequency.MONTHLY -> {
                // ONLY IF ###.33, ADD 0.01
                taxablePortion = taxableAmount
                if (taxablePortion.remainder(BigDecimal.ONE).compareTo(BigDecimal(0.33)) == 0) {
                    val correction = BigDecimal("0.01")
                    taxablePortion = taxablePortion.add(correction)
                }

                // MULTIPLY BY 3
                taxablePortion = taxablePortion.multiply(BigDecimal(3))

                // DIVIDE BY 13
                taxablePortion = taxablePortion.divide(BigDecimal(13), BigDecimal.ROUND_HALF_UP)

                // CENTS ADJUSTMENTS
                KnowledgePaygPolicy.ignoreCentsAndAdd99cents(taxablePortion)
            }
            PaymentFrequency.QUARTERLY -> {
                /*
                 * Divide the sum of the quarterly earnings and the amount of any allowances subject to withholding by 13.
                 * Ignore any cents in the result and then add 99 cents.
                 */
                // DIVIDE BY 13
                taxablePortion = taxableAmount.divide(BigDecimal(13), BigDecimal.ROUND_HALF_UP)
                // CENTS ADJUSTMENTS
                KnowledgePaygPolicy.ignoreCentsAndAdd99cents(taxablePortion)
            }
            else -> {
                // i.e. PaymentFrequency.ANNUALLY
                // FULL taxableAmount IS taxableAmount
                taxableAmount
            }
        }
    }

    /**
     * Gets Total Withholding Tax (PAY + MedicareLevy) for Schedule 1 Scale, Frequency and Taxable Amount
     *
     * @param scaleNumber   Currently only supports Scale 2
     * @param frequency     One of: Weekly, Fortnightly, Monthly, Quarterly, Annually
     * @param taxableAmount Amount after GST, Super or any Tax Offsets (total taxable amount)
     * @return BigDecimal Total Tax
     */
    override fun getTaxForScaleFrequencyAndAmount(scaleNumber: Int, frequency: PaymentFrequency, taxableAmount: BigDecimal): BigDecimal {
        var result = BigDecimal.ZERO
        val taxableIncome = getTaxableIncome(frequency, taxableAmount)
        val taxScale = KnowledgePaygPolicy.getScheduleScale(taxSchedule, scaleNumber)

        if (taxScale != null) {
            val payg = KnowledgePaygPolicy.getMarginForScale(taxScale, taxableIncome)
            val totalWithholding = payg.getTotalTaxInclMedicareLevy(taxableIncome)

            result = when (frequency) {
                PaymentFrequency.WEEKLY -> Rounding.roundHalfUp(totalWithholding)
                PaymentFrequency.FORTNIGHTLY ->
                    /*
                     * Work out the rounded weekly withholding amount applicable to the weekly equivalent of earnings,
                     * before any adjustment for tax offsets.
                     * Multiply this amount by 2.
                     */
                    totalWithholding.multiply(BigDecimal(2))
                PaymentFrequency.MONTHLY -> {
                    val tax = totalWithholding.multiply(BigDecimal(13)).divide(BigDecimal(3), RoundingMode.HALF_UP)
                    KnowledgePaygPolicy.roundToTheNearestDollar(tax)
                }
                PaymentFrequency.QUARTERLY ->
                    /*
                     * Work out the rounded weekly withholding amount applicable to the weekly equivalent of earnings,
                     * before any adjustment for tax offsets.
                     * Multiply this amount by 13.
                     */
                    totalWithholding.multiply(BigDecimal(13))
                else -> {
                    // PaymentFrequency.ANNUALLY or PaymentFrequency.NONE
                    BigDecimal.ZERO
                }
            }
        }

        return result
    }
}
