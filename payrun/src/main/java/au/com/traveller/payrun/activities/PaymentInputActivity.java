package au.com.traveller.payrun.activities;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.math.BigDecimal;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.dialogs.FinancialYearDialogFragment;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.FinancialYear;
import au.com.traveller.payrun.interfaces.FYDialogListener;
import au.com.traveller.payrun.models.PaymentInput;
import au.com.traveller.payrun.models.PaymentInputs;
import au.com.traveller.payrun.policies.PaymentInputPolicy;
import au.com.traveller.payrun.repositories.PaymentInputRepo;
import au.com.traveller.payrun.services.FabricAnswersService;
import au.com.traveller.payrun.utils.LocalStorageUtil;
import au.com.traveller.payrun.utils.ViewUtil;

public class PaymentInputActivity extends BaseActivity
{
    protected String LOG_TAG = PaymentInputActivity.class.getSimpleName();

    private static final String KEY_FINANCIAL_YEAR = "saved_financial_year";

    private PaymentInput _paymentInput;
    private int _employmentTypeId;

    private Switch _switchIsPermanent;
    private ViewGroup _rowDailyRate;
    private EditText _txtDailyRate;
    private ViewGroup _rowAnnualBase;
    private EditText _txtAnnualInput;
    private Switch _switchInclSuper;
    private EditText _txtSuperRate;
    private ViewGroup _rowGstRate;
    private EditText _txtGstRate;

    private TextView _lblFinYear;
    private FinancialYear _selectedFinancialYear;

    private Button _btnSave;
    private Button _btnDelete;

    private FYDialogListener _dialogListener = new FYDialogListener()
    {
        @Override
        public void onNewFinancialYear(FinancialYear newValue)
        {
            updateFinancialYear(newValue);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_input);

        this.initToolbar(R.string.title_activity_payment_input);

        if (getIntent() != null)
        {
            Intent intent = getIntent();
            String paymentInputId = intent.getStringExtra(PayrunActivity.PAYMENT_INPUT_ID_KEY);
            if (paymentInputId != null)
            {
                _paymentInput = LocalStorageUtil.retrieveAnnualInput(paymentInputId);
            }

            _employmentTypeId = intent.getIntExtra(PayrunActivity.EMPLOYMENT_TYPE_ID_KEY, EmploymentType.CASUAL.getId());
        }

        this.initResources();
        this.populateForm();
        this.initButtons();
    }

    @Override
    protected String getTag()
    {
        return LOG_TAG;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        if (_selectedFinancialYear != null)
        {
            outState.putString(KEY_FINANCIAL_YEAR, _selectedFinancialYear.getKey());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        String finYearKey = savedInstanceState.getString(KEY_FINANCIAL_YEAR, null);
        if (finYearKey != null)
        {
            FinancialYear finYear = FinancialYear.getByKey(finYearKey);
            this.updateFinancialYear(finYear);
        }
    }

    private void initResources()
    {
        _switchIsPermanent  = (Switch) findViewById(R.id.switch_employment_type);
        _rowDailyRate       = (ViewGroup) findViewById(R.id.payment_row_daily_rate);
        _txtDailyRate       = (EditText) findViewById(R.id.edit_daily_rate);
        _rowAnnualBase      = (ViewGroup) findViewById(R.id.payment_row_annual_base);
        _txtAnnualInput     = (EditText) findViewById(R.id.edit_annual_base);
        _switchInclSuper    = (Switch) findViewById(R.id.switch_payment_includes_super);
        _txtSuperRate       = (EditText) findViewById(R.id.edit_super_rate);
        _rowGstRate         = (ViewGroup) findViewById(R.id.payment_row_gst);
        _txtGstRate         = (EditText) findViewById(R.id.edit_gst_rate);
        _lblFinYear         = (TextView) findViewById(R.id.label_financial_year);

        _btnSave            = (Button) findViewById(R.id.btn_annual_input_save);
        _btnDelete          = (Button) findViewById(R.id.btn_annual_input_delete);
    }

    private void initButtons()
    {
        _switchIsPermanent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked)
            {
                changeEmploymentType();
            }
        });

        _lblFinYear.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showFinancialYearPicker();
            }
        });

        _btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                savePaymentInput();
            }
        });

        _btnDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                deletePaymentInput();
            }
        });
    }

    private void populateForm()
    {
        if (_paymentInput != null)
        {
            // UPDATE PaymentInput
            BigDecimal superRate    = _paymentInput.getSuperRate().multiply(new BigDecimal(100));
            BigDecimal gstRate      = _paymentInput.getGstRate().multiply(new BigDecimal(100));

            _switchIsPermanent.setChecked(ViewUtil.getTaxScheduleAsBoolean(_paymentInput.getEmploymentType()));
            _txtDailyRate.setText(ViewUtil.formatBigDecimalForEdit(_paymentInput.getDailyRateEntered()));
            _txtAnnualInput.setText(ViewUtil.formatBigDecimalForEdit(_paymentInput.getAnnualIncome()));
            _switchInclSuper.setChecked(_paymentInput.rateIncludesSuper());
            _txtSuperRate.setText(ViewUtil.formatBigDecimalForEdit(superRate));
            _txtGstRate.setText(ViewUtil.formatBigDecimalForEdit(gstRate));

            this.updateFinancialYear(_paymentInput.getFinancialYear());
        }
        else
        {
            // CREATE PaymentInput
            if (_employmentTypeId == EmploymentType.PERMANENT.getId())
            {
                _switchIsPermanent.setChecked(true);
            }
            else
            {
                _switchIsPermanent.setChecked(false);
            }

            FinancialYear defaultFinYear = PaymentInputPolicy.INSTANCE.getDefaultFinancialYear();
            if (defaultFinYear != FinancialYear.NONE)
            {
                this.updateFinancialYear(defaultFinYear);
                _txtSuperRate.setText(ViewUtil.formatBigDecimalForEdit(defaultFinYear.getMinSuper()));
                _txtGstRate.setText(ViewUtil.formatBigDecimalForEdit(defaultFinYear.getGst()));
            }
        }

        this.toggleVisibleElements();
    }

    private void toggleVisibleElements()
    {
        _btnDelete.setVisibility(View.GONE);
        // PERMANENT
        _rowDailyRate.setVisibility(View.GONE);
        _rowAnnualBase.setVisibility(View.VISIBLE);
        _rowGstRate.setVisibility(View.GONE);

        if (_paymentInput != null)
        {
            _btnDelete.setVisibility(View.VISIBLE);
        }

        if (!this.isPermanent())
        {
            // CASUAL
            _rowDailyRate.setVisibility(View.VISIBLE);
            _rowAnnualBase.setVisibility(View.GONE);
            _rowGstRate.setVisibility(View.VISIBLE);
        }

        this.setWidgetFocus(this.isPermanent());
    }

    private void setWidgetFocus(boolean isPermanent)
    {
        if (isPermanent)
        {
            // SET FOCUS TO AnnualBase EDIT TEXT
            _txtAnnualInput.requestFocus();
        }
        else
        {
            // SET FOCUS TO DailyRate EDIT TEXT
            _txtDailyRate.requestFocus();
        }
    }

    private void changeEmploymentType()
    {
        this.toggleVisibleElements();
    }

    private boolean isPermanent()
    {
        return _switchIsPermanent.isChecked();
    }

    private void savePaymentInput()
    {
        EmploymentType employmentType   = ViewUtil.getValueAsTaxSchedule(isPermanent());
        BigDecimal dailyRate            = ViewUtil.getValueAsBigDecimal(_txtDailyRate.getText().toString());
        BigDecimal annualIncome         = ViewUtil.getValueAsBigDecimal(_txtAnnualInput.getText().toString());
        boolean includesSuper           = _switchInclSuper.isChecked();
        BigDecimal superRate            = ViewUtil.getValueAsBigDecimal(_txtSuperRate.getText().toString());
        BigDecimal gstRate              = ViewUtil.getValueAsBigDecimal(_txtGstRate.getText().toString());
        FinancialYear finYear           = _selectedFinancialYear;

        superRate = superRate.multiply(new BigDecimal(0.01));
        gstRate   = gstRate.multiply(new BigDecimal(0.01));

        if (_paymentInput == null)
        {
            // CREATE PaymentInput
            PaymentInput pi = PaymentInput.Factory(null, employmentType.getId(), dailyRate, annualIncome, includesSuper, superRate, gstRate, finYear.getKey());
            if (pi != null)
            {
                if (pi.isValid())
                {
                    PaymentInputRepo.createPaymentInput(pi);
                    FabricAnswersService.paymentInputCreated(pi);
                    this.finish();
                }
                else
                {
                    showError(getString(R.string.payment_input_validation_error));
                }
            }
            else
            {
                showError(getString(R.string.payment_input_validation_error));
            }
        }
        else
        {
            // UPDATE PaymentInput
            PaymentInput pi = _paymentInput;
            pi.setEmploymentTypeId(employmentType.getId());
            pi.setDailyRateEntered(dailyRate);
            pi.setAnnualIncome(annualIncome);
            pi.setRateIncludesSuper(includesSuper);
            pi.setSuperRate(superRate);
            pi.setGstRate(gstRate);
            pi.setFinancialYearKey(finYear.getKey());
            if (pi.isValid())
            {
                PaymentInputRepo.updatePaymentInput(pi);
                FabricAnswersService.paymentInputUpdated();

                this.finish();
            }
            else
            {
                showError(getString(R.string.payment_input_validation_error));
            }
        }
    }

    private void deletePaymentInput()
    {
        AlertDialog.Builder builder = ViewUtil.buildDialogWithOptions(PaymentInputActivity.this, null, getString(R.string.confirm_delete));
        builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                delete();
            }
        });
        builder.setNegativeButton(R.string.btn_cancel, null);

        if (!isFinishing())
        {
            builder.show();
        }
    }

    private void showError(String message)
    {
        AlertDialog dialog = ViewUtil.buildDialog(PaymentInputActivity.this, null, message);
        if (dialog != null && !isFinishing())
        {
            dialog.show();
        }
    }

    private void delete()
    {
        PaymentInputs paymentInputs = new PaymentInputs(LocalStorageUtil.retrieveAnnualInputs());
        paymentInputs.removeItem(_paymentInput);
        LocalStorageUtil.storePaymentInputs(paymentInputs);
        this.finish();
    }

    private void showFinancialYearPicker()
    {
        FragmentManager fm = getFragmentManager();
        FinancialYearDialogFragment f = FinancialYearDialogFragment.newInstance(R.string.title_financial_year, _selectedFinancialYear, _dialogListener);
        f.show(fm, "financial_year_dialog_fragment");
    }

    /**
     * Sets _selectedFinancialYear value and sets its {@link FinancialYear#getKey()} value to <code>_lblFinYear</code>
     * @param financialYear Financial Year to se set as selected
     */
    private void updateFinancialYear(FinancialYear financialYear)
    {
        _selectedFinancialYear = financialYear;
        _lblFinYear.setText(_selectedFinancialYear.getKey());
    }
}
