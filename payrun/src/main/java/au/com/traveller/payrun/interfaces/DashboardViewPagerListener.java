package au.com.traveller.payrun.interfaces;

import au.com.traveller.payrun.enums.EmploymentType;

public interface DashboardViewPagerListener
{
    void onRefreshViewPagerTabs();
    void onAddPaymentInput(EmploymentType employmentType);
}
