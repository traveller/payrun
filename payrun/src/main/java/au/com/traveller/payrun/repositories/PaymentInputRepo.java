package au.com.traveller.payrun.repositories;

import au.com.traveller.payrun.models.PaymentInput;
import au.com.traveller.payrun.models.PaymentInputs;
import au.com.traveller.payrun.utils.LocalStorageUtil;

public class PaymentInputRepo {
    public static void createPaymentInput(PaymentInput pi) {
        PaymentInputs paymentInputs = new PaymentInputs(LocalStorageUtil.retrieveAnnualInputs());
        paymentInputs.addItem(pi);
        LocalStorageUtil.storePaymentInputs(paymentInputs);
    }

    public static void updatePaymentInput(PaymentInput pi) {
        PaymentInputs paymentInputs = new PaymentInputs(LocalStorageUtil.retrieveAnnualInputs());
        paymentInputs.updateItem(pi);
        LocalStorageUtil.storePaymentInputs(paymentInputs);
    }
}
