package au.com.traveller.payrun.models

data class TaxScale(val scaleNumber: Int, val margins: List<TaxScaleMargin>)
