package au.com.traveller.payrun.models

import java.math.BigDecimal

import au.com.traveller.payrun.enums.EmploymentType
import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.enums.PaymentFrequency
import au.com.traveller.payrun.utils.ViewUtil


class PayrunOutput(private val payrunInput: PayrunInput) {

    var incomePackage: BigDecimal? = null
        private set
    var superContribution: BigDecimal? = null
    var incomeBase: BigDecimal = BigDecimal.ZERO
        private set
    var taxTotal: BigDecimal? = null
    var taxMedicareLevy: BigDecimal? = null
    var lowIncomeTaxOffset: BigDecimal? = null
    var taxPaygContribution: BigDecimal? = null
    var netPay: BigDecimal? = null

    var gstAmount: BigDecimal? = null
    var invoiceAmountWithGst: BigDecimal? = null

    // ANNUAL
    var taxMedicareLevyAnnual: BigDecimal? = null

    /**
     * Returns IncomeBase<br></br>
     * Future-proofing: apply tax offsets
     * @return Income to pay taxes on (PAYG, Medicare Levy etc.)
     */
    val taxableIncome: BigDecimal?
        get() = incomeBase

    // HELPER
    val payrunLengthDescription: String
        get() = if (this.payrunInput.employmentType == EmploymentType.CASUAL) {
            String.format("%s %s", ViewUtil.formatPayrunDays(payrunDays), ViewUtil.pluraliseDay(payrunDays))
        } else {
            payrunInput.frequency.frequencyName
        }

    val payrunLengthCaption: String?
        get() = if (payrunInput.frequency != null) {
            payrunInput.frequency.frequencyName
        } else null

    val gstRate: BigDecimal
        get() = payrunInput.gstRate

    val payrunDays: BigDecimal
        get() = payrunInput.payrunDays

    val frequency: PaymentFrequency
        get() = payrunInput.frequency

    val financialYear: FinancialYear
        get() = payrunInput.financialYear

    val employmentType: EmploymentType
        get() = payrunInput.employmentType

    val taxScale: Int
        get() = payrunInput.taxScale

    init {
        if (payrunInput.payrunIncomeBase != null) {
            this.setPayrunPackageAndBase()
        } else {
            this.incomePackage = BigDecimal.ZERO
            this.superContribution = BigDecimal.ZERO
            this.incomeBase = BigDecimal.ZERO
            this.taxTotal = BigDecimal.ZERO
            this.taxMedicareLevy = BigDecimal.ZERO
            this.taxPaygContribution = BigDecimal.ZERO
            this.netPay = BigDecimal.ZERO
            this.gstAmount = BigDecimal.ZERO
            this.invoiceAmountWithGst = BigDecimal.ZERO
            this.taxMedicareLevyAnnual = BigDecimal.ZERO
        }
    }

    private fun setPayrunPackageAndBase() {
        this.incomeBase = payrunInput.payrunIncomeBase
        this.incomePackage = payrunInput.payrunIncomePackage
    }

    fun includesSuper(): Boolean {
        return payrunInput.includesSuper()
    }
}
