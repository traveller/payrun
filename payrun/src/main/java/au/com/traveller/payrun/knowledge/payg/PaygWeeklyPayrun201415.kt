package au.com.traveller.payrun.knowledge.payg

import au.com.traveller.payrun.knowledge.PayrunCalculator
import au.com.traveller.payrun.models.PayrunInput
import au.com.traveller.payrun.models.PayrunOutput
import au.com.traveller.payrun.repositories.MedicareLevyRepo
import au.com.traveller.payrun.repositories.PaygWeeklyScheduleRepo
import au.com.traveller.payrun.utils.ConversionUtil
import au.com.traveller.payrun.utils.Rounding
import java.math.RoundingMode


class PaygWeeklyPayrun201415(private val medicareLevyRepo: MedicareLevyRepo,
                             private val paygScheduleRepo: PaygWeeklyScheduleRepo) : PayrunCalculator {

    private lateinit var payrunOutput: PayrunOutput

    override fun getPayrunOutput(payrunInput: PayrunInput): PayrunOutput {
        payrunOutput = PayrunOutput(payrunInput)

        // AS CLOSED BOUNDARY
        calcSuperContribution()
                .calcPayrunTotalTax()
                .calcPayrunMedicareLevy()
                .calcPayrunPaygContribution()
                .calcNetPay()
                .calcInvoiceAmountWithGst()
                .calcGstAmount()

        return payrunOutput
    }

    private fun calcPayrunTotalTax(): PaygWeeklyPayrun201415 {
        if (payrunOutput.taxTotal == null) {
            val taxableIncome = payrunOutput.taxableIncome
            val payg = paygScheduleRepo.getPaygTaxSchedule(payrunOutput.financialYear)
            val totalTax = payg.getTaxForScaleFrequencyAndAmount(payrunOutput.taxScale, payrunOutput.frequency, taxableIncome)
            payrunOutput.taxTotal = Rounding.roundHalfUp(totalTax, 2)
        }
        return this
    }

    private fun calcSuperContribution(): PaygWeeklyPayrun201415 {
        if (payrunOutput.superContribution == null) {
            payrunOutput.superContribution = payrunOutput.incomePackage!!.subtract(payrunOutput.incomeBase)
        }

        return this
    }

    private fun calcPayrunMedicareLevy(): PaygWeeklyPayrun201415 {
        if (payrunOutput.taxMedicareLevy == null) {
            val frequencyRate = payrunOutput.frequency.frequency
            val payrunTaxableIncome = payrunOutput.taxableIncome
            val annualTaxableIncome = payrunTaxableIncome!!.multiply(frequencyRate)

            val medicareLevy = medicareLevyRepo.getMedicareLevy(payrunOutput.financialYear)
            val annualLevy = medicareLevy.getAnnualLevy(annualTaxableIncome)
            val payrunLevy = annualLevy.divide(frequencyRate, RoundingMode.HALF_UP)
            payrunOutput.taxMedicareLevy = Rounding.roundHalfUp(payrunLevy, 2)
        }

        return this
    }

    private fun calcPayrunPaygContribution(): PaygWeeklyPayrun201415 {
        if (payrunOutput.taxPaygContribution == null) {
            // TAKE PAYRUN TaxTotal AND SUBTRACT TaxMedicareLevy
            payrunOutput.taxPaygContribution = payrunOutput.taxTotal!!.subtract(payrunOutput.taxMedicareLevy)
        }

        return this
    }

    private fun calcNetPay(): PaygWeeklyPayrun201415 {
        if (payrunOutput.netPay == null) {
            val netPay = payrunOutput.incomeBase.subtract(payrunOutput.taxTotal)
            payrunOutput.netPay = netPay
        }
        return this
    }

    private fun calcInvoiceAmountWithGst(): PaygWeeklyPayrun201415 {
        if (payrunOutput.invoiceAmountWithGst == null) {
            val result = ConversionUtil.increaseValueByRate(payrunOutput.incomePackage, payrunOutput.gstRate)
            payrunOutput.invoiceAmountWithGst = Rounding.roundHalfUp(result, 2)
        }
        return this
    }

    private fun calcGstAmount(): PaygWeeklyPayrun201415 {
        if (payrunOutput.gstAmount == null) {
            val result = payrunOutput.invoiceAmountWithGst!!.subtract(payrunOutput.incomePackage)
            payrunOutput.gstAmount = Rounding.roundHalfUp(result, 2)
        }
        return this
    }
}
