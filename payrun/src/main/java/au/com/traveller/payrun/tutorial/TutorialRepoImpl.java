package au.com.traveller.payrun.tutorial;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.view.Gravity;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.TutorialStep;
import au.com.traveller.payrun.utils.AndroidUtil;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

public abstract class TutorialRepoImpl implements TutorialRepo {
    static TourGuide TOUR_GUIDE_HANDLER;

    public abstract boolean hasCompleted();

    public abstract void markCompleted();

    public abstract TourGuide getNextStep(Activity activity, TutorialStep nextStep);

    @SuppressLint("NewApi")
    protected TourGuide init(final Activity activity, int titleResId, int descResId) {
        ToolTip t = new ToolTip();
        if (titleResId > 0) {
            t.setTitle(activity.getString(titleResId));
        }
        if (descResId > 0) {
            t.setDescription(activity.getString(descResId));
        }
        t.setGravity(Gravity.BOTTOM);
        t.setEnterAnimation(getEnterAnimation());
        if (AndroidUtil.getSdk() <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            //noinspection deprecation
            t.setBackgroundColor(activity.getResources().getColor(R.color.brand_primary_color_accent));
        }
        if (AndroidUtil.getSdk() >= Build.VERSION_CODES.M) {
            t.setBackgroundColor(activity.getResources().getColor(R.color.brand_primary_color_accent, activity.getTheme()));
        }

        return TourGuide
                .init(activity)
                .motionType(TourGuide.MotionType.ALLOW_ALL)
                .setToolTip(t)
                //.setOverlay(new Overlay().setOnClickListener(v -> getNextStep(activity, TutorialStep.NONE)))
                ;
    }

    private Animation getEnterAnimation() {
        Animation anim = new AlphaAnimation(0f, 1f);
        anim.setDuration(1000);
        anim.setFillAfter(true);
        anim.setInterpolator(new LinearInterpolator());
        return anim;
    }
}