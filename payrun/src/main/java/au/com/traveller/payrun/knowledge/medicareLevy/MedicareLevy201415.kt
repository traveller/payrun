package au.com.traveller.payrun.knowledge.medicareLevy

import au.com.traveller.payrun.knowledge.MedicareLevy
import au.com.traveller.payrun.models.MedicareLevySchedule
import java.math.BigDecimal

class MedicareLevy201415(private val medicareLevySchedule: MedicareLevySchedule) : MedicareLevy {

    override fun getAnnualLevy(annualTaxableIncome: BigDecimal): BigDecimal {
        return when {
            annualTaxableIncome > (medicareLevySchedule.threshold2) -> annualTaxableIncome.multiply(medicareLevySchedule.rate2)
            annualTaxableIncome > (medicareLevySchedule.threshold1) -> annualTaxableIncome.subtract(medicareLevySchedule.threshold1).multiply(medicareLevySchedule.rate1)
            else -> BigDecimal.ZERO
        }
    }
}
