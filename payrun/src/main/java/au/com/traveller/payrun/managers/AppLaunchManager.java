package au.com.traveller.payrun.managers;

public interface AppLaunchManager
{
    void runUpgrades(int newVersion);
}
