package au.com.traveller.payrun.policies

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.repositories.FinancialYearRepo
import au.com.traveller.payrun.repositories.FinancialYearRepoImpl
import java.util.*

object PaymentInputPolicy {

    private val repo: FinancialYearRepo

    init {
        repo = FinancialYearRepoImpl()
    }

    val defaultFinancialYear = getDefaultFinancialYear(null)

    fun getDefaultFinancialYear(calendar: Calendar?): FinancialYear {
        val yearStart: Int
        val yearEnd: Int
        val currentYear: Int
        val currentMonth: Int

        val c = calendar ?: Calendar.getInstance(TimeZone.getDefault())
        currentYear = c.get(Calendar.YEAR)
        currentMonth = c.get(Calendar.MONTH) + 1 // Calendar.MONTH returns zero-based index (0..11)

        if (currentMonth < 7) {
            yearStart = currentYear - 1
            yearEnd = currentYear
        } else {
            yearStart = currentYear
            yearEnd = currentYear + 1
        }

        val supportedYear = repo.supportedYears.firstOrNull { it.yearStart == yearStart && it.yearEnd == yearEnd }

        return supportedYear ?: repo.latestSupportedYear
    }
}