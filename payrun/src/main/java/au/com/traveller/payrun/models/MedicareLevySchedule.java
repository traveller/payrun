package au.com.traveller.payrun.models;

import java.math.BigDecimal;

public class MedicareLevySchedule
{
    private BigDecimal threshold_1;
    private BigDecimal rate_1;
    private BigDecimal threshold_2;
    private BigDecimal rate_2;

    public MedicareLevySchedule(BigDecimal threshold1, BigDecimal rate1, BigDecimal threshold2, BigDecimal rate2)
    {
        this.threshold_1    = threshold1;
        this.rate_1         = rate1;
        this.threshold_2    = threshold2;
        this.rate_2         = rate2;
    }

    public BigDecimal getThreshold1()
    {
        return threshold_1;
    }

    public BigDecimal getRate1()
    {
        return rate_1;
    }

    public BigDecimal getThreshold2()
    {
        return threshold_2;
    }

    public BigDecimal getRate2()
    {
        return rate_2;
    }
}
