package au.com.traveller.payrun.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import java.math.BigDecimal;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.interfaces.DialogValueListener;
import au.com.traveller.payrun.widgets.NumberPicker;

public class PayrunDaysDialogFragment extends DialogFragment
{
    private NumberPicker _payrunDaysPicker;
    private DialogValueListener _listener;

    public static PayrunDaysDialogFragment newInstance(int title, int payrunDays, DialogValueListener listener)
    {
        PayrunDaysDialogFragment f = new PayrunDaysDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putInt("initial_payrun_days", payrunDays);
        f._listener = listener;
        f.setArguments(args);
        return f;
    }

    public PayrunDaysDialogFragment()
    {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        int title = getArguments().getInt("title");
        int initialPayrunDays = getArguments().getInt("initial_payrun_days");

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_payrun_days, null);
        _payrunDaysPicker = (NumberPicker) v.findViewById(R.id.numberPicker_payrun_days);

        // DISABLE USER EDIT (SOFT-KEYBOARD)
        _payrunDaysPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        _payrunDaysPicker.setMinValue(1);
        _payrunDaysPicker.setMaxValue(366);
        _payrunDaysPicker.setValue(initialPayrunDays);
        _payrunDaysPicker.setWrapSelectorWheel(false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Dialog);
        builder.setTitle(title);
        builder.setView(v);

        builder.setPositiveButton(android.R.string.ok,
            new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    _listener.onNewPayrunDays(new BigDecimal(_payrunDaysPicker.getValue()));
                }
            });
        builder.setNegativeButton(android.R.string.cancel,
            new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    // JUST CLOSE THE DIALOG
                }
            });

        return builder.create();
    }
}
