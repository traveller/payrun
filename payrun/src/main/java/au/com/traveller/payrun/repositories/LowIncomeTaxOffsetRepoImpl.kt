package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.LowIncomeTaxOffset
import au.com.traveller.payrun.knowledge.lowIncome.LowIncome20152016
import au.com.traveller.payrun.knowledge.lowIncome.LowIncomeEmpty
import au.com.traveller.payrun.knowledge.lowIncome.LowIncomeTaxOffsetCalc
import au.com.traveller.payrun.knowledge.lowIncome.LowIncomeTaxOffsetCalc20182019


class LowIncomeTaxOffsetRepoImpl : LowIncomeTaxOffsetRepo {

    override fun getIncomeOffset(financialYear: FinancialYear): List<LowIncomeTaxOffset> {
        return when (financialYear) {
            FinancialYear.FY_2014_15,
            FinancialYear.FY_2015_16,
            FinancialYear.FY_2016_17,
            FinancialYear.FY_2017_18 -> listOf(LowIncomeTaxOffsetCalc(LowIncome20152016()))
            FinancialYear.FY_2018_19 -> listOf(LowIncomeTaxOffsetCalc(LowIncome20152016()), LowIncomeTaxOffsetCalc20182019())
            else -> listOf(LowIncomeTaxOffsetCalc(LowIncomeEmpty()))
        }
    }
}