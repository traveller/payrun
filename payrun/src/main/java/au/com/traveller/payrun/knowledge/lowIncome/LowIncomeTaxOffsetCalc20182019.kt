package au.com.traveller.payrun.knowledge.lowIncome

import au.com.traveller.payrun.knowledge.LowIncomeTaxOffset
import au.com.traveller.payrun.utils.Rounding
import java.math.BigDecimal


/**
 * If your income:
 *  does not exceed $37,000 you are entitled to $200
 *  exceeds $37,000 but does not exceed $48,000 you are entitled to $200 plus 3% of the amount of the income that exceeds $37,000
 *  exceeds $48,000 but not $90,000, you are entitled to $530
 *  exceeds $90,000 you are entitled to $530 less 1.5% of the amount of the income that exceeds $90,000.
 */
class LowIncomeTaxOffsetCalc20182019() : LowIncomeTaxOffset {

    private val baseRange = (BigDecimal.ZERO..BigDecimal(37000.01))
    private val baseOffset = BigDecimal(200)

    private val lowRange = (BigDecimal(37000)..BigDecimal(48000))
    private val lowRangeRate = BigDecimal(0.03) // 3%

    private val midRange = (BigDecimal(48000)..BigDecimal(90000))
    private val highThreshold = BigDecimal(90000)

    private val maxOffset = BigDecimal(530)
    private val taxDecreaseRate = BigDecimal(0.015)

    override fun getAmount(taxableIncome: BigDecimal): BigDecimal {
        val result = when (taxableIncome) {
            in baseRange -> baseOffset
            in lowRange -> {
                // CALCULATE OFFSET
                val lowIncomePortion = taxableIncome.subtract(lowRange.start)
                baseOffset.add(lowIncomePortion.multiply(lowRangeRate))
            }
            in midRange -> maxOffset
            else -> {
                // CALCULATE OFFSET
                val highIncomePortion = taxableIncome.subtract(highThreshold)
                val portionTax = highIncomePortion.multiply(taxDecreaseRate)
                val reducedOffset = maxOffset.subtract(portionTax)
                if (reducedOffset > BigDecimal.ZERO) reducedOffset else BigDecimal.ZERO
            }
        }

        return Rounding.roundHalfUp(result, 2)
    }
}