package au.com.traveller.payrun.policies;

import android.content.Context;

import java.math.BigDecimal;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.DashboardPageType;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.PaymentRateType;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.managers.PayrunManager;
import au.com.traveller.payrun.models.AppSettings;
import au.com.traveller.payrun.models.PaymentInput;
import au.com.traveller.payrun.models.PayrunInput;
import au.com.traveller.payrun.models.PayrunOutput;
import au.com.traveller.payrun.utils.LocalStorageUtil;

public class DashboardPolicy
{
    public static EmploymentType getEmploymentType(DashboardPageType pageType)
    {
        EmploymentType result = EmploymentType.CASUAL;
        if (pageType == DashboardPageType.PERMANENT)
        {
            result = EmploymentType.PERMANENT;
        }

        return result;
    }

    public static boolean attachAddClickEvent(DashboardPageType pageType)
    {
        if (pageType == DashboardPageType.CASUAL || pageType == DashboardPageType.PERMANENT)
        {
            return true;
        }

        return false;
    }

    public static boolean showShortPreviewPayrun(AppSettings appSettings, EmploymentType employmentType)
    {
        if (employmentType == EmploymentType.CASUAL)
        {
            return appSettings.isWeeklyPayrunCasualVisible();
        }
        else
        {
            return appSettings.isPermWeeklyPayrunVisible();
        }
    }

    public static boolean showMediumPreviewPayrun(AppSettings appSettings, EmploymentType employmentType)
    {
        if (employmentType == EmploymentType.CASUAL)
        {
            return appSettings.isFortnightlyPayrunCasualVisible();
        }
        else
        {
            return appSettings.isPermFortnightlyPayrunVisible();
        }
    }

    public static boolean showLongPreviewPayrun(AppSettings appSettings, EmploymentType employmentType)
    {
        if (employmentType == EmploymentType.CASUAL)
        {
            return appSettings.isMonthlyPayrunCasualVisible();
        }
        else
        {
            return appSettings.isPermMonthlyPayrunVisible();
        }
    }

    public static boolean showVeryLongPreviewPayrun(AppSettings appSettings, EmploymentType employmentType)
    {
        if (employmentType == EmploymentType.CASUAL)
        {
            return appSettings.isAnnuallyPayrunCasualVisible();
        }
        else
        {
            return appSettings.isPermAnnuallyPayrunVisible();
        }
    }

    public static BigDecimal getRateAmount(PaymentInput paymentInput)
    {
        BigDecimal result;

        AppSettings appSettings = LocalStorageUtil.retrieveSettings();
        if (paymentInput.getEmploymentType() == EmploymentType.CASUAL)
        {
            result = paymentInput.getDailyRate(appSettings);
        }
        else
        {
            result = paymentInput.getAnnualRate(appSettings);
        }

        return result;
    }

    public static String getRateCaption(Context context, PaymentInput paymentInput)
    {
        String result = "%s (%s)";
        String rateFrequency;
        String rateType;

        AppSettings appSettings = LocalStorageUtil.retrieveSettings();
        if (paymentInput.getEmploymentType() == EmploymentType.CASUAL)
        {
            rateFrequency = context.getString(R.string.label_daily_rate);
            rateType = getRateCaptionByType(context, paymentInput, appSettings.getCasualRateType());
        }
        else
        {
            rateFrequency = context.getString(R.string.label_annual_income);
            rateType = getRateCaptionByType(context, paymentInput, appSettings.getPermanentRateType());
        }

        result = String.format(result, rateFrequency, rateType);

        return result;
    }

    private static String getRateCaptionByType(Context context, PaymentInput paymentInput, PaymentRateType paymentRateType)
    {
        String result;
        switch (paymentRateType)
        {
            case BASE:
                result = context.getString(R.string.label_rate_base);
                break;
            case PACKAGE:
                result = context.getString(R.string.label_rate_package);
                break;
            default:
                if (paymentInput.rateIncludesSuper())
                {
                    result = context.getString(R.string.label_rate_package);
                }
                else
                {
                    result = context.getString(R.string.label_rate_base);
                }
                break;
        }
        return result;
    }

    public static String getIncomeCaption(Context context, PaymentInput paymentInput)
    {
        String result = "%s (%s)";
        String incomeLabel;
        String rateType;

        AppSettings appSettings = LocalStorageUtil.retrieveSettings();
        if (paymentInput.getEmploymentType() == EmploymentType.CASUAL)
        {
            incomeLabel = context.getString(R.string.label_payrun_income);
            rateType = getRateCaptionByType(context, paymentInput, appSettings.getCasualRateType());
        }
        else
        {
            incomeLabel = context.getString(R.string.label_payrun_income);
            rateType = getRateCaptionByType(context, paymentInput, appSettings.getPermanentRateType());
        }

        result = String.format(result, incomeLabel, rateType);

        return result;
    }

    public static BigDecimal getPayrunIncome(PaymentInput paymentInput, BigDecimal payrunIncome, PaymentFrequency payrunFrequency)
    {
        BigDecimal result;

        if (paymentInput.getEmploymentType() == EmploymentType.CASUAL)
        {
            result = payrunIncome;
        }
        else
        {
            AppSettings appSettings = LocalStorageUtil.retrieveSettings();
            PayrunInput payrunInput = new PayrunInput(paymentInput.getFinancialYear(), payrunFrequency, paymentInput.getSuperRate(), paymentInput.getGstRate(), paymentInput.getTaxScheduleScale());
            payrunInput.setPermanentValues(PayrunPolicy.payrunIncomeIncludesSuper(paymentInput, appSettings), payrunIncome);
            PayrunManager pm = new PayrunManager();
            PayrunOutput po = pm.getPayrunOutput(payrunInput);

            // SHOW Base OR Package (AppSettings)
            result = PayrunPolicy.payrunOutputIncomeValue(po, appSettings);
        }

        return result;
    }

    public static BigDecimal getDailyRate(PaymentInput paymentInput)
    {
        BigDecimal result;

        AppSettings appSettings = LocalStorageUtil.retrieveSettings();
        if (paymentInput.getEmploymentType() == EmploymentType.CASUAL)
        {
            result = paymentInput.getDailyRate(appSettings);
        }
        else
        {
            result = BigDecimal.ZERO;
        }

        return result;
    }
}
