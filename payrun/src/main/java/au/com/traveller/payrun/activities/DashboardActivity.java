package au.com.traveller.payrun.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import au.com.traveller.payrun.R;
import au.com.traveller.payrun.adapters.ViewPagerAdapter;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.TutorialStep;
import au.com.traveller.payrun.interfaces.DashboardViewPagerListener;
import au.com.traveller.payrun.services.AppFlowService;
import au.com.traveller.payrun.tutorial.TutorialDashboardRepoImpl;
import au.com.traveller.payrun.tutorial.TutorialRepo;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.TourGuide;

public class DashboardActivity extends BaseActivity implements DashboardViewPagerListener {
    protected String LOG_TAG = DashboardActivity.class.getSimpleName();

    private ViewPager _viewPager;
    private TabLayout _tabLayout;
    private TutorialRepo _tutorial;

    public static String POSITION = "POSITION";

    @Override
    public void onRefreshViewPagerTabs() {
        _viewPager.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onAddPaymentInput(EmploymentType employmentType) {
        AppFlowService.showPaymentInputForm(DashboardActivity.this, null, employmentType);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(POSITION, _tabLayout.getSelectedTabPosition());
        Log.i(getTag(), "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        _viewPager.setCurrentItem(savedInstanceState.getInt(POSITION));
        Log.i(getTag(), "onRestoreInstanceState");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        this.initToolbar(R.string.title_activity_dashboard, false);

        this.injectDependencies();

        this.initResources();

        this.initTabs();

        this.startTutorial();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // REFRESH ALL TABS
        this.onRefreshViewPagerTabs();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                showSettings();
                break;
            case R.id.action_about:
                showAbout();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected String getTag() {
        return LOG_TAG;
    }

    private void injectDependencies() {
        _tutorial = new TutorialDashboardRepoImpl();
    }

    private void initResources() {
        this._viewPager = findViewById(R.id.dashboard_viewpager);
        _tabLayout = findViewById(R.id.toolbar_tab_layout);
    }

    private void initTabs() {
        setupViewPager(this._viewPager);
        _tabLayout.setupWithViewPager(this._viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter _viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), DashboardActivity.this);
        viewPager.setAdapter(_viewPagerAdapter);
    }

    private void showSettings() {
        Intent intent = new Intent(DashboardActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    private void showAbout() {
        Intent intent = new Intent(DashboardActivity.this, AboutActivity.class);
        startActivity(intent);
    }

    private void startTutorial() {
        if (!_tutorial.hasCompleted()) {
            _tutorial.markCompleted();
            tutorialTabCasual();
        }
    }

    private void tutorialTabCasual() {
        View targetView = ((ViewGroup) _tabLayout.getChildAt(0)).getChildAt(1);
        TourGuide tg = _tutorial.getNextStep(this, TutorialStep.TAB_CASUAL);
        tg.setOverlay(new Overlay()
                        .setOnClickListener(v -> tutorialTabPermanent())
                //.setEnterAnimation(null)
        );
        tg.playOn(targetView);
    }

    private void tutorialTabPermanent() {
        View targetView = ((ViewGroup) _tabLayout.getChildAt(0)).getChildAt(2);
        TourGuide tg = _tutorial.getNextStep(this, TutorialStep.TAB_PERMANENT);
        tg.setOverlay(new Overlay()
                        .setOnClickListener(v -> tutorialTabFavourites())
                //.setEnterAnimation(null)
        );
        tg.playOn(targetView);
    }

    private void tutorialTabFavourites() {
        View targetView = ((ViewGroup) _tabLayout.getChildAt(0)).getChildAt(0);
        TourGuide tg = _tutorial.getNextStep(this, TutorialStep.TAB_FAVOURITES);
        tg.playOn(targetView);
    }
}