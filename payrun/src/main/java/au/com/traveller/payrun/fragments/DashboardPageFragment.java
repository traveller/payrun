package au.com.traveller.payrun.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import au.com.traveller.payrun.R;
import au.com.traveller.payrun.adapters.DashboardAdapter;
import au.com.traveller.payrun.enums.DashboardPageType;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.enums.TutorialStep;
import au.com.traveller.payrun.interfaces.DashboardItemListener;
import au.com.traveller.payrun.interfaces.DashboardViewPagerListener;
import au.com.traveller.payrun.models.PaymentInput;
import au.com.traveller.payrun.policies.DashboardPolicy;
import au.com.traveller.payrun.repositories.PaymentInputRepo;
import au.com.traveller.payrun.services.AppFlowService;
import au.com.traveller.payrun.services.FabricAnswersService;
import au.com.traveller.payrun.tutorial.TutorialPaymentItemRepoImpl;
import au.com.traveller.payrun.tutorial.TutorialRepo;
import au.com.traveller.payrun.utils.LocalStorageUtil;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.TourGuide;


public class DashboardPageFragment extends Fragment implements DashboardItemListener {
    private DashboardPageType _dashboardPageType;

    private static final String ARG_PAGE_TYPE = "ARG_PAGE_TYPE";

    private List<PaymentInput> _dashboardPageItems;
    private DashboardAdapter _dashboardAdapter;

    private RecyclerView _recyclerView;
    private ViewGroup _emptyPanel;
    private FloatingActionButton _fab;
    private TutorialRepo _tutorial;

    public static DashboardPageFragment newInstance(DashboardPageType dashboardPageType) {
        DashboardPageFragment df = new DashboardPageFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PAGE_TYPE, dashboardPageType);
        df.setArguments(args);

        return df;
    }

    public DashboardPageFragment() {
        this.injectDependencies();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this._dashboardPageType = (DashboardPageType) getArguments().getSerializable(ARG_PAGE_TYPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_tab_fragment, container, false);

        this.initResources(view);
        this.initRecyclerViewer();
        this.bindButtons();

        return view;
    }

    private DashboardPageType getDashboardPageType() {
        if (_dashboardPageType != null) {
            return _dashboardPageType;
        } else {
            return DashboardPageType.NONE;
        }
    }

    private void injectDependencies() {
        _tutorial = new TutorialPaymentItemRepoImpl();
    }

    private void initResources(View view) {
        TextView _emptyMessage;

        _recyclerView = view.findViewById(R.id.dashboard_tab_list);
        _emptyPanel = view.findViewById(R.id.dashboard_tab_empty);
        _emptyMessage = view.findViewById(R.id.dashboard_tab_empty_message);
        _fab = view.findViewById(R.id.dashboard_tab_fab);

        if (_emptyMessage != null) {
            switch (getDashboardPageType()) {
                case FAVORITES:
                    _emptyMessage.setText(R.string.dashboard_tab_favourites_empty);
                    _fab.hide();
                    break;
                case CASUAL:
                    _emptyMessage.setText(R.string.dashboard_tab_casual_empty);
                    _fab.show();
                    break;
                case PERMANENT:
                    _emptyMessage.setText(R.string.dashboard_tab_permanent_empty);
                    break;
            }
        }
    }

    private void initRecyclerViewer() {
        _dashboardPageItems = new ArrayList<>();
        _dashboardAdapter = new DashboardAdapter(_dashboardPageItems, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        _recyclerView.setLayoutManager(layoutManager);
        _recyclerView.setHasFixedSize(true);
        _recyclerView.setAdapter(_dashboardAdapter);

        updatePage();
    }

    private void bindButtons() {
        _fab.hide();
        switch (getDashboardPageType()) {
            case FAVORITES:
                _fab.setOnClickListener(null);
                break;
            case CASUAL:
                if (getActivity() instanceof DashboardViewPagerListener) {
                    _fab.show();
                    _fab.setOnClickListener(v -> ((DashboardViewPagerListener) getActivity()).onAddPaymentInput(EmploymentType.CASUAL));
                }
                break;
            case PERMANENT:
                if (getActivity() instanceof DashboardViewPagerListener) {
                    _fab.show();
                    _fab.setOnClickListener(v -> {
                        if (getActivity() instanceof DashboardViewPagerListener) {
                            ((DashboardViewPagerListener) getActivity()).onAddPaymentInput(EmploymentType.PERMANENT);
                        }
                    });
                }
                break;
        }
    }

    private void updatePage() {
        if (getDashboardPageType() != DashboardPageType.NONE) {
            _dashboardPageItems.clear();
            _dashboardPageItems.addAll(LocalStorageUtil.retrieveAnnualInputs(getDashboardPageType()));
            if (_dashboardAdapter != null) {
                _dashboardAdapter.notifyDataSetChanged();
            }

            this.toggleEmptyView();
        }
    }

    @Override
    public void onPaymentInputEdit(PaymentInput paymentInput) {
        AppFlowService.showPaymentInputForm(getActivity(), paymentInput, paymentInput.getEmploymentType());
    }

    @Override
    public void onPaymentInputPayrun(PaymentInput paymentInput, BigDecimal payrunDays, PaymentFrequency frequency) {
        AppFlowService.showPayrunDetails(getActivity(), paymentInput, payrunDays, frequency);
    }

    @Override
    public void onPaymentInputPinned(PaymentInput paymentInput) {
        this.pinUnpinPaymentInput(paymentInput);
    }

    private void pinUnpinPaymentInput(PaymentInput paymentInput) {
        int pinUnpinMessage;

        // REVERSE VALUE
        paymentInput.setFavourite(!paymentInput.isFavourite());

        // UPDATE PaymentInput
        PaymentInputRepo.updatePaymentInput(paymentInput);

        // UPDATE ALL DASHBOARD TABS
        if (getActivity() instanceof DashboardViewPagerListener) {
            ((DashboardViewPagerListener) getActivity()).onRefreshViewPagerTabs();
        }

        if (paymentInput.isFavourite()) {
            FabricAnswersService.payrunAddedToFavs();
            pinUnpinMessage = R.string.text_favourites_added;
        } else {
            FabricAnswersService.payrunRemovedFromFavs();
            pinUnpinMessage = R.string.text_favourites_removed;
        }

        Toast.makeText(getActivity(), getString(pinUnpinMessage), Toast.LENGTH_SHORT).show();
    }

    private void toggleEmptyView() {
        if (_recyclerView != null && _emptyPanel != null) {
            if (_dashboardPageItems.size() == 0) {
                // SHOW EMPTY MESSAGE
                _recyclerView.setVisibility(View.GONE);
                _emptyPanel.setVisibility(View.VISIBLE);

                if (DashboardPolicy.attachAddClickEvent(getDashboardPageType())) {
                    _emptyPanel.setOnClickListener(v -> {
                        if (getActivity() instanceof DashboardViewPagerListener) {
                            ((DashboardViewPagerListener) getActivity()).onAddPaymentInput(DashboardPolicy.getEmploymentType(getDashboardPageType()));
                        }
                    });
                }
            } else {
                // SHOW THE LIST
                _recyclerView.setVisibility(View.VISIBLE);
                _emptyPanel.setVisibility(View.GONE);

                // REMOVE CLICK LISTENER (IF ANY)
                _emptyPanel.setOnClickListener(null);
            }
        }
    }

    private void showTutorial(final View view) {
        if (!_tutorial.hasCompleted()) {
            _tutorial.markCompleted();
            showEditTutorial(view);
        }
    }

    private void showEditTutorial(final View view) {
        View targetView = view.findViewById(R.id.btn_edit_annual_input);
        if (targetView != null) {
            TourGuide tg = _tutorial.getNextStep(getActivity(), TutorialStep.PAYMENT_EDIT);
            tg.setOverlay(new Overlay()
                    .setOnClickListener(v -> showTutorialFavourite(view))
                    //.setEnterAnimation(null)
            );
            tg.playOn(targetView);
        }
    }

    private void showTutorialFavourite(final View view) {
        View targetView = view.findViewById(R.id.btn_favourite_payment_input);
        if (targetView != null) {
            TourGuide tg = _tutorial.getNextStep(getActivity(), TutorialStep.PAYMENT_FAVOURITE);
            tg.setOverlay(new Overlay()
                    .setOnClickListener(v -> showTutorialDetails(view))
                    //.setEnterAnimation(null)
            );
            tg.playOn(targetView);
        }
    }

    private void showTutorialDetails(final View view) {
        View targetView = view.findViewById(R.id.btn_show_payrun_details);
        if (targetView != null) {
            TourGuide tg = _tutorial.getNextStep(getActivity(), TutorialStep.PAYRUN_DETAILS);
            tg.setOverlay(new Overlay()
                    .setOnClickListener(v -> showAppSettings())
                    //.setEnterAnimation(null)
            );
            tg.playOn(targetView);
        }
    }

    private void showAppSettings() {
        View targetView = getActivity().findViewById(R.id.toolbar).getTouchables().get(0);
        if (targetView != null) {
            TourGuide tg = _tutorial.getNextStep(getActivity(), TutorialStep.APP_SETTINGS);
            tg.playOn(targetView);
        }
    }

    /**
     * Called from ViewPagerAdapter to notify Fragment being the one user can see now (current page/tab)
     */
    public void setAsPrimary() {
        if (getDashboardPageType() != DashboardPageType.NONE) {
            if (_dashboardPageItems != null && _dashboardPageItems.size() > 0) {
                RecyclerView.ViewHolder v = _recyclerView.findViewHolderForAdapterPosition(0);
                if (v != null) {
                    showTutorial(v.itemView);
                }
            }
        }
    }
}