package au.com.traveller.payrun.dialogs;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.FinancialYear;
import au.com.traveller.payrun.interfaces.FYDialogListener;
import au.com.traveller.payrun.repositories.FinancialYearRepo;
import au.com.traveller.payrun.repositories.FinancialYearRepoImpl;
import au.com.traveller.payrun.utils.FinancialYearDialogUtil;
import au.com.traveller.payrun.widgets.NumberPicker;

public class FinancialYearDialogFragment extends DialogFragment
{
    private NumberPicker _picker;
    private FYDialogListener _listener;
    private String[] _financialYears;

    private FinancialYearRepo _financialYearRepo;

    public static FinancialYearDialogFragment newInstance(int title, FinancialYear financialYear, FYDialogListener listener)
    {
        FinancialYearDialogFragment f = new FinancialYearDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putString("initial_financial_year", financialYear.getKey());
        f._listener = listener;
        f.setArguments(args);
        return f;
    }

    public FinancialYearDialogFragment()
    {
        this.initDependencies();
        _financialYears = _financialYearRepo.getSupportedYearsKeys();
    }

    private void initDependencies()
    {
        _financialYearRepo = new FinancialYearRepoImpl();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        int title = getArguments().getInt("title");
        String initialFYKey = getArguments().getString("initial_financial_year");
        FinancialYear initialFY = FinancialYear.getByKey(initialFYKey);

        @SuppressLint("InflateParams")
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_payrun_days, null, false);
        _picker = (NumberPicker) v.findViewById(R.id.numberPicker_payrun_days);

        // DISABLE USER EDIT (SOFT-KEYBOARD)
        _picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        _picker.setMinValue(0);
        _picker.setMaxValue(_financialYears.length - 1);
        _picker.setValue(FinancialYearDialogUtil.INSTANCE.finYearToIndex(initialFY));
        _picker.setWrapSelectorWheel(false);
        _picker.setDisplayedValues(_financialYears);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Dialog);
        builder.setTitle(title);
        builder.setView(v);

        builder.setPositiveButton(android.R.string.ok,
            new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    int selectedIndex = _picker.getValue();
                    _listener.onNewFinancialYear(FinancialYearDialogUtil.INSTANCE.indexToFinYear(selectedIndex));
                }
            });
        builder.setNegativeButton(android.R.string.cancel,
            new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    // JUST CLOSE THE DIALOG
                }
            });

        return builder.create();
    }
}
