package au.com.traveller.payrun.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import au.com.traveller.payrun.R;


public abstract class BaseActivity extends AppCompatActivity {
    protected String TAG = BaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void initToolbar(int titleResId) {
        initToolbar(titleResId, true);
    }

    protected void initToolbar(int titleResId, boolean withUpButton) {
        // Set a ToolBar to replace the ActionBar.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET CUSTOM TITLE PER ACTIVITY
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(titleResId);
            if (withUpButton) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(getTag(), "onOptionsItemSelected");
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected String getTag() {
        return TAG;
    }
}