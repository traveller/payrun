package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear

interface FinancialYearRepo {
    val supportedYears: List<FinancialYear>
    val supportedYearsKeys: Array<String>

    val latestSupportedYear: FinancialYear
}
