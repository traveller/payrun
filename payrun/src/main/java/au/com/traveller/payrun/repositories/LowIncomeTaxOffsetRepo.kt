package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.LowIncomeTaxOffset

interface LowIncomeTaxOffsetRepo {

    fun getIncomeOffset(financialYear: FinancialYear): List<LowIncomeTaxOffset>
}