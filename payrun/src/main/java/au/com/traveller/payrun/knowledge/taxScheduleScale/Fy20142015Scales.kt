package au.com.traveller.payrun.knowledge.taxScheduleScale

import au.com.traveller.payrun.models.TaxScaleMargin


/**
 * Uses ATO for 2014/15
 * Rates changed for 2014/15 effective on 1st July 2014.
 */
class Fy20142015Scales : TaxScheduleScalesFactory() {

    override val scale2 = listOf(
            TaxScaleMargin(355.0f, 0.0000f, 0.0000f),
            TaxScaleMargin(395.0f, 0.1900f, 67.4635f),
            TaxScaleMargin(493.0f, 0.2900f, 106.9673f),
            TaxScaleMargin(711.0f, 0.2100f, 67.4642f),
            TaxScaleMargin(1282.0f, 0.3477f, 165.4431f),
            TaxScaleMargin(1538.0f, 0.3450f, 161.9815f),
            TaxScaleMargin(3461.0f, 0.3900f, 231.2123f),
            TaxScaleMargin(3461.0f, 0.4900f, 577.3662f)
    )

    override val scale6 = listOf(
            TaxScaleMargin(355.0f, 0.0000f, 0.0000f),
            TaxScaleMargin(660.0f, 0.1900f, 67.4635f),
            TaxScaleMargin(711.0f, 0.2400f, 100.5087f),
            TaxScaleMargin(826.0f, 0.3777f, 198.4875f),
            TaxScaleMargin(1282.0f, 0.3377f, 165.4429f),
            TaxScaleMargin(1538.0f, 0.3350f, 161.9813f),
            TaxScaleMargin(3461.0f, 0.3800f, 231.2121f),
            TaxScaleMargin(3461.0f, 0.4800f, 577.3660f)
    )

}