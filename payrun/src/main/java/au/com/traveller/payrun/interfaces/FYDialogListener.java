package au.com.traveller.payrun.interfaces;

import au.com.traveller.payrun.enums.FinancialYear;

public interface FYDialogListener
{
    void onNewFinancialYear(FinancialYear newValue);
}
