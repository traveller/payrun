package au.com.traveller.payrun.managers;

import au.com.traveller.payrun.enums.FinancialYear;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.knowledge.PayrunCalculator;

public interface PayrunKnowledgeManager
{
    PayrunCalculator getPayrunCalculator(FinancialYear financialYear, PaymentFrequency frequency);
}
