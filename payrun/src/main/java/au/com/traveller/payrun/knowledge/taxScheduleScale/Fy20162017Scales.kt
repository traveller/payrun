package au.com.traveller.payrun.knowledge.taxScheduleScale

import au.com.traveller.payrun.models.TaxScaleMargin


/**
 * Uses ATO for 2016/17
 * Rates changed for 2016/17 effective on 1st Oct 2016.
 */
class Fy20162017Scales : TaxScheduleScalesFactory() {

    override val scale2 = listOf(
            TaxScaleMargin(355.0f, 0.0000f, 0.0000f),
            TaxScaleMargin(410.0f, 0.1900f, 67.4635f),
            TaxScaleMargin(512.0f, 0.2900f, 108.4923f),
            TaxScaleMargin(711.0f, 0.2100f, 67.4646f),
            TaxScaleMargin(1282.0f, 0.3477f, 165.4435f),
            TaxScaleMargin(1673.0f, 0.3450f, 161.9819f),
            TaxScaleMargin(3461.0f, 0.3900f, 237.2704f),
            TaxScaleMargin(3461.0f, 0.4900f, 583.4242f)
    )

    override val scale6 = listOf(
            TaxScaleMargin(355.0f, 0.0000f, 0.0000f),
            TaxScaleMargin(692.0f, 0.1900f, 67.4635f),
            TaxScaleMargin(711.0f, 0.2400f, 102.0798f),
            TaxScaleMargin(865.0f, 0.3777f, 200.0587f),
            TaxScaleMargin(1282.0f, 0.3377f, 165.4425f),
            TaxScaleMargin(1673.0f, 0.3350f, 161.9810f),
            TaxScaleMargin(3461.0f, 0.3800f, 237.2694f),
            TaxScaleMargin(3461.0f, 0.4800f, 583.4233f)
    )

}