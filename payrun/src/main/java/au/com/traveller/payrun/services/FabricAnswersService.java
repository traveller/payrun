package au.com.traveller.payrun.services;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import au.com.traveller.payrun.App;
import au.com.traveller.payrun.activities.PayrunActivity;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.models.PaymentInput;

public class FabricAnswersService {
    private static final String LOG_TAG = FabricAnswersService.class.toString();


    private final static String PERMANENT_INPUT_CREATED = "Permanent Input Created";
    private final static String CASUAL_INPUT_CREATED = "Casual Input Created";
    private final static String PAYMENT_INPUT_UPDATED = "Payment Input Updated";

    private final static String PERMANENT_PAYRUN_VIEWED = "Permanent Payrun Viewed";
    private final static String CASUAL_PAYRUN_VIEWED = "Casual Payrun Viewed";

    private final static String PAYRUN_DAYS_CHANGED = "Payrun Days Changed";
    private final static String PAYRUN_FREQUENCY_CHANGED = "Payrun Frequency Changed";
    private final static String PAYRUN_INCOME_CHANGED = "Payrun Income Changed";

    private final static String FAVOURITE_ADDED = "Added to Favourites";
    private final static String FAVOURITE_REMOVED = "Removed from Favourites";

    private final static String SETTINGS_VIEWED = "Setting Viewed";
    private final static String ABOUT_VIEWED = "About Viewed";

    public static void paymentInputCreated(PaymentInput pi) {
        if (pi.getEmploymentType() == EmploymentType.PERMANENT) {
            logCustomEvent(new CustomEvent(PERMANENT_INPUT_CREATED));
        } else {
            logCustomEvent(new CustomEvent(CASUAL_INPUT_CREATED));
        }

    }

    public static void paymentInputUpdated() {
        logCustomEvent(new CustomEvent(PAYMENT_INPUT_UPDATED));
    }

    public static void payrunViewed(PaymentInput pi, PaymentFrequency frequency) {
        CustomEvent event;
        if (pi.getEmploymentType() == EmploymentType.PERMANENT) {
            event = new CustomEvent(PERMANENT_PAYRUN_VIEWED);
        } else {
            event = new CustomEvent(CASUAL_PAYRUN_VIEWED);
        }

        event.putCustomAttribute(PayrunActivity.PAYRUN_FREQUENCY_KEY, String.format("%s", frequency));
        logCustomEvent(event);
    }

    public static void payrunDaysChanged() {
        logCustomEvent(new CustomEvent(PAYRUN_DAYS_CHANGED));
    }

    public static void payrunFrequencyChanged() {
        logCustomEvent(new CustomEvent(PAYRUN_FREQUENCY_CHANGED));
    }

    public static void payrunIncomeChanged() {
        logCustomEvent(new CustomEvent(PAYRUN_INCOME_CHANGED));
    }

    public static void payrunAddedToFavs() {
        logCustomEvent(new CustomEvent(FAVOURITE_ADDED));
    }

    public static void payrunRemovedFromFavs() {
        logCustomEvent(new CustomEvent(FAVOURITE_REMOVED));
    }

    public static void payrunSettingsViewed() {
        logCustomEvent(new CustomEvent(SETTINGS_VIEWED));
    }

    public static void payrunAboutViewed() {
        logCustomEvent(new CustomEvent(ABOUT_VIEWED));
    }

    public static void payrunWarning(String message) {
        if (!App.isDevEnv()) {
            Crashlytics.log(Log.WARN, "CUSTOM_WARNING", message);
        } else {
            Log.w("CUSTOM_WARNING", message);
        }
    }

    public static void payrunInfo(String message) {
        if (!App.isDevEnv()) {
            Crashlytics.log(Log.INFO, "CUSTOM_INFO", message);
        } else {
            Log.i("CUSTOM_INFO", message);
        }
    }

    private static void logCustomEvent(CustomEvent event) {
        if (!App.isDevEnv()) {
            Answers.getInstance().logCustom(event);
        }
    }

    public static void reportException(String exceptionMessage) {
        if (!App.isDevEnv()) {
            Crashlytics.logException(new Exception(exceptionMessage));
        } else {
            Log.w(LOG_TAG, exceptionMessage);
        }
    }
}
