package au.com.traveller.payrun.models;

import java.math.BigDecimal;
import java.math.RoundingMode;

import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.FinancialYear;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.utils.ConversionUtil;
import au.com.traveller.payrun.utils.Rounding;

public class PayrunInput {
    private EmploymentType employment_type;
    private FinancialYear financial_year;
    private PaymentFrequency frequency;
    private BigDecimal super_rate;
    private BigDecimal gst_rate;
    private int tax_scale;

    private boolean includes_super;
    private BigDecimal payrun_income;

    private BigDecimal payrun_days;

    public PayrunInput(FinancialYear financialYear, PaymentFrequency frequency, BigDecimal superRate, BigDecimal gstRate, int taxScale) {
        this.financial_year = financialYear;
        this.frequency = frequency;
        this.super_rate = superRate;
        this.gst_rate = gstRate;
        this.tax_scale = taxScale;
    }

    public PayrunInput setPermanentValues(boolean includesSuper, BigDecimal payrunIncome) {
        this.employment_type = EmploymentType.PERMANENT;
        this.includes_super = includesSuper;
        this.payrun_income = payrunIncome;

        if (this.frequency != PaymentFrequency.ANNUALLY) {
            // USE Weekly Schedule TO CALCULATE Payrun
            this.convertToCasual();
        }

        return this;
    }

    public PayrunInput setCasualValues(boolean includesSuper, BigDecimal payrunDays, BigDecimal payrunIncome) {
        this.employment_type = EmploymentType.CASUAL;
        this.includes_super = includesSuper;
        this.payrun_days = payrunDays;
        this.payrun_income = payrunIncome;

        if (frequency == PaymentFrequency.ANNUALLY) {
            // USE Annual Schedule TO CALCULATE Payrun
            this.convertToPermanent();
        }

        return this;
    }

    public EmploymentType getEmploymentType() {
        return this.employment_type;
    }

    public FinancialYear getFinancialYear() {
        return this.financial_year;
    }

    public PaymentFrequency getFrequency() {
        return this.frequency;
    }

    public boolean includesSuper() {
        return this.includes_super;
    }

    public BigDecimal getPayrunIncomeBase() {
        BigDecimal result;
        if (includes_super) {
            // REMOVE Super
            result = ConversionUtil.reduceValueByRate(payrun_income, super_rate);
        } else {
            result = payrun_income;
        }

        return result;
    }

    public BigDecimal getPayrunIncomePackage() {
        BigDecimal result;
        if (includes_super) {
            result = payrun_income;
        } else {
            // ADD Super
            result = ConversionUtil.increaseValueByRate(payrun_income, super_rate);
        }

//        return Rounding.roundHalfUp(result);
        return result;
    }

    public BigDecimal getPayrunDays() {
        return this.payrun_days;
    }

    public BigDecimal getGstRate() {
        return this.gst_rate;
    }

    public int getTaxScale() {
        return tax_scale;
    }

    /**
     * Recalculates Casual To Permanent rates.<br/>
     * Does <strong><u>not</u></strong> change the <code>employment_type</code>.
     */
    protected void convertToPermanent() {
        this.payrun_income = this.payrun_income.multiply(this.frequency.getFrequency());
    }

    /**
     * Recalculates Permanent To Casual rates.<br/>
     * Does <strong><u>not</u></strong> change the <code>employment_type</code>.
     */
    protected void convertToCasual() {
        this.payrun_days = null;
        this.payrun_income = this.payrun_income.divide(this.frequency.getFrequency(), 2, RoundingMode.HALF_UP);
    }
}