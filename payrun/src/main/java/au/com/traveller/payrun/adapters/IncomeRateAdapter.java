package au.com.traveller.payrun.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.PaymentRateType;

public class IncomeRateAdapter extends ArrayAdapter<PaymentRateType>
{
    public IncomeRateAdapter(Context context, int resource)
    {
        super(context, resource, new PaymentRateType[] {PaymentRateType.AS_ENTERED, PaymentRateType.BASE, PaymentRateType.PACKAGE});
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = super.getView(position, convertView, parent);
        this.setCustomView(v, position);
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        View v = super.getDropDownView(position, convertView, parent);
        this.setCustomView(v, position);
        return v;
    }

    private void setCustomView(View v, int position)
    {
        if (v != null)
        {
            CheckedTextView tv = (CheckedTextView) v.findViewById(android.R.id.text1);
            if (tv != null)
            {
                PaymentRateType p = getItem(position);
                tv.setText(PaymentRateType.getStringResId(p));
            }
        }
    }
}
