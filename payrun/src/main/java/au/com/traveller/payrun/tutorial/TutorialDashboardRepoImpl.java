package au.com.traveller.payrun.tutorial;

import android.app.Activity;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.TutorialStep;
import au.com.traveller.payrun.models.Tutorials;
import au.com.traveller.payrun.utils.LocalStorageUtil;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.TourGuide;

public class TutorialDashboardRepoImpl extends TutorialRepoImpl {
    @Override
    public boolean hasCompleted() {
        Tutorials tutorials = LocalStorageUtil.retrieveTutorial();
        return tutorials.isDashboardCompleted();
    }

    @Override
    public void markCompleted() {
        Tutorials tutorials = LocalStorageUtil.retrieveTutorial();
        tutorials.setDashboardCompleted(true);
        LocalStorageUtil.storeTutorials(tutorials);
    }

    @Override
    public TourGuide getNextStep(Activity activity, TutorialStep nextStep) {
        if (TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER != null) {
            try {
                TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER.cleanUp();
            } catch (Exception ex) {
                // JUST IN CASE
            }
        }

        if (nextStep != null && nextStep != TutorialStep.NONE) {
            switch (nextStep) {
                case TAB_CASUAL:
                    return getTabCasual(activity);
                case TAB_PERMANENT:
                    return getTabPermanent(activity);
                case TAB_FAVOURITES:
                    return getTabFavourites(activity);
            }
        }

        return null;
    }

    protected TourGuide getTabCasual(final Activity activity) {
        TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER = init(activity, R.string.tutorial_tab_casual_title, R.string.tutorial_tab_casual);
        TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER.setOverlay(new Overlay().setOnClickListener(v -> getNextStep(activity, TutorialStep.NONE)));
        return TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER;
    }

    protected TourGuide getTabPermanent(Activity activity) {
        TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER = init(activity, R.string.tutorial_tab_permanent_title, R.string.tutorial_tab_permanent);
        TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER.setOverlay(new Overlay().setOnClickListener(v -> getNextStep(activity, TutorialStep.NONE)));
        return TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER;
    }

    protected TourGuide getTabFavourites(Activity activity) {
        TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER = init(activity, R.string.tutorial_tab_favourites_title, R.string.tutorial_tab_favourites);
        TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER.setOverlay(new Overlay().setOnClickListener(v -> getNextStep(activity, TutorialStep.NONE)));
        return TutorialDashboardRepoImpl.TOUR_GUIDE_HANDLER;
    }
}