package au.com.traveller.payrun.knowledge.lowIncome

import java.math.BigDecimal

class LowIncomeEmpty : LowIncomeThresholds {

    override val offsetThresholdMin = BigDecimal(0.00)

    override val offsetThresholdMax = BigDecimal(0.00)

    override val maxTaxOffset = BigDecimal(0)

    override val taxDecreaseRate = BigDecimal(0.00)
}