package au.com.traveller.payrun.interfaces;

import java.math.BigDecimal;

import au.com.traveller.payrun.enums.PaymentFrequency;

public interface DialogValueListener
{
    void onNewPayrunDays(BigDecimal newValue);
    void onNewPayrunIncome(BigDecimal newValue);
    void onNewPayrunFrequency(PaymentFrequency newValue);
}
