package au.com.traveller.payrun.managers.impl

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.enums.PaymentFrequency
import au.com.traveller.payrun.knowledge.PayrunCalculator
import au.com.traveller.payrun.knowledge.payg.PaygAnnualPayrun201415
import au.com.traveller.payrun.knowledge.payg.PaygWeeklyPayrun201415
import au.com.traveller.payrun.managers.PayrunKnowledgeManager
import au.com.traveller.payrun.repositories.LowIncomeTaxOffsetRepoImpl
import au.com.traveller.payrun.repositories.MedicareLevyRepoImpl
import au.com.traveller.payrun.repositories.PaygAnnualScheduleRepoImpl
import au.com.traveller.payrun.repositories.PaygWeeklyScheduleRepoImpl

class PayrunKnowledgeManagerImpl : PayrunKnowledgeManager {

    override fun getPayrunCalculator(financialYear: FinancialYear, frequency: PaymentFrequency): PayrunCalculator {
        return when (financialYear) {
            FinancialYear.FY_2014_15, // 2014/15 RULES AND TAX MARGINS
            FinancialYear.FY_2015_16, // 2015/16 USES SAME RULES AND TAX MARGINS AS 2014/15
            FinancialYear.FY_2016_17, // 2016/17 USES SAME RULES AS 2014/15 // 2016/17 USES TAX MARGINS (Oct 1st 2016)
            FinancialYear.FY_2017_18,  // 2017/18 USES SAME RULES AS 2014/15  // 2017/18 USES TAX MARGINS (Jul 1st 2017)
            FinancialYear.FY_2018_19 ->  // 2018/19 USES SAME RULES AS 2014/15  // 2018/19 USES TAX MARGINS (Jul 1st 2018)
                if (frequency == PaymentFrequency.ANNUALLY) {
                    PaygAnnualPayrun201415(MedicareLevyRepoImpl(), PaygAnnualScheduleRepoImpl(), LowIncomeTaxOffsetRepoImpl())
                } else {
                    PaygWeeklyPayrun201415(MedicareLevyRepoImpl(), PaygWeeklyScheduleRepoImpl())
                }
            else -> PaygAnnualPayrun201415(MedicareLevyRepoImpl(), PaygAnnualScheduleRepoImpl(), LowIncomeTaxOffsetRepoImpl())
        }
    }
}
