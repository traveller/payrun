package au.com.traveller.payrun.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import au.com.traveller.payrun.App;
import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.DashboardPageType;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.models.AppSettings;
import au.com.traveller.payrun.models.PaymentInput;
import au.com.traveller.payrun.models.PaymentInputs;
import au.com.traveller.payrun.models.Tutorials;

public class LocalStorageUtil
{
    private static final String SHARED_PREFS_NAME = "au.com.traveller.payrun";

    protected static SharedPreferences getSharedPreferences(Context context)
    {
        return context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static PaymentInput retrieveAnnualInput(String id)
    {
        PaymentInput result = null;

        List<PaymentInput> paymentInputs = retrieveAnnualInputs();
        for (PaymentInput ai : paymentInputs)
        {
            if (ai.getId().equals(id))
            {
                result = ai;
                break;
            }
        }

        return result;
    }

    public static void storePaymentInputs(PaymentInputs paymentInputs)
    {
        storeObjectAsJson(R.string.shared_annual_input, paymentInputs);
    }

    public static List<PaymentInput> retrieveAnnualInputs()
    {
        List<PaymentInput> result;
        PaymentInputs all = retrieveObjectFromJson(R.string.shared_annual_input, PaymentInputs.class);

        if (all == null)
        {
            result = new ArrayList<PaymentInput>();
        }
        else
        {
            result = all.getItems();
        }

        return result;
    }

    public static Object retrieveAllObjects()
    {
        Object result = retrieveObjectFromJson(R.string.shared_annual_input, Object.class);

        if (result == null)
        {
            result = new Object();
        }

        return result;
    }

    public static List<PaymentInput> retrieveAnnualInputs(DashboardPageType dashboardPageType)
    {
        List<PaymentInput> result = new ArrayList<PaymentInput>();

        if (dashboardPageType != null)
        {
            // FILTER PaymentInputs BY dashboardPageType
            for (PaymentInput pi : retrieveAnnualInputs())
            {
                switch (dashboardPageType)
                {
                    case FAVORITES:
                        if (pi.isFavourite())
                        {
                            result.add(pi);
                        }
                        break;
                    case CASUAL:
                        if (pi.getEmploymentType() == EmploymentType.CASUAL)
                        {
                            result.add(pi);
                        }
                        break;
                    case PERMANENT:
                        if (pi.getEmploymentType() == EmploymentType.PERMANENT)
                        {
                            result.add(pi);
                        }
                        break;
                }
            }
        }

        Collections.sort(result, new PaymentInputComparator());

        return result;
    }

    public static void storeSettings(AppSettings appSettings)
    {
        storeObjectAsJson(R.string.shared_app_settings, appSettings);
    }

    public static AppSettings retrieveSettings()
    {
        AppSettings result = retrieveObjectFromJson(R.string.shared_app_settings, AppSettings.class);
        if (result == null)
        {
            result = new AppSettings();
        }

        return result;
    }

    public static Integer retrieveCurrentModelVersion()
    {
        return retrieveObjectFromJson(R.string.shared_app_model_version, Integer.class);
    }

    public static void storeCurrentModelVersion(int newVersion)
    {
        storeObjectAsJson(R.string.shared_app_model_version, newVersion);
    }

    public static void storeTutorials(Tutorials tutorials)
    {
        storeObjectAsJson(R.string.shared_app_tutorial, tutorials);
    }

    public static Tutorials retrieveTutorial()
    {
        Tutorials result = retrieveObjectFromJson(R.string.shared_app_tutorial, Tutorials.class);
        if (result == null)
        {
            result = new Tutorials();
        }

        return result;
    }

    private static <T> T retrieveObjectFromJson(int resId, Class<T> type)
    {
        T result = null;
        Context context = App.getAppContext();

        if (context != null)
        {
            SharedPreferences sharedPreferences = getSharedPreferences(context);
            String objectJSON = sharedPreferences.getString(context.getString(resId), null);
            if (objectJSON != null)
            {
                result = new Gson().fromJson(objectJSON, type);
            }
        }
        return result;
    }

    private static boolean storeObjectAsJson(int resId, Object object)
    {
        boolean result = false;
        Context context = App.getAppContext();
        String objectJson = new Gson().toJson(object);
        if (objectJson != null)
        {
            if (context != null)
            {
                SharedPreferences sharedPreferences = getSharedPreferences(context);
                SharedPreferences.Editor e = sharedPreferences.edit();
                e.putString(context.getString(resId), objectJson);
                e.commit();
                result = true;
            }
        }

        return result;
    }
}
