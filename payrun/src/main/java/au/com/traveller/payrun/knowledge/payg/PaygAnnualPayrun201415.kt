package au.com.traveller.payrun.knowledge.payg

import au.com.traveller.payrun.knowledge.LowIncomeTaxOffset
import au.com.traveller.payrun.knowledge.MedicareLevy
import au.com.traveller.payrun.knowledge.PaygAnnualSchedule
import au.com.traveller.payrun.knowledge.PayrunCalculator
import au.com.traveller.payrun.models.PayrunInput
import au.com.traveller.payrun.models.PayrunOutput
import au.com.traveller.payrun.repositories.LowIncomeTaxOffsetRepo
import au.com.traveller.payrun.repositories.MedicareLevyRepo
import au.com.traveller.payrun.repositories.PaygAnnualScheduleRepo
import au.com.traveller.payrun.utils.ConversionUtil
import au.com.traveller.payrun.utils.Rounding
import java.math.BigDecimal
import java.math.RoundingMode


class PaygAnnualPayrun201415(
        private val medicareLevyRepo: MedicareLevyRepo,
        private val paygScheduleRepo: PaygAnnualScheduleRepo,
        private val lowIncomeTaxRepo: LowIncomeTaxOffsetRepo
) : PayrunCalculator {

    private lateinit var medicareLevy: MedicareLevy
    private lateinit var paygAnnualSchedule: PaygAnnualSchedule
    private lateinit var lowIncomeTaxOffsets: List<LowIncomeTaxOffset>

    private lateinit var payrunOutput: PayrunOutput

    override fun getPayrunOutput(payrunInput: PayrunInput): PayrunOutput {
        payrunOutput = PayrunOutput(payrunInput)

        medicareLevy = medicareLevyRepo.getMedicareLevy(payrunOutput.financialYear)
        paygAnnualSchedule = paygScheduleRepo.getPaygSchedule(payrunOutput.financialYear)
        lowIncomeTaxOffsets = lowIncomeTaxRepo.getIncomeOffset(payrunOutput.financialYear)

        // AS CLOSED BOUNDARY
        calcSuperContribution()
                .calcAnnualMedicareLevy()
                .calcPayrunPaygContribution()
                .calcPayrunMedicareLevy()
                .calcLowIncomeTaxOffset()
                .calcPayrunTotalTax()
                .calcNetPay()
                .calcInvoiceAmountWithGst()
                .calcGstAmount()

        return payrunOutput
    }

    private fun calcPayrunTotalTax(): PaygAnnualPayrun201415 {
        if (payrunOutput.taxTotal == null) {
            val totalTax = payrunOutput.taxPaygContribution?.add(payrunOutput.taxMedicareLevy)?.subtract(payrunOutput.lowIncomeTaxOffset) // LESS Low Income Tax Offset
            payrunOutput.taxTotal = Rounding.roundHalfUp(totalTax, 2)
        }
        return this
    }

    private fun calcSuperContribution(): PaygAnnualPayrun201415 {
        if (payrunOutput.superContribution == null) {
            payrunOutput.superContribution = payrunOutput.incomePackage?.subtract(payrunOutput.incomeBase)
        }

        return this
    }

    private fun calcPayrunMedicareLevy(): PaygAnnualPayrun201415 {
        if (payrunOutput.taxMedicareLevy == null) {
            val frequencyRate = payrunOutput.frequency.frequency
            val annualLevy = payrunOutput.taxMedicareLevyAnnual
            val payrunLevy = annualLevy?.divide(frequencyRate, RoundingMode.HALF_UP)
            payrunOutput.taxMedicareLevy = Rounding.roundHalfUp(payrunLevy, 2)
        }

        return this
    }

    /**
     * If your taxable income is less than $66,667, you will get the low income tax offset.
     * The maximum tax offset of $445 applies if your taxable income is $37,000 or less.
     * This amount is reduced by 1.5 cents for each dollar over $37,000.
     *
     * @return PayrunOutput
     */
    private fun calcLowIncomeTaxOffset(): PaygAnnualPayrun201415 {
        if (payrunOutput.lowIncomeTaxOffset == null) {
            var totalOffset = BigDecimal.ZERO
            lowIncomeTaxOffsets.map {
                totalOffset = totalOffset.add(it.getAmount(payrunOutput.incomeBase))
            }

            payrunOutput.lowIncomeTaxOffset = totalOffset
        }
        return this
    }

    private fun calcPayrunPaygContribution(): PaygAnnualPayrun201415 {
        if (payrunOutput.taxPaygContribution == null) {
            val taxableIncome = payrunOutput.taxableIncome
            payrunOutput.taxPaygContribution = paygAnnualSchedule.getTaxForAmount(taxableIncome)
        }

        return this
    }

    private fun calcNetPay(): PaygAnnualPayrun201415 {
        if (payrunOutput.netPay == null) {
            val netPay = payrunOutput.incomeBase.subtract(payrunOutput.taxTotal)
            payrunOutput.netPay = netPay
        }
        return this
    }

    private fun calcInvoiceAmountWithGst(): PaygAnnualPayrun201415 {
        payrunOutput.invoiceAmountWithGst = ConversionUtil.increaseValueByRate(payrunOutput.incomePackage, payrunOutput.gstRate)
        return this
    }

    private fun calcGstAmount(): PaygAnnualPayrun201415 {
        payrunOutput.gstAmount = payrunOutput.invoiceAmountWithGst?.subtract(payrunOutput.incomePackage)
        return this
    }

    private fun calcAnnualMedicareLevy(): PaygAnnualPayrun201415 {
        if (payrunOutput.taxMedicareLevyAnnual == null) {
            val annualTaxableIncome = payrunOutput.taxableIncome
            val annualLevy = medicareLevy.getAnnualLevy(annualTaxableIncome)
            payrunOutput.taxMedicareLevyAnnual = Rounding.roundHalfUp(annualLevy)
        }
        return this
    }
}