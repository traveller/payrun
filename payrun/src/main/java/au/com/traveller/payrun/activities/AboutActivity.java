package au.com.traveller.payrun.activities;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.services.FabricAnswersService;

public class AboutActivity extends BaseActivity
{
    protected String LOG_TAG = PayrunActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        FabricAnswersService.payrunAboutViewed();

        this.initToolbar(R.string.title_activity_about);

        this.initResources();
    }

    @Override
    protected String getTag()
    {
        return LOG_TAG;
    }

    private void initResources()
    {
        TextView icons = (TextView) findViewById(R.id.about_credits_elegant_themes);
        // ENABLE CLICKING ON INTERNAL a>href URL (INSIDE OF A <string> RESOURCE)
        icons.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
