package au.com.traveller.payrun.interfaces;

import java.math.BigDecimal;

import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.models.PaymentInput;

public interface DashboardItemListener
{
    void onPaymentInputEdit(final PaymentInput paymentInput);
    void onPaymentInputPayrun(final PaymentInput paymentInput, BigDecimal payrunDays, PaymentFrequency frequency);
    void onPaymentInputPinned(final PaymentInput paymentInput);
}
