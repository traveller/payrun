package au.com.traveller.payrun.models

import au.com.traveller.payrun.utils.Rounding
import java.math.BigDecimal
import java.math.RoundingMode

class TaxScaleMargin(earningLessThan: Float, columnA: Float, columnB: Float) {
    val earningLessThan: BigDecimal
    val columnA: BigDecimal
    val columnB: BigDecimal

    init {
        this.earningLessThan = build(earningLessThan)
        this.columnA = build(columnA, 4)
        this.columnB = build(columnB, 4)
    }

    /**
     * Calculates total income tax (PAYG + MedicareLevy)
     * @return Total Income Tax for margin
     */
    fun getTotalTaxInclMedicareLevy(amount: BigDecimal): BigDecimal {
        val y: BigDecimal = amount
                .multiply(columnA)
                .subtract(columnB)

        return Rounding.roundHalfUp(y)
    }

    private fun build(value: Float, scale: Int = 0): BigDecimal {
        return BigDecimal(value.toString()).setScale(scale, RoundingMode.FLOOR)
    }
}
