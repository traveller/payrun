package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.PaygWeeklySchedule
import au.com.traveller.payrun.knowledge.TaxScheduleScales
import au.com.traveller.payrun.knowledge.payg.PaygWeeklySchedule201415
import au.com.traveller.payrun.knowledge.taxScheduleScale.Fy20142015Scales
import au.com.traveller.payrun.knowledge.taxScheduleScale.Fy20162017Scales
import au.com.traveller.payrun.knowledge.taxScheduleScale.Fy20172018Scales
import au.com.traveller.payrun.knowledge.taxScheduleScale.Fy20182019Scales
import au.com.traveller.payrun.knowledge.taxScheduleScale.ZeroValuesScales


class PaygWeeklyScheduleRepoImpl : PaygWeeklyScheduleRepo {

    override fun getPaygTaxSchedule(financialYear: FinancialYear): PaygWeeklySchedule {
        val taxScales = when (financialYear) {
            FinancialYear.FY_2014_15, FinancialYear.FY_2015_16 -> Fy20142015Scales()
            FinancialYear.FY_2016_17 -> Fy20162017Scales()
            FinancialYear.FY_2017_18 -> Fy20172018Scales()
            FinancialYear.FY_2018_19 -> Fy20182019Scales()
            else -> ZeroValuesScales()
        }
        return PaygWeeklySchedule201415(taxScales.getSchedule())
    }
}