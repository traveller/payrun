package au.com.traveller.payrun.knowledge.taxScheduleScale

import au.com.traveller.payrun.knowledge.TaxScheduleScales
import au.com.traveller.payrun.models.TaxScale
import au.com.traveller.payrun.models.TaxScaleMargin
import au.com.traveller.payrun.models.TaxSchedule

abstract class TaxScheduleScalesFactory : TaxScheduleScales {

    /**
     * Scale 2: Where the employee claimed the tax-free threshold
     * in Tax file number declaration
     */
    protected abstract val scale2: List<TaxScaleMargin>

    /**
     * Scale 6: Where the employee claimed the HALF exemption
     * from Medicare levy in Medicare levy variation declaration
     */
    protected abstract val scale6: List<TaxScaleMargin>

    override fun getSchedule(): TaxSchedule {
        val taxScales = listOf(TaxScale(2, scale2), TaxScale(6, scale6))

        return TaxSchedule(taxScales)
    }
}