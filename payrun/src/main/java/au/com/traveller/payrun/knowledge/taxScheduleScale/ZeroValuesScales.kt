package au.com.traveller.payrun.knowledge.taxScheduleScale

import au.com.traveller.payrun.models.TaxScaleMargin


/**
 * Provides zero-values for all scales
 * Used when financial year is not supported
 */
class ZeroValuesScales : TaxScheduleScalesFactory() {

    override val scale2 = listOf(TaxScaleMargin(0.00f, 0.0000f, 0.0000f))

    override val scale6 = listOf(TaxScaleMargin(0.00f, 0.0000f, 0.0000f))

}