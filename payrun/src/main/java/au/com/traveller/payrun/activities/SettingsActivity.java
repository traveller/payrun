package au.com.traveller.payrun.activities;

import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.adapters.IncomeRateAdapter;
import au.com.traveller.payrun.enums.PaymentRateType;
import au.com.traveller.payrun.models.AppSettings;
import au.com.traveller.payrun.services.FabricAnswersService;
import au.com.traveller.payrun.utils.LocalStorageUtil;
import au.com.traveller.payrun.utils.ViewUtil;

public class SettingsActivity extends BaseActivity
{
    protected String LOG_TAG = SettingsActivity.class.getSimpleName();

    private Switch _toggleWeeklyCasual;
    private EditText _daysWeeklyCasual;
    private Switch _toggleFortnightlyCasual;
    private EditText _daysFortnightlyCasual;
    private Switch _toggleMonthlyCasual;
    private EditText _daysMonthlyCasual;
    private Switch _toggleAnnuallyCasual;
    private EditText _daysAnnuallyCasual;

    private Switch _toggleWeeklyPermanent;
    private Switch _toggleFortnightlyPermanent;
    private Switch _toggleMonthlyPermanent;
    private Switch _toggleAnnuallyPermanent;

    private Spinner _casualRateType;
    private IncomeRateAdapter _casualRateAdapter;
    private Spinner _permanentRateType;
    private IncomeRateAdapter _permanentRateAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        FabricAnswersService.payrunSettingsViewed();

        this.initToolbar(R.string.title_activity_settings);

        this.initResources();
        this.populateCasualRates();
        this.populateForm();
    }

    @Override
    public void onBackPressed()
    {
        if (saveSettings())
        {
            super.onBackPressed();
        }
    }

    @Override
    protected String getTag()
    {
        return LOG_TAG;
    }

    private void initResources()
    {
        _toggleWeeklyCasual         = (Switch) findViewById(R.id.toggle_payrun_casual_weekly_show);
        _toggleFortnightlyCasual    = (Switch) findViewById(R.id.toggle_payrun_casual_fortnightly_show);
        _toggleMonthlyCasual        = (Switch) findViewById(R.id.toggle_payrun_casual_monthly_show);
        _toggleAnnuallyCasual       = (Switch) findViewById(R.id.toggle_payrun_casual_annually_show);

        _daysWeeklyCasual           = (EditText) findViewById(R.id.toggle_payrun_casual_weekly_days);
        _daysFortnightlyCasual      = (EditText) findViewById(R.id.toggle_payrun_casual_fortnightly_days);
        _daysMonthlyCasual          = (EditText) findViewById(R.id.toggle_payrun_casual_monthly_days);
        _daysAnnuallyCasual         = (EditText) findViewById(R.id.toggle_payrun_casual_annually_days);

        _toggleWeeklyPermanent      = (Switch) findViewById(R.id.toggle_payrun_permanent_weekly_show);
        _toggleFortnightlyPermanent = (Switch) findViewById(R.id.toggle_payrun_permanent_fortnightly_show);
        _toggleMonthlyPermanent     = (Switch) findViewById(R.id.toggle_payrun_permanent_monthly_show);
        _toggleAnnuallyPermanent    = (Switch) findViewById(R.id.toggle_payrun_permanent_annually_show);

        _casualRateType             = (Spinner) findViewById(R.id.spinner_settings_casual_super);
        _permanentRateType          = (Spinner) findViewById(R.id.spinner_settings_permanent_super);
    }

    private void populateCasualRates()
    {
        _casualRateAdapter = new IncomeRateAdapter(SettingsActivity.this, android.R.layout.simple_spinner_dropdown_item);
        _casualRateType.setAdapter(_casualRateAdapter);

        _permanentRateAdapter = new IncomeRateAdapter(SettingsActivity.this, android.R.layout.simple_spinner_dropdown_item);
        _permanentRateType.setAdapter(_permanentRateAdapter);
    }

    private void populateForm()
    {
        AppSettings appSettings = LocalStorageUtil.retrieveSettings();

        _toggleWeeklyCasual.setChecked(appSettings.isWeeklyPayrunCasualVisible());
        _daysWeeklyCasual.setText(ViewUtil.formatPayrunDaysForEdit(appSettings.getWeeklyPayrunDays()));

        _toggleFortnightlyCasual.setChecked(appSettings.isFortnightlyPayrunCasualVisible());
        _daysFortnightlyCasual.setText(ViewUtil.formatPayrunDaysForEdit(appSettings.getFortnightlyPayrunDays()));

        _toggleMonthlyCasual.setChecked(appSettings.isMonthlyPayrunCasualVisible());
        _daysMonthlyCasual.setText(ViewUtil.formatPayrunDaysForEdit(appSettings.getMonthlyPayrunDays()));

        _toggleAnnuallyCasual.setChecked(appSettings.isAnnuallyPayrunCasualVisible());
        _daysAnnuallyCasual.setText(ViewUtil.formatPayrunDaysForEdit(appSettings.getAnnuallyPayrunDays()));

        _toggleWeeklyPermanent.setChecked(appSettings.isPermWeeklyPayrunVisible());
        _toggleFortnightlyPermanent.setChecked(appSettings.isPermFortnightlyPayrunVisible());
        _toggleMonthlyPermanent.setChecked(appSettings.isPermMonthlyPayrunVisible());
        _toggleAnnuallyPermanent.setChecked(appSettings.isPermAnnuallyPayrunVisible());

        if (_casualRateAdapter != null)
        {
            int casualRatePosition = _casualRateAdapter.getPosition(appSettings.getCasualRateType());
            _casualRateType.setSelection(casualRatePosition);
        }

        if (_permanentRateAdapter != null)
        {
            int permanentRatePosition = _permanentRateAdapter.getPosition(appSettings.getPermanentRateType());
            _permanentRateType.setSelection(permanentRatePosition);
        }
    }

    private boolean saveSettings()
    {
        boolean result = false;

        AppSettings appSettings = LocalStorageUtil.retrieveSettings();

        appSettings.setWeeklyPayrunCasualVisible(_toggleWeeklyCasual.isChecked());
        appSettings.setWeeklyPayrunDays(ViewUtil.getValueAsBigDecimal(_daysWeeklyCasual.getText().toString()));

        appSettings.setFortnightlyPayrunCasualVisible(_toggleFortnightlyCasual.isChecked());
        appSettings.setFortnightlyPayrunDays(ViewUtil.getValueAsBigDecimal(_daysFortnightlyCasual.getText().toString()));

        appSettings.setMonthlyPayrunCasualVisible(_toggleMonthlyCasual.isChecked());
        appSettings.setMonthlyPayrunDays(ViewUtil.getValueAsBigDecimal(_daysMonthlyCasual.getText().toString()));

        appSettings.setAnnuallyPayrunCasualVisible(_toggleAnnuallyCasual.isChecked());
        appSettings.setAnnuallyPayrunDays(ViewUtil.getValueAsBigDecimal(_daysAnnuallyCasual.getText().toString()));

        appSettings.setPermWeeklyPayrunVisible(_toggleWeeklyPermanent.isChecked());
        appSettings.setPermFortnightlyPayrunVisible(_toggleFortnightlyPermanent.isChecked());
        appSettings.setPermMonthlyPayrunVisible(_toggleMonthlyPermanent.isChecked());
        appSettings.setPermAnnuallyPayrunVisible(_toggleAnnuallyPermanent.isChecked());

        appSettings.setCasualRateTypeId(((PaymentRateType) _casualRateType.getSelectedItem()).getId());
        appSettings.setPermanentRateType(((PaymentRateType) _permanentRateType.getSelectedItem()).getId());

        if (appSettings.isValid())
        {
            LocalStorageUtil.storeSettings(appSettings);
            result = true;
        }
        else
        {
            showError(getString(R.string.settings_validation_error));
        }

        return result;
    }

    private void showError(String message)
    {
        AlertDialog dialog = ViewUtil.buildDialog(SettingsActivity.this, null, message);
        if (dialog != null && !isFinishing())
        {
            dialog.show();
        }
    }
}
