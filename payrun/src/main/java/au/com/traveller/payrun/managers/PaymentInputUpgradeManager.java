package au.com.traveller.payrun.managers;

import com.google.gson.internal.LinkedTreeMap;

import au.com.traveller.payrun.models.PaymentInput;

public interface PaymentInputUpgradeManager
{
    PaymentInput upgradeToVersion(LinkedTreeMap paymentInput, int toVersion);
}
