package au.com.traveller.payrun.utils;

import java.util.Comparator;

import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.models.PaymentInput;

public class PaymentInputComparator implements Comparator<PaymentInput>
{
    @Override
    public int compare(PaymentInput paymentInput, PaymentInput t1)
    {
        if (paymentInput.getEmploymentType() == EmploymentType.CASUAL)
        {
            return paymentInput.getDailyRateEntered().compareTo(t1.getDailyRateEntered());
        }
        else
        {
            return paymentInput.getAnnualIncome().compareTo(t1.getAnnualIncome());
        }
    }
}
