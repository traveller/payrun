package au.com.traveller.payrun.enums;

import java.math.BigDecimal;

public enum PaymentFrequency
{
    NONE("none", 0),
    ANNUALLY("annually", 1),
    QUARTERLY("quarterly", 4),
    MONTHLY("monthly", 12),
    FORTNIGHTLY("fortnightly", 26),
    WEEKLY("weekly", 52);

    private String frequency_name;
    private BigDecimal frequency;

    PaymentFrequency(String frequencyName, int frequency)
    {
        this.frequency_name = frequencyName;
        this.frequency      = new BigDecimal(frequency);
    }

    public String getFrequencyName()
    {
        return frequency_name;
    }

    public BigDecimal getFrequency()
    {
        return frequency;
    }

    public static PaymentFrequency getByFrequencyName(String frequencyName)
    {
        if (frequencyName == null)
        {
            frequencyName = "";
        }

        if (frequencyName.equals("weekly"))
        {
            return WEEKLY;
        }
        else if (frequencyName.equals("fortnightly"))
        {
            return FORTNIGHTLY;
        }
        else if (frequencyName.equals("monthly"))
        {
            return MONTHLY;
        }
        else if (frequencyName.equals("quarterly"))
        {
            return QUARTERLY;
        }
        else if (frequencyName.equals("annually"))
        {
            return ANNUALLY;
        }

        return NONE;
    }
}
