package au.com.traveller.payrun.policies

import au.com.traveller.payrun.models.TaxScale
import au.com.traveller.payrun.models.TaxScaleMargin
import au.com.traveller.payrun.models.TaxSchedule
import au.com.traveller.payrun.utils.Rounding
import java.math.BigDecimal

class KnowledgePaygPolicy {

    companion object {
        @JvmStatic
        fun getScheduleScale(taxSchedule: TaxSchedule, scaleNumber: Int): TaxScale? {
            return taxSchedule.taxScales.firstOrNull { scaleNumber == it.scaleNumber }
        }

        @JvmStatic
        fun getMarginForScale(taxScale: TaxScale, amount: BigDecimal): TaxScaleMargin {
            // taxScale.margins IS IMMUTABLE LIST
            val margins = taxScale.margins.toMutableList()

            // REMOVE LAST ELEMENT
            val result = margins.last()
            margins.remove(result)

            // ITERATE LIST IN REVERSE ORDER
            margins.reverse()

            // RETURN LAST MARGIN GREATER THAN AMOUNT (OR LAST ITEM IF NOTHING FOUND)
            return margins.findLast { it.earningLessThan > amount } ?: result
        }

        @JvmStatic
        fun ignoreCentsAndAdd99cents(taxableAmount: BigDecimal): BigDecimal {
            // IGNORE CENTS
            var result = Rounding.floor(taxableAmount, 2)

            // ADD 0.99
            result = result.add(BigDecimal("0.99"))

            return result
        }

        @JvmStatic
        fun roundToTheNearestDollar(amount: BigDecimal): BigDecimal {
            return amount.setScale(2, BigDecimal.ROUND_FLOOR)
        }
    }

}
