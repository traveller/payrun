package au.com.traveller.payrun.knowledge.taxScheduleScale

import au.com.traveller.payrun.models.TaxScaleMargin


/**
 * Uses ATO for 2018/19
 * Rates changed for 2018/19 effective on 1st July 2018.
 *
 * @return TaxSchedule
 */
class Fy20182019Scales : TaxScheduleScalesFactory() {

    override val scale2 = listOf(
            TaxScaleMargin(355.0f, 0.0000f, 0.0000f),
            TaxScaleMargin(422.0f, 0.1900f, 67.4635f),
            TaxScaleMargin(528.0f, 0.2900f, 109.7327f),
            TaxScaleMargin(711.0f, 0.2100f, 67.4635f),
            TaxScaleMargin(1282.0f, 0.3477f, 165.4423f),
            TaxScaleMargin(1730.0f, 0.3450f, 161.9808f),
            TaxScaleMargin(3461.0f, 0.3900f, 239.8654f),
            TaxScaleMargin(3461.0f, 0.4700f, 516.7885f)
    )

    override val scale6 = listOf(
            TaxScaleMargin(355.0f, 0.0000f, 0.0000f),
            TaxScaleMargin(711.0f, 0.1900f, 67.4635f),
            TaxScaleMargin(713.0f, 0.3277f, 165.4423f),
            TaxScaleMargin(891.0f, 0.3777f, 201.1048f),
            TaxScaleMargin(1282.0f, 0.3377f, 165.4425f),
            TaxScaleMargin(1730.0f, 0.3350f, 161.9810f),
            TaxScaleMargin(3461.0f, 0.3800f, 239.8656f),
            TaxScaleMargin(3461.0f, 0.4600f, 516.7887f)
    )

}