package au.com.traveller.payrun.knowledge

import au.com.traveller.payrun.models.PayrunInput
import au.com.traveller.payrun.models.PayrunOutput


interface PayrunCalculator {
    fun getPayrunOutput(payrunInput: PayrunInput): PayrunOutput
}
