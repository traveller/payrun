package au.com.traveller.payrun.dialogs;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.interfaces.DialogValueListener;
import au.com.traveller.payrun.widgets.NumberPicker;

public class PayrunFrequencyDialogFragment extends DialogFragment
{
    private NumberPicker _picker;
    private DialogValueListener _listener;
    private String[] _frequencies;

    public static PayrunFrequencyDialogFragment newInstance(int title, PaymentFrequency frequency, DialogValueListener listener)
    {
        PayrunFrequencyDialogFragment f = new PayrunFrequencyDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putString("initial_payrun_frequency", frequency.getFrequencyName());
        f._listener = listener;
        f.setArguments(args);
        return f;
    }

    public PayrunFrequencyDialogFragment()
    {
        // Required empty public constructor
        _frequencies = new String[] {
                        PaymentFrequency.WEEKLY.getFrequencyName(),
                        PaymentFrequency.FORTNIGHTLY.getFrequencyName(),
                        PaymentFrequency.MONTHLY.getFrequencyName(),
                        PaymentFrequency.ANNUALLY.getFrequencyName()
        };
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        int title = getArguments().getInt("title");
        String initialFrequencyName = getArguments().getString("initial_payrun_frequency");
        PaymentFrequency initialFrequency = PaymentFrequency.getByFrequencyName(initialFrequencyName);

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_payrun_days, null);
        _picker = (NumberPicker) v.findViewById(R.id.numberPicker_payrun_days);

        // DISABLE USER EDIT (SOFT-KEYBOARD)
        _picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        _picker.setMinValue(0);
        _picker.setMaxValue(_frequencies.length - 1);
        _picker.setValue(resolveToInt(initialFrequency));
        _picker.setWrapSelectorWheel(false);
        _picker.setDisplayedValues(_frequencies);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Dialog);
        builder.setTitle(title);
        builder.setView(v);

        builder.setPositiveButton(android.R.string.ok,
            new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    int selectedIndex = _picker.getValue();
                    _listener.onNewPayrunFrequency(resolve(selectedIndex));
                }
            });
        builder.setNegativeButton(android.R.string.cancel,
            new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    // JUST CLOSE THE DIALOG
                }
            });

        return builder.create();
    }

    private PaymentFrequency resolve(int selectedIndex)
    {
        PaymentFrequency result = PaymentFrequency.NONE;

        switch (selectedIndex)
        {
            case 0:
                result = PaymentFrequency.WEEKLY;
                break;
            case 1:
                result = PaymentFrequency.FORTNIGHTLY;
                break;
            case 2:
                result = PaymentFrequency.MONTHLY;
                break;
            case 3:
                result = PaymentFrequency.ANNUALLY;
                break;
        }

        return result;
    }

    private int resolveToInt(PaymentFrequency selectedFrequency)
    {
        int result = -1;
        switch (selectedFrequency)
        {
            case WEEKLY:
                result = 0;
                break;
            case FORTNIGHTLY:
                result = 1;
                break;
            case MONTHLY:
                result = 2;
                break;
            case ANNUALLY:
                result = 3;
                break;
        }

        return result;
    }
}
