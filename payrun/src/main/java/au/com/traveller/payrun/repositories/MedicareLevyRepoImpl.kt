package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.MedicareLevy
import au.com.traveller.payrun.knowledge.medicareLevy.MedicareLevy201415
import au.com.traveller.payrun.models.MedicareLevySchedule
import java.math.BigDecimal

class MedicareLevyRepoImpl : MedicareLevyRepo {

    override fun getMedicareLevy(financialYear: FinancialYear): MedicareLevy {
        return when (financialYear) {
            FinancialYear.FY_2014_15,
            FinancialYear.FY_2015_16,
            FinancialYear.FY_2016_17,
            FinancialYear.FY_2017_18 -> MedicareLevy201415(initMedicareLevy20142015())
            FinancialYear.FY_2018_19 -> MedicareLevy201415(initMedicareLevy20182019())
            else -> MedicareLevy201415(initMedicareLevyEmpty())
        }
    }

    private fun initMedicareLevy20142015(): MedicareLevySchedule {
        return MedicareLevySchedule(
                BigDecimal(20896),
                BigDecimal(0.01),
                BigDecimal(26121),
                BigDecimal(0.02)
        )
    }

    private fun initMedicareLevy20182019(): MedicareLevySchedule {
        return MedicareLevySchedule(
                BigDecimal(21980),
                BigDecimal(0.01),
                BigDecimal(27475),
                BigDecimal(0.02)
        )
    }

    /**
     * Used for non-supported [FinancialYear] or
     * [FinancialYear] that have been phased out of support (used to be supported)
     *
     * @return zero-value Schedule
     */
    private fun initMedicareLevyEmpty(): MedicareLevySchedule {
        return MedicareLevySchedule(
                BigDecimal(0.00),
                BigDecimal(0.00),
                BigDecimal(0.00),
                BigDecimal(0.00)
        )
    }
}
