package au.com.traveller.payrun.widgets;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import au.com.traveller.payrun.R;

public class NumberPicker extends android.widget.NumberPicker
{
    public NumberPicker(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    public void addView(View child)
    {
        super.addView(child);
        updateView(child);
    }

    @Override
    public void addView(View child, int index, android.view.ViewGroup.LayoutParams params)
    {
        super.addView(child, index, params);
        updateView(child);
    }

    @Override
    public void addView(View child, android.view.ViewGroup.LayoutParams params)
    {
        super.addView(child, params);
        updateView(child);
    }

    private void updateView(View view)
    {
        if (view instanceof EditText)
        {
            float density = getResources().getDisplayMetrics().density;
            float textSize = getResources().getDimension(R.dimen.text_l) / density;
            ((EditText) view).setTextSize(textSize);
            ((EditText) view).setTextColor(Color.parseColor("#4f5d73"));
//            ((EditText) view).setTextColor(getContext().getColor(R.color.brand_primary_color));
        }
    }
}