package au.com.traveller.payrun.policies;

import java.math.BigDecimal;

import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.models.AppSettings;
import au.com.traveller.payrun.models.PaymentInput;
import au.com.traveller.payrun.models.PayrunOutput;

public class PayrunPolicy
{
    private PaymentInput _paymentInput;

    public PayrunPolicy(PaymentInput paymentInput)
    {
        this._paymentInput = paymentInput;
    }

    public boolean isPermanent()
    {
        return this._paymentInput.getEmploymentType() == EmploymentType.PERMANENT;
    }

    public boolean isCasual()
    {
        return this._paymentInput.getEmploymentType() == EmploymentType.CASUAL;
    }

    public static boolean useAnnualCalc(EmploymentType employmentType, PaymentFrequency frequency)
    {
        return employmentType == EmploymentType.PERMANENT ||
                (employmentType == EmploymentType.CASUAL && frequency == PaymentFrequency.ANNUALLY);
    }

    public static boolean payrunIncomeIncludesSuper(PaymentInput paymentInput, AppSettings appSettings)
    {
        boolean result = false;

        if (paymentInput.getEmploymentType() == EmploymentType.CASUAL)
        {
            switch (appSettings.getCasualRateType())
            {
                case BASE:
                    result = false;
                    break;
                case PACKAGE:
                    result = true;
                    break;
                case AS_ENTERED:
                    result = paymentInput.rateIncludesSuper();
                    break;
            }
        }
        else
        {
            switch (appSettings.getPermanentRateType())
            {
                case BASE:
                    result = false;
                    break;
                case PACKAGE:
                    result = true;
                    break;
                case AS_ENTERED:
                    result = paymentInput.rateIncludesSuper();
                    break;
            }
        }
        return result;
    }

    public static BigDecimal payrunOutputIncomeValue(PayrunOutput payrunOutput, AppSettings appSettings)
    {
        BigDecimal result = null;

        if (payrunOutput.getEmploymentType() == EmploymentType.CASUAL)
        {
            switch (appSettings.getCasualRateType())
            {
                case BASE:
                    result = payrunOutput.getIncomeBase();
                    break;
                case PACKAGE:
                    result = payrunOutput.getIncomePackage();
                    break;
                case AS_ENTERED:
                    if (payrunOutput.includesSuper())
                    {
                        result = payrunOutput.getIncomePackage();
                    }
                    else
                    {
                        result = payrunOutput.getIncomeBase();
                    }
                    break;
            }
        }
        else
        {
            switch (appSettings.getPermanentRateType())
            {
                case BASE:
                    result = payrunOutput.getIncomeBase();
                    break;
                case PACKAGE:
                    result = payrunOutput.getIncomePackage();
                    break;
                case AS_ENTERED:
                    if (payrunOutput.includesSuper())
                    {
                        result = payrunOutput.getIncomePackage();
                    }
                    else
                    {
                        result = payrunOutput.getIncomeBase();
                    }
                    break;
            }
        }
        return result;
    }
}