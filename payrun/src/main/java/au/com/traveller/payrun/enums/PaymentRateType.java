package au.com.traveller.payrun.enums;

import au.com.traveller.payrun.R;

public enum PaymentRateType
{
    AS_ENTERED(1),
    BASE(2),
    PACKAGE(3);

    private int id;

    PaymentRateType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static PaymentRateType getById(int id)
    {
        PaymentRateType result = AS_ENTERED;

        switch (id)
        {
            case 1:
                result = AS_ENTERED;
                break;
            case 2:
                result = BASE;
                break;
            case 3:
                result = PACKAGE;
                break;
        }

        return result;
    }

    public static int getStringResId(PaymentRateType value)
    {
        int result;

        switch (value)
        {
            case AS_ENTERED:
                result = R.string.label_payment_rate_as_entered;
                break;
            case BASE:
                result = R.string.label_payment_rate_base;
                break;
            case PACKAGE:
                result = R.string.label_payment_rate_package;
                break;

            default:
                result = R.string.label_payment_rate_as_entered;
                break;
        }

        return result;
    }
}
