package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.PaygWeeklySchedule


interface PaygWeeklyScheduleRepo {
    fun getPaygTaxSchedule(financialYear: FinancialYear): PaygWeeklySchedule
}