package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.PaygAnnualSchedule


interface PaygAnnualScheduleRepo {
    fun getPaygSchedule(financialYear: FinancialYear): PaygAnnualSchedule
}