package au.com.traveller.payrun.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.math.BigDecimal;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.interfaces.DialogValueListener;
import au.com.traveller.payrun.utils.ViewUtil;

public class PayrunIncomeDialogFragment extends DialogFragment
{
    private EditText _payrunIncomeText;
    private DialogValueListener _listener;

    public static PayrunIncomeDialogFragment newInstance(int title, BigDecimal payrunIncome, DialogValueListener listener)
    {
        PayrunIncomeDialogFragment f = new PayrunIncomeDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putDouble("initial_payrun_income", payrunIncome.doubleValue());
        f._listener = listener;
        f.setArguments(args);
        return f;
    }

    public PayrunIncomeDialogFragment()
    {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        int title = getArguments().getInt("title");
        double initial_payrun_income = getArguments().getDouble("initial_payrun_income");
        BigDecimal initialIncome = new BigDecimal(initial_payrun_income);

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_payrun_income, null, false);
        _payrunIncomeText = (EditText) v.findViewById(R.id.edit_text_payrun_income);
        _payrunIncomeText.setText(ViewUtil.formatBigDecimalForEdit(initialIncome));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Dialog);
        builder.setTitle(title);
        builder.setView(v);

        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        _listener.onNewPayrunIncome(new BigDecimal(_payrunIncomeText.getText().toString()));
                    }
                });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        // JUST CLOSE THE DIALOG
                    }
                });

        return builder.create();
    }
}
