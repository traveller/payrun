package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import java.util.*

class FinancialYearRepoImpl : FinancialYearRepo {

    override val supportedYears: List<FinancialYear>
        get() {
            val result = ArrayList<FinancialYear>()
            result.add(FinancialYear.FY_2014_15)
            result.add(FinancialYear.FY_2015_16)
            result.add(FinancialYear.FY_2016_17)
            result.add(FinancialYear.FY_2017_18)
            result.add(FinancialYear.FY_2018_19)

            return result
        }

    override val supportedYearsKeys: Array<String>
        get() {
            val keys = ArrayList<String>(supportedYears.size)
            for (finYear in supportedYears) {
                keys.add(finYear.key)
            }
            //val result = arrayOfNulls<String>(keys.size)

            return keys.toTypedArray()
        }

    override val latestSupportedYear: FinancialYear
        get() {
            val years = supportedYears
            return years[years.size - 1]
        }
}