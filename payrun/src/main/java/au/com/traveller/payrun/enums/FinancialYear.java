package au.com.traveller.payrun.enums;

import java.math.BigDecimal;

public enum FinancialYear {
    NONE("NONE", 0, 0, 0f, 0f),
    FY_2014_15("2014/15", 2014, 2015, 9.50f, 10.0f),
    FY_2015_16("2015/16", 2015, 2016, 9.50f, 10.0f),
    FY_2016_17("2016/17", 2016, 2017, 9.50f, 10.0f),
    FY_2017_18("2017/18", 2017, 2018, 9.50f, 10.0f),
    FY_2018_19("2018/19", 2018, 2019, 9.50f, 10.0f),
    FY_2019_20("2019/20", 2019, 2020, 9.50f, 10.0f);

    private String key;
    private int yearStart;
    private int yearEnd;
    private BigDecimal minSuper;
    private BigDecimal gst;

    FinancialYear(String key, int yearStart, int yearEnd, float minSuper, float gst) {
        this.key = key;
        this.yearStart = yearStart;
        this.yearEnd = yearEnd;
        this.minSuper = new BigDecimal(minSuper);
        this.gst = new BigDecimal(gst);
    }

    public static FinancialYear getByKey(String financialYearKey) {
        for (FinancialYear fy : values()) {
            if (fy.getKey().equals(financialYearKey)) {
                return fy;
            }
        }
        return NONE;
    }

    public String getKey() {
        return key;
    }

    public int getYearStart() {
        return yearStart;
    }

    public int getYearEnd() {
        return yearEnd;
    }

    public BigDecimal getMinSuper() {
        return minSuper;
    }

    public BigDecimal getGst() {
        return gst;
    }
}