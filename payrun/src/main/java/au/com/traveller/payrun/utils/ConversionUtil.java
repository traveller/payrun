package au.com.traveller.payrun.utils;

import java.math.BigDecimal;

public class ConversionUtil {
    /**
     * Returns <code>valueWithRate</code> reduced by the <code>rate</code> (rounded to 4 decimal places)
     *
     * @param valueWithRate value with rate, i.e. Income with Super
     * @param rate          Value between 0 and 1, i.e. Super=0.095 (as 9.5%)
     * @return value reduced by rate, i.e. Income without Super
     */
    public static BigDecimal reduceValueByRate(BigDecimal valueWithRate, BigDecimal rate) {
        BigDecimal newRate = BigDecimal.ONE.add(rate);
        if (valueWithRate != null) {
            return valueWithRate.divide(newRate, 4, BigDecimal.ROUND_HALF_UP);
        }
        return null;
    }

    /**
     * Returns <code>valueWithoutRate</code> increased by the <code>rate</code>
     *
     * @param valueWithoutRate value with rate, i.e. Income without Super
     * @param rate             Value between 0 and 1, i.e. Super=0.095 (as 9.5%)
     * @return value increased by rate, i.e. Income with Super
     */
    public static BigDecimal increaseValueByRate(BigDecimal valueWithoutRate, BigDecimal rate) {
        if (valueWithoutRate != null) {
            BigDecimal newRate = BigDecimal.ONE.add(rate);
            return valueWithoutRate.multiply(newRate);
        }
        return null;
    }
}
