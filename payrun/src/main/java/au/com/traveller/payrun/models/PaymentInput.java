package au.com.traveller.payrun.models;

import java.math.BigDecimal;
import java.util.UUID;

import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.FinancialYear;
import au.com.traveller.payrun.utils.Rounding;
import au.com.traveller.payrun.utils.Validator;

public class PaymentInput
{
    private String id;
    private int version;
    private int employment_type_id;
    private BigDecimal daily_rate_entered;
    private BigDecimal annual_income;
    private BigDecimal super_rate;
    private BigDecimal gst_rate;
    private boolean rate_includes_super;
    private boolean favourite;
    private String financial_year_key;
    private int tax_schedule_scale;

    public static PaymentInput Factory(String id, int employmentTypeId, BigDecimal dailyRate, BigDecimal annualIncome, boolean rateIncludesSuper, BigDecimal superRate, BigDecimal gstRate, String financialYearKey)
    {
        PaymentInput result = null;
        if (superRate != null && gstRate != null)
        {
            result = new PaymentInput(id, employmentTypeId, dailyRate, annualIncome, rateIncludesSuper, superRate, gstRate, financialYearKey);
        }
        return result;
    }

    protected PaymentInput(String guid, int employmentTypeId, BigDecimal dailyRate, BigDecimal annualIncome, boolean rateIncludesSuper, BigDecimal superRate, BigDecimal gstRate, String financialYearKey)
    {
        id                  = guid;
        if (id == null)
        {
            id = UUID.randomUUID().toString();
        }

        version             = 1;
        employment_type_id  = employmentTypeId;
        daily_rate_entered  = dailyRate;
        annual_income       = annualIncome;
        rate_includes_super = rateIncludesSuper;
        super_rate          = superRate;
        gst_rate            = gstRate;
        financial_year_key  = financialYearKey;
        tax_schedule_scale  = 2;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String value)
    {
        this.id = value;
    }

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int value)
    {
        this.version = value;
    }

    public EmploymentType getEmploymentType()
    {
        return EmploymentType.getById(employment_type_id);
    }

    public void setEmploymentTypeId(int value)
    {
        this.employment_type_id = value;
    }

    public BigDecimal getDailyRateEntered()
    {
        return daily_rate_entered;
    }

    public void setDailyRateEntered(BigDecimal value)
    {
        this.daily_rate_entered = value;
    }

    public BigDecimal getAnnualIncome()
    {
        return annual_income != null ? annual_income : BigDecimal.ZERO;
    }

    public void setAnnualIncome(BigDecimal value)
    {
        this.annual_income = value;
    }

    public BigDecimal getSuperRate()
    {
        return super_rate;
    }

    public void setSuperRate(BigDecimal value)
    {
        this.super_rate = value;
    }

    public BigDecimal getGstRate()
    {
        return gst_rate;
    }

    public void setGstRate(BigDecimal value)
    {
        this.gst_rate = value;
    }

    public FinancialYear getFinancialYear()
    {
        return FinancialYear.getByKey(financial_year_key);
    }

    public void setFinancialYearKey(String value)
    {
        this.financial_year_key = value;
    }

    public BigDecimal getDailyRateBase()
    {
        if (this.rate_includes_super)
        {
            // REMOVE SUPER FROM Payment Income Rate
            BigDecimal superRate = BigDecimal.ONE.add(this.super_rate);
            if (daily_rate_entered != null)
            {
                return this.daily_rate_entered.divide(superRate, 4, BigDecimal.ROUND_HALF_UP);
            }
            return null;
        }
        else
        {
            return this.daily_rate_entered;
        }
    }

    public BigDecimal getDailyRatePackage()
    {
        if (this.rate_includes_super)
        {
            return this.daily_rate_entered;
        }
        else
        {
            // ADD SUPER ON TOP OF Payment Income Rate
            BigDecimal superRate = BigDecimal.ONE.add(this.super_rate);
            return Rounding.roundHalfUp(this.daily_rate_entered.multiply(superRate));
        }
    }

    public BigDecimal getAnnualBase()
    {
        if (this.rate_includes_super)
        {
            // REMOVE SUPER FROM Payment Income Rate
            BigDecimal superRate = BigDecimal.ONE.add(this.super_rate);
            return this.annual_income.divide(superRate, 2, BigDecimal.ROUND_HALF_UP);
        }
        else
        {
            return this.annual_income;
        }
    }

    public BigDecimal getAnnualPackage()
    {
        if (this.rate_includes_super)
        {
            return this.annual_income;
        }
        else
        {
            if (this.annual_income != null && this.super_rate != null)
            {
                // ADD SUPER ON TOP OF Payment Income Rate
                BigDecimal superRate = BigDecimal.ONE.add(this.super_rate);
                return Rounding.roundHalfUp(this.annual_income.multiply(superRate));
            }
            else
            {
                return BigDecimal.ZERO;
            }
        }
    }

    public boolean rateIncludesSuper()
    {
        return rate_includes_super;
    }

    public void setRateIncludesSuper(boolean value)
    {
        this.rate_includes_super = value;
    }

    public boolean isFavourite()
    {
        return favourite;
    }

    public void setFavourite(boolean value)
    {
        this.favourite = value;
    }

    /**
     * Returns Daily Rate with or without Super.<br/>
     * Value depends on AppSettings.CasualRateType
     * @param appSettings Used to inspect CasualRateType and determine With or Without Super
     * @return Daily Rate (with or without Super)
     */
    public BigDecimal getDailyRate(AppSettings appSettings)
    {
        BigDecimal result;

        switch (appSettings.getCasualRateType())
        {
            case BASE:
                result = getDailyRateBase();
                break;
            case PACKAGE:
                result = getDailyRatePackage();
                break;
            default:
                if (this.rate_includes_super)
                {
                    result = getDailyRatePackage();
                }
                else
                {
                    result = getDailyRateBase();
                }
                break;
        }

        return result;
    }

    public BigDecimal getAnnualRate(AppSettings appSettings)
    {
        BigDecimal result;

        switch (appSettings.getPermanentRateType())
        {
            case BASE:
                result = getAnnualBase();
                break;
            case PACKAGE:
                result = getAnnualPackage();
                break;
            default:
                if (this.rate_includes_super)
                {
                    result = getAnnualPackage();
                }
                else
                {
                    result = getAnnualBase();
                }
                break;
        }

        return result;
    }

    public int getTaxScheduleScale()
    {
        return tax_schedule_scale;
    }

    public boolean isValid()
    {
        Validator validator = Validator.Build();
        validator
                .zeroOrMore(this.super_rate)
                .zeroOrMore(this.gst_rate)
                .notEmptyNorValue(this.financial_year_key, FinancialYear.NONE.getKey());

        if (employment_type_id == EmploymentType.CASUAL.getId())
        {
            validator.notZero(this.daily_rate_entered);
        }
        else
        {
            validator.notZero(this.annual_income);
        }

        return validator.isValid();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        PaymentInput that = (PaymentInput) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode()
    {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString()
    {
        return id;
    }
}
