package au.com.traveller.payrun.activities;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.BigDecimal;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.dialogs.PayrunDaysDialogFragment;
import au.com.traveller.payrun.dialogs.PayrunFrequencyDialogFragment;
import au.com.traveller.payrun.dialogs.PayrunIncomeDialogFragment;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.interfaces.DialogValueListener;
import au.com.traveller.payrun.managers.PayrunManager;
import au.com.traveller.payrun.models.AppSettings;
import au.com.traveller.payrun.models.PaymentInput;
import au.com.traveller.payrun.models.PayrunInput;
import au.com.traveller.payrun.models.PayrunOutput;
import au.com.traveller.payrun.policies.DashboardPolicy;
import au.com.traveller.payrun.policies.PayrunPolicy;
import au.com.traveller.payrun.services.FabricAnswersService;
import au.com.traveller.payrun.utils.LocalStorageUtil;
import au.com.traveller.payrun.utils.ViewUtil;

public class PayrunActivity extends BaseActivity implements DialogValueListener
{
    protected String LOG_TAG = PayrunActivity.class.getSimpleName();

    public static final String PAYRUN_BUNDLE_KEY        = "payrunBundle";
    public final static String PAYMENT_INPUT_ID_KEY     = "paymentInputId";
    public static final String PAYRUN_DAYS_KEY          = "payrunDays";
    public static final String PAYRUN_FREQUENCY_KEY     = "payrunFrequency";
    public static final String PAYRUN_INCOME_KEY        = "payrunIncome";
    public final static String EMPLOYMENT_TYPE_ID_KEY   = "employmentTypeId";

    private PayrunPolicy _policy;
    private AppSettings _appSettings;

    private ViewGroup _groupPayrunDays;
    private ViewGroup _groupInvoice;

    private TextView _txtPaymentRate;
    private TextView _txtPaymentRateCaption;

    private TextView _txtCasualPayrunDays;
    private TextView _txtPayrunFrequency;
    private TextView _txtPayrunIncome;
    private TextView _txtPayrunIncomeCaption;

    private TextView _labelInvoiced;
    private TextView _labelSuperContrib;
    private TextView _labelWithoutSuper;
    private TextView _labelIncomeTax;
    private TextView _labelMedicareLevy;
    private TextView _labelTotalTax;
    private TextView _labelNetPay;

    private PaymentInput _paymentInput;
    private BigDecimal _payrunDays;
    private PaymentFrequency _payrunFrequency;
    private BigDecimal _payrunIncome;

    private TextView _labelInvoiceIncome;
    private TextView _labelGstAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payrun);

        _appSettings = LocalStorageUtil.retrieveSettings();

        this.initResources();

        this.extractIntentExtras();

        if (this.intentValuesValid())
        {
            _policy = new PayrunPolicy(_paymentInput);
            FabricAnswersService.payrunViewed(_paymentInput, _payrunFrequency);

            this.setPayrunDays(_payrunDays);

            this.initToolbar(R.string.title_activity_payrun);
            this.initButtons();
            this.showPayrunCalculations(_payrunDays);
        }
        else
        {
            AlertDialog.Builder builder = ViewUtil.buildDialogWithOptions(PayrunActivity.this, null, getString(R.string.error_contact_us));
            builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    finish();
                }
            });

            if (!isFinishing())
            {
                builder.show();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        if (_payrunFrequency != null)
        {
            outState.putString(PAYRUN_FREQUENCY_KEY, _payrunFrequency.getFrequencyName());
        }

        if (_payrunDays != null)
        {
            outState.putFloat(PAYRUN_DAYS_KEY, _payrunDays.floatValue());
        }

        if (_payrunIncome != null)
        {
            outState.putFloat(PAYRUN_INCOME_KEY, _payrunIncome.floatValue());
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        String frequencyKey = savedInstanceState.getString(PAYRUN_FREQUENCY_KEY, null);
        if (frequencyKey != null)
        {
            _payrunFrequency = PaymentFrequency.getByFrequencyName(frequencyKey);
            this.setPayrunFrequency(_payrunFrequency);
        }

        float payrunDays = savedInstanceState.getFloat(PAYRUN_DAYS_KEY, -1);
        if (payrunDays > -1)
        {
            _payrunDays = new BigDecimal(payrunDays);
            this.setPayrunDays(_payrunDays);
        }

        float payrunIncome = savedInstanceState.getFloat(PAYRUN_INCOME_KEY, -1);
        if (payrunIncome > -1)
        {
            _payrunIncome = new BigDecimal(payrunIncome);
            this.setPayrunIncome(_payrunIncome);
        }
        this.showPayrunCalculations(_payrunDays);
    }

    @Override
    protected String getTag()
    {
        return LOG_TAG;
    }

    /**
     * Extracts from Intent's BundleExtra into class fields<br/>
     * <ul>
     * <li>PaymentInput</li>
     * <li>payrunDays</li>
     * <li>payrunFrequency</li>
     * </ul>
     */
    private void extractIntentExtras()
    {
        Intent intent = getIntent();
        if (intent != null)
        {
            if (!intent.hasExtra(PayrunActivity.PAYRUN_BUNDLE_KEY))
            {
                FabricAnswersService.payrunInfo("[PayrunActivity.extractIntentExtras] Intent has no extra (Bundle) key");
            }

            Bundle extra = intent.getBundleExtra(PayrunActivity.PAYRUN_BUNDLE_KEY);
            if (extra != null)
            {
                String paymentInputId   = extra.getString(PayrunActivity.PAYMENT_INPUT_ID_KEY);
                double presetPayrunDays = extra.getDouble(PayrunActivity.PAYRUN_DAYS_KEY, -1);
                String frequencyName    = extra.getString(PayrunActivity.PAYRUN_FREQUENCY_KEY);

                FabricAnswersService.payrunInfo(String.format("[PayrunActivity.extractIntentExtras] PaymentInputId=[%s], PayrunDays=[%s], Frequency=[%s]",  paymentInputId, presetPayrunDays, frequencyName));

                if (paymentInputId != null)
                {
                    _paymentInput = LocalStorageUtil.retrieveAnnualInput(paymentInputId);
                }

                _payrunDays         = new BigDecimal(presetPayrunDays);
                _payrunFrequency    = PaymentFrequency.getByFrequencyName(frequencyName);
            }
        }
    }

    private boolean intentValuesValid()
    {
        if (_paymentInput != null)
        {
            if (_payrunDays != null && _payrunDays.compareTo(BigDecimal.ZERO) > 0)
            {
                if (_payrunFrequency != null && _payrunFrequency != PaymentFrequency.NONE)
                {
                    return true;
                }
            }
        }

        FabricAnswersService.payrunWarning(String.format("[PayrunActivity.intentValuesValid] PaymentInputId=[%s], PayrunDays=[%s], Frequency=[%s]",  _paymentInput, _payrunDays, _payrunFrequency));
        FabricAnswersService.reportException("PayrunActivity missing intent data");
        return false;
    }

    private void initResources()
    {
        _txtPaymentRate         = (TextView) findViewById(R.id.label_payrun_rate);
        _txtPaymentRateCaption  = (TextView) findViewById(R.id.label_payrun_rate_caption);

        _groupPayrunDays        = (ViewGroup) findViewById(R.id.group_payrun_days);
        _txtCasualPayrunDays    = (TextView) findViewById(R.id.label_payrun_days);
        _txtPayrunFrequency     = (TextView) findViewById(R.id.label_payrun_frequency);
        _txtPayrunIncome        = (TextView) findViewById(R.id.label_payrun_income);
        _txtPayrunIncomeCaption = (TextView) findViewById(R.id.label_payrun_income_caption);

        _labelInvoiced          = (TextView) findViewById(R.id.label_payrun_invoiced);
        _labelWithoutSuper      = (TextView) findViewById(R.id.label_payrun_without_super);

        _labelGstAmount         = (TextView) findViewById(R.id.label_payrun_gst_amount);

        _labelSuperContrib      = (TextView) findViewById(R.id.label_payrun_super_contribution);
        _labelIncomeTax         = (TextView) findViewById(R.id.label_payrun_income_tax);
        _labelMedicareLevy      = (TextView) findViewById(R.id.label_payrun_medicare_levy);
        _labelTotalTax          = (TextView) findViewById(R.id.label_payrun_total_tax);
        _labelNetPay            = (TextView) findViewById(R.id.label_payrun_net_pay);

        // INVOICE
        _groupInvoice           = (ViewGroup) findViewById(R.id.payrun_card_invoice);
        _labelInvoiceIncome     = (TextView) findViewById(R.id.label_payrun_income_no_gst);
    }

    private void initButtons()
    {
        if (_txtCasualPayrunDays != null)
        {
            this.markAsClickable(_txtCasualPayrunDays);
            _txtCasualPayrunDays.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    showPayrunDaysPicker();
                }
            });
        }

        if (_txtPayrunFrequency != null)
        {
            this.markAsClickable(_txtPayrunFrequency);
            _txtPayrunFrequency.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    showFrequencyPicker();
                }
            });
        }

        if (_policy.isCasual())
        {
            if (_txtPayrunIncome != null)
            {
                this.markAsClickable(_txtPayrunIncome);
                _txtPayrunIncome.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        showPayrunIncomeDialog();
                    }
                });
            }
        }
    }

    private void markAsClickable(View v)
    {
        if (v != null)
        {
            v.setBackgroundResource(R.drawable.spinner_default_holo_dark_am);
        }
    }

    /**
     * Recalculates (only) Casual Payrun using new <code>payrunDays</code>
     * @param payrunDays Number of days in a payrun (can be NULL)
     */
    private void setPayrunDays(@Nullable BigDecimal payrunDays)
    {
        if (_policy.isCasual())
        {
            _payrunDays = payrunDays;
            if (payrunDays != null)
            {
                BigDecimal dailyRate = DashboardPolicy.getDailyRate(_paymentInput);
                _payrunIncome = _payrunDays.multiply(dailyRate);
            }
        }
        else
        {
            _payrunIncome = _paymentInput.getAnnualRate(_appSettings);
        }
        this.populatePayrunToolbar();
    }

    private void setPayrunFrequency(PaymentFrequency frequency)
    {
        _payrunFrequency = frequency;
        FabricAnswersService.payrunFrequencyChanged();
        this.populatePayrunToolbar();
    }

    private void setPayrunIncome(BigDecimal payrunIncome)
    {
        FabricAnswersService.payrunIncomeChanged();
        _payrunIncome = payrunIncome;
        if (_policy.isCasual())
        {
            BigDecimal dailyRate = DashboardPolicy.getDailyRate(_paymentInput);
            _payrunDays = _payrunIncome.divide(dailyRate, BigDecimal.ROUND_HALF_UP);
        }
        this.populatePayrunToolbar();
    }

    private void populatePayrunToolbar()
    {
        // ROW #1 COL #1
        if (_txtPaymentRate != null)
        {
            _txtPaymentRate.setText(ViewUtil.formatBigDecimalForLabel(DashboardPolicy.getRateAmount(_paymentInput)));
        }
        if (_txtPaymentRateCaption != null)
        {
            _txtPaymentRateCaption.setText(DashboardPolicy.getRateCaption(this, _paymentInput));
        }

        // ROW #1 COL #2
        if (_txtPayrunFrequency != null)
        {
            _txtPayrunFrequency.setText(ViewUtil.formatPayrunTimeForEdit(_payrunFrequency));
        }

        // ROW #2 COL #1
        if (_txtCasualPayrunDays != null)
        {
            _txtCasualPayrunDays.setText(ViewUtil.formatPayrunDays(_payrunDays));
        }
        // ROW #2 COL #2
        if (_txtPayrunIncome != null)
        {
            _txtPayrunIncome.setText(ViewUtil.formatBigDecimalForLabel(DashboardPolicy.getPayrunIncome(_paymentInput, _payrunIncome, _payrunFrequency)));
        }
        if (_txtPayrunIncomeCaption != null)
        {
            _txtPayrunIncomeCaption.setText(DashboardPolicy.getIncomeCaption(this, _paymentInput));
        }

        this.toggleVisibleElements();
    }

    private void toggleVisibleElements()
    {
        if (_groupInvoice != null && _groupPayrunDays != null)
        {
            if (_policy.isCasual())
            {

                _groupInvoice.setVisibility(View.VISIBLE);
                _groupPayrunDays.setVisibility(View.VISIBLE);
            }
            else
            {
                _groupInvoice.setVisibility(View.GONE);
                _groupPayrunDays.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void showPayrunCalculations(BigDecimal payrunDays)
    {
        if (payrunDays == null || (payrunDays.compareTo(BigDecimal.ZERO) < 1))
        {
            return;
        }

        PayrunInput payrunInput = new PayrunInput(_paymentInput.getFinancialYear(), _payrunFrequency, _paymentInput.getSuperRate(), _paymentInput.getGstRate(), _paymentInput.getTaxScheduleScale());
        if (_paymentInput != null)
        {
            if (PayrunPolicy.useAnnualCalc(_paymentInput.getEmploymentType(), _payrunFrequency))
            {
                _paymentInput.setAnnualIncome(_payrunIncome);
            }

            boolean includesSuper = PayrunPolicy.payrunIncomeIncludesSuper(_paymentInput, _appSettings);
            if (_paymentInput.getEmploymentType() == EmploymentType.CASUAL)
            {
                payrunInput.setCasualValues(includesSuper, _payrunDays, _payrunIncome);
            }
            else
            {
                payrunInput.setPermanentValues(includesSuper, _payrunIncome);
            }

            PayrunManager pm = new PayrunManager();
            PayrunOutput po = pm.getPayrunOutput(payrunInput);
            this.bindPayrunCalculations(po);
        }
    }

    private void bindPayrunCalculations(PayrunOutput po)
    {
        Log.i(LOG_TAG, "bindPayrunCalculations");
        // SUMMARY
        _labelSuperContrib.setText(ViewUtil.formatBigDecimalForLabel(po.getSuperContribution()));
        _labelTotalTax.setText(ViewUtil.formatBigDecimalForLabel(po.getTaxTotal()));
        _labelNetPay.setText(ViewUtil.formatBigDecimalForLabel(po.getNetPay()));

        // DETAILS
        _labelWithoutSuper.setText(ViewUtil.formatBigDecimalForLabel(po.getIncomeBase()));
        _labelMedicareLevy.setText(ViewUtil.formatBigDecimalForLabel(po.getTaxMedicareLevy()));
        _labelIncomeTax.setText(ViewUtil.formatBigDecimalForLabel(po.getTaxPaygContribution()));

        // INVOICE
        _labelInvoiceIncome.setText(ViewUtil.formatBigDecimalForLabel(po.getIncomePackage()));
        _labelGstAmount.setText(ViewUtil.formatBigDecimalForLabel(po.getGstAmount()));
        _labelInvoiced.setText(ViewUtil.formatBigDecimalForLabel(po.getInvoiceAmountWithGst()));
    }

    private void showPayrunDaysPicker()
    {
        BigDecimal initialPayrunDays = ViewUtil.getValueAsBigDecimal(_txtCasualPayrunDays.getText().toString());

        int initialValue = initialPayrunDays.intValue();
        FragmentManager fm = getFragmentManager();
        PayrunDaysDialogFragment f = PayrunDaysDialogFragment.newInstance(R.string.title_payment_run_days, initialValue, this);
        f.show(fm, "pay_days_dialog_fragment");
    }

    private void showFrequencyPicker()
    {
        PaymentFrequency initialFrequency = PaymentFrequency.getByFrequencyName(_txtPayrunFrequency.getText().toString());

        FragmentManager fm = getFragmentManager();
        PayrunFrequencyDialogFragment f = PayrunFrequencyDialogFragment.newInstance(R.string.title_payment_run_frequency, initialFrequency, this);
        f.show(fm, "frequency_dialog_fragment");
    }

    private void showPayrunIncomeDialog()
    {
        BigDecimal initialValue = ViewUtil.getValueAsBigDecimal(_txtPayrunIncome.getText().toString());

        FragmentManager fm = getFragmentManager();
        PayrunIncomeDialogFragment f = PayrunIncomeDialogFragment.newInstance(R.string.label_payrun_income, initialValue, this);
        f.show(fm, "pay_income_dialog_fragment");
    }

    @Override
    public void onNewPayrunDays(BigDecimal newVal)
    {
        FabricAnswersService.payrunDaysChanged();
        this.setPayrunDays(newVal);
        this.showPayrunCalculations(_payrunDays);
    }

    @Override
    public void onNewPayrunIncome(BigDecimal newVal)
    {
        setPayrunIncome(newVal);
        this.showPayrunCalculations(_payrunDays);
    }

    @Override
    public void onNewPayrunFrequency(PaymentFrequency newValue)
    {
        setPayrunFrequency(newValue);
        this.showPayrunCalculations(_payrunDays);
    }
}