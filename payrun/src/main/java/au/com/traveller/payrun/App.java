package au.com.traveller.payrun;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;

import au.com.traveller.payrun.managers.AppLaunchManager;
import au.com.traveller.payrun.managers.impl.AppLauncherManagerImpl;
import io.fabric.sdk.android.Fabric;

public class App extends Application {
    private static Context _context;
    private final static int MODEL_VERSION = 1;

    @Override
    public void onCreate() {
        super.onCreate();

        this.initAppGlobals();

        if (!App.isDevEnv()) {
            Fabric.with(this, new Crashlytics(), new Answers());
        }

        AppLaunchManager apm = new AppLauncherManagerImpl();
        apm.runUpgrades(MODEL_VERSION);
    }

    private void initAppGlobals() {
        App._context = getApplicationContext();
    }

    public static Context getAppContext() {
        return App._context;
    }

    /**
     * Returns True if app_environment is set to "dev" (defined in app/build.gradle/buildVariant)
     *
     * @return True if "dev", False otherwise
     */
    public static boolean isDevEnv() {
        String env = getAppContext().getString(R.string.app_environment);
        return "dev".equals(env);
    }

    /**
     * Returns True if app_environment is set to "beta" (defined in app/build.gradle/buildVariant)
     *
     * @return True if "beta", False otherwise
     */
    public static boolean isBetaEnv() {
        String env = getAppContext().getString(R.string.app_environment);
        return "beta".equals(env);
    }
}
