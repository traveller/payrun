package au.com.traveller.payrun.managers.impl

import au.com.traveller.payrun.managers.AppLaunchManager
import au.com.traveller.payrun.utils.LocalStorageUtil

class AppLauncherManagerImpl : AppLaunchManager {

    override fun runUpgrades(newVersion: Int) {
        LocalStorageUtil.storeCurrentModelVersion(newVersion)
    }
}
