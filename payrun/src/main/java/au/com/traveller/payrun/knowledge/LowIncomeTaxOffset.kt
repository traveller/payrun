package au.com.traveller.payrun.knowledge

import java.math.BigDecimal

interface LowIncomeTaxOffset {

    /**
     * Calculates Low Income Tax Offset (LITO)
     *
     * @see https://www.ato.gov.au/general/new-legislation/in-detail/direct-taxes/income-tax-for-individuals/personal-income-tax-plan/
     *
     * @param taxableIncome Taxable income (after super and deductions)
     *
     * @return Offset amount (rounded to precision of 2 decimal places)
     */
    fun getAmount(taxableIncome: BigDecimal): BigDecimal
}
