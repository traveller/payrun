package au.com.traveller.payrun.adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.DashboardPageType;
import au.com.traveller.payrun.fragments.DashboardPageFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter
{
    private static final String LOG_TAG = ViewPagerAdapter.class.getSimpleName();

    private final Context mContext;
    private DashboardPageFragment _currentPageFragment;

    public ViewPagerAdapter(FragmentManager manager, Context context)
    {
        super(manager);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position)
    {
        Log.i(LOG_TAG, String.format("getItem:position=%s", position));
        Fragment result = null;
            switch (position)
            {
                case 0:
                    result = DashboardPageFragment.newInstance(DashboardPageType.FAVORITES);
                    break;

                case 1:
                    result = DashboardPageFragment.newInstance(DashboardPageType.CASUAL);
                    break;

                case 2:
                    result = DashboardPageFragment.newInstance(DashboardPageType.PERMANENT);
                    break;
            }

        return result;
    }

    @Override
    public int getCount()
    {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        String result = "";
        switch (position)
        {
            case 0:
                result = mContext.getString(R.string.dashboard_tab_favourites);
                break;

            case 1:
                result = mContext.getString(R.string.dashboard_tab_casual);
                break;

            case 2:
                result = mContext.getString(R.string.dashboard_tab_permanent);
                break;
        }

        return result;

    }

    @Override
    public int getItemPosition(Object object)
    {
        // If you want "Fragment must be always recreated"
        // override and return POSITION_NONE from getItemPosition() method
        return POSITION_NONE;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object)
    {
        if (object instanceof DashboardPageFragment)
        {
            if (_currentPageFragment != object)
            {
                _currentPageFragment = (DashboardPageFragment) object;
                _currentPageFragment.setAsPrimary();
            }
        }
        super.setPrimaryItem(container, position, object);
    }
}
