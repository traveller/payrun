package au.com.traveller.payrun.models;

/**
 * Created by sini on 5/03/16.
 */
public class Tutorials
{
    private boolean dashboardCompleted;
    private boolean newPaymentCompleted;

    public Tutorials()
    {
        this.dashboardCompleted     = false;
        this.newPaymentCompleted    = false;
    }

    public boolean isDashboardCompleted()
    {
        return dashboardCompleted;
    }

    public void setDashboardCompleted(boolean value)
    {
        this.dashboardCompleted = value;
    }

    public boolean isNewPaymentCompleted()
    {
        return newPaymentCompleted;
    }

    public void setNewPaymentCompleted(boolean value)
    {
        this.newPaymentCompleted = value;
    }
}
