package au.com.traveller.payrun.knowledge.lowIncome

import java.math.BigDecimal

interface LowIncomeThresholds {

    val offsetThresholdMin: BigDecimal

    val offsetThresholdMax: BigDecimal

    val maxTaxOffset: BigDecimal

    val taxDecreaseRate: BigDecimal
}
