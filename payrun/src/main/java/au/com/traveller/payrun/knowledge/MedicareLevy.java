package au.com.traveller.payrun.knowledge;

import java.math.BigDecimal;

public interface MedicareLevy
{
    BigDecimal getAnnualLevy(BigDecimal taxableIncome);
}
