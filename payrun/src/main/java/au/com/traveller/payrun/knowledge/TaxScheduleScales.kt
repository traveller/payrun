package au.com.traveller.payrun.knowledge

import au.com.traveller.payrun.models.TaxSchedule

interface TaxScheduleScales {

    fun getSchedule(): TaxSchedule
}
