package au.com.traveller.payrun.managers;

import au.com.traveller.payrun.knowledge.PayrunCalculator;
import au.com.traveller.payrun.managers.impl.PayrunKnowledgeManagerImpl;
import au.com.traveller.payrun.models.PayrunInput;
import au.com.traveller.payrun.models.PayrunOutput;

public class PayrunManager {
    private PayrunKnowledgeManager payrunKnowledgeManager;

    public PayrunManager() {
        payrunKnowledgeManager = new PayrunKnowledgeManagerImpl();
    }

    public PayrunOutput getPayrunOutput(PayrunInput payrunInput) {
        PayrunOutput po;

        PayrunCalculator pc = payrunKnowledgeManager.getPayrunCalculator(payrunInput.getFinancialYear(), payrunInput.getFrequency());
        po = pc.getPayrunOutput(payrunInput);
        return po;
    }
}
