package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.MedicareLevy


interface MedicareLevyRepo {
    fun getMedicareLevy(financialYear: FinancialYear): MedicareLevy
}
