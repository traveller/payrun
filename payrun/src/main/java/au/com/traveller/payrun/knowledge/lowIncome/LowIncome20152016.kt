package au.com.traveller.payrun.knowledge.lowIncome

import java.math.BigDecimal

class LowIncome20152016 : LowIncomeThresholds {

    override val offsetThresholdMin = BigDecimal(37000)

    override val offsetThresholdMax = BigDecimal(66666.99)

    override val maxTaxOffset = BigDecimal(445)

    override val taxDecreaseRate = BigDecimal(0.015)
}