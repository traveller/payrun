package au.com.traveller.payrun.tutorial;

import android.app.Activity;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.TutorialStep;
import au.com.traveller.payrun.models.Tutorials;
import au.com.traveller.payrun.utils.LocalStorageUtil;
import tourguide.tourguide.TourGuide;

public class TutorialPaymentItemRepoImpl extends TutorialRepoImpl
{
    @Override
    public boolean hasCompleted()
    {
        Tutorials tutorials = LocalStorageUtil.retrieveTutorial();
        return tutorials.isNewPaymentCompleted();
    }

    @Override
    public void markCompleted()
    {
        Tutorials tutorials = LocalStorageUtil.retrieveTutorial();
        tutorials.setNewPaymentCompleted(true);
        LocalStorageUtil.storeTutorials(tutorials);
    }

    @Override
    public TourGuide getNextStep(Activity activity, TutorialStep nextStep)
    {
        if (TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER != null)
        {
            try
            {
                TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER.cleanUp();
            }
            catch (Exception ex)
            {
                // JUST IN CASE
            }
        }

        if (nextStep != null && nextStep != TutorialStep.NONE)
        {
            switch (nextStep)
            {
                case PAYMENT_EDIT:
                    return getPaymentEditIcon(activity);
                case PAYMENT_FAVOURITE:
                    return getPaymentFavouriteIcon(activity);
                case PAYRUN_DETAILS:
                    return getPaymentDetailsIcon(activity);
                case APP_SETTINGS:
                    return getAppSettings(activity);
            }
        }

        return null;
    }

    protected TourGuide getPaymentEditIcon(Activity activity)
    {
        TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER = init(activity, R.string.tutorial_payment_edit_title, R.string.tutorial_payment_edit);
        return TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER;
    }

    protected TourGuide getPaymentFavouriteIcon(Activity activity)
    {
        TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER = init(activity, R.string.tutorial_payment_favourites_title, R.string.tutorial_payment_favourites);
        return TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER;
    }

    protected TourGuide getPaymentDetailsIcon(Activity activity)
    {
        TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER = init(activity, R.string.tutorial_payment_details_title, R.string.tutorial_payment_details);
        return TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER;
    }

    protected TourGuide getAppSettings(Activity activity)
    {
        TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER = init(activity, R.string.tutorial_app_settings_title, R.string.tutorial_app_settings);
        return TutorialPaymentItemRepoImpl.TOUR_GUIDE_HANDLER;
    }
}