package au.com.traveller.payrun.utils;

import android.content.Context;
import androidx.appcompat.app.AlertDialog;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import au.com.traveller.payrun.R;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.PaymentFrequency;

public class ViewUtil {
    public static BigDecimal getValueAsBigDecimal(String value) {
        BigDecimal result = BigDecimal.ZERO;

        if (value != null) {
            value = value.replaceAll("[^\\d.]", "");
            if (value.length() > 0) {
                result = new BigDecimal(value);
            }
        }
        result = result.setScale(2, RoundingMode.HALF_UP);

        return result;
    }

    public static EmploymentType getValueAsTaxSchedule(boolean isPermanent) {
        return isPermanent ? EmploymentType.PERMANENT : EmploymentType.CASUAL;
    }

    public static String formatBigDecimalForEdit(BigDecimal value) {
        String result = "-";
        if (value != null) {
            value = value.setScale(2, BigDecimal.ROUND_HALF_UP);
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(2);
            df.setGroupingUsed(false);

            result = df.format(value);
        }

        return result;
    }

    public static String formatBigDecimalForLabel(BigDecimal value) {
        return formatBigDecimalForLabel(value, "$");
    }

    public static String formatBigDecimalForLabel(BigDecimal value, String currency) {
        String result = "-";

        if (value != null) {
            value = value.setScale(2, BigDecimal.ROUND_HALF_UP);
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(2);
            df.setGroupingUsed(true);

            result = df.format(value);
        }

        return String.format("%s%s", currency, result);
    }

    /**
     * Formats days as String.<br/>
     * Shows decimal point and values only if BigDecimal not a round integer.<br/>
     * Examples:<br/>
     * 5.00 => "5"
     * 5.11 => "5.11"
     *
     * @param days number of days to format
     * @return Number of days as String (int or double)
     */
    public static String formatPayrunDays(BigDecimal days) {
        if (days != null) {
            if (days.compareTo(days.setScale(0, RoundingMode.HALF_UP)) == 0) {
                return String.format("%s", days.intValue());
            } else {
                return String.format("%s", Rounding.roundHalfUp(days, 2));
            }
        }
        return "";
    }

    public static String formatPayrunDaysForEdit(BigDecimal days) {
        return String.format("%s", days);
    }

    public static boolean getTaxScheduleAsBoolean(EmploymentType employmentType) {
        return employmentType == EmploymentType.PERMANENT;
    }

    public static String formatEmploymentType(EmploymentType employmentType) {
        return employmentType == EmploymentType.PERMANENT ? "Permanent" : "Casual";
    }

    public static String formatPayrunTimeForEdit(PaymentFrequency payrunFrequency) {
        return payrunFrequency.getFrequencyName();
    }

    public static String pluraliseDay(BigDecimal amount) {
        String result = "-";

        if (amount != null) {
            if (BigDecimal.ONE.compareTo(amount) == 0) {
                result = "day";
            } else {
                result = "days";
            }
        }

        return result;
    }

    public static AlertDialog buildDialog(Context context, String title, String message) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            if (title != null) {
                builder.setTitle(title);
            } else {
                builder.setTitle(R.string.app_name);
            }

            builder.setMessage(message);
            builder.setPositiveButton(R.string.btn_ok, null);

            return builder.create();
        }

        return null;
    }

    public static AlertDialog.Builder buildDialogWithOptions(Context context, String title, String message) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MainAlertDialogStyle);
            if (title != null) {
                builder.setTitle(title);
            } else {
                builder.setTitle(R.string.app_name);
            }

            builder.setMessage(message);

            return builder;
        }

        return null;
    }
}
