package au.com.traveller.payrun.models;

import java.math.BigDecimal;

import au.com.traveller.payrun.enums.PaymentRateType;
import au.com.traveller.payrun.utils.Validator;

public class AppSettings
{
    private boolean weeklyPayrunCasualVisible;
    private boolean fortnightlyPayrunCasualVisible;
    private boolean monthlyPayrunCasualVisible;
    private boolean annuallyPayrunCasualVisible;

    private BigDecimal weeklyPayrunDays;
    private BigDecimal fortnightlyPayrunDays;
    private BigDecimal monthlyPayrunDays;
    private BigDecimal annuallyPayrunDays;

    private boolean permWeeklyPayrunVisible;
    private boolean permFortnightlyPayrunVisible;
    private boolean permMonthlyPayrunVisible;
    private boolean permAnnuallyPayrunVisible;

    private int casualRateTypeId;
    private int permanentRateTypeId;

    public AppSettings()
    {
        weeklyPayrunCasualVisible       = true;
        fortnightlyPayrunCasualVisible  = true;
        monthlyPayrunCasualVisible      = false;
        annuallyPayrunCasualVisible     = false;

        weeklyPayrunDays                = new BigDecimal(5);
        fortnightlyPayrunDays           = new BigDecimal(10);
        monthlyPayrunDays               = new BigDecimal(20);
        annuallyPayrunDays              = new BigDecimal(260);

        permWeeklyPayrunVisible         = false;
        permFortnightlyPayrunVisible    = true;
        permMonthlyPayrunVisible        = true;
        permAnnuallyPayrunVisible       = false;

        casualRateTypeId                = PaymentRateType.AS_ENTERED.getId();
        permanentRateTypeId             = PaymentRateType.AS_ENTERED.getId();
    }

    public boolean isWeeklyPayrunCasualVisible()
    {
        return weeklyPayrunCasualVisible;
    }

    public void setWeeklyPayrunCasualVisible(boolean value)
    {
        this.weeklyPayrunCasualVisible = value;
    }

    public boolean isFortnightlyPayrunCasualVisible()
    {
        return fortnightlyPayrunCasualVisible;
    }

    public void setFortnightlyPayrunCasualVisible(boolean value)
    {
        this.fortnightlyPayrunCasualVisible = value;
    }

    public boolean isMonthlyPayrunCasualVisible()
    {
        return monthlyPayrunCasualVisible;
    }

    public void setMonthlyPayrunCasualVisible(boolean monthlyPayrunCasualVisible)
    {
        this.monthlyPayrunCasualVisible = monthlyPayrunCasualVisible;
    }

    public boolean isAnnuallyPayrunCasualVisible()
    {
        return annuallyPayrunCasualVisible;
    }

    public void setAnnuallyPayrunCasualVisible(boolean value)
    {
        this.annuallyPayrunCasualVisible = value;
    }

    public BigDecimal getWeeklyPayrunDays()
    {
        return weeklyPayrunDays;
    }

    public void setWeeklyPayrunDays(BigDecimal value)
    {
        this.weeklyPayrunDays = value;
    }

    public BigDecimal getFortnightlyPayrunDays()
    {
        return fortnightlyPayrunDays;
    }

    public void setFortnightlyPayrunDays(BigDecimal value)
    {
        this.fortnightlyPayrunDays = value;
    }

    public BigDecimal getMonthlyPayrunDays()
    {
        return monthlyPayrunDays;
    }

    public void setMonthlyPayrunDays(BigDecimal value)
    {
        this.monthlyPayrunDays = value;
    }

    public BigDecimal getAnnuallyPayrunDays()
    {
        return annuallyPayrunDays;
    }

    public void setAnnuallyPayrunDays(BigDecimal value)
    {
        this.annuallyPayrunDays = value;
    }

    public boolean isPermWeeklyPayrunVisible()
    {
        return permWeeklyPayrunVisible;
    }

    public void setPermWeeklyPayrunVisible(boolean value)
    {
        this.permWeeklyPayrunVisible = value;
    }

    public boolean isPermFortnightlyPayrunVisible()
    {
        return permFortnightlyPayrunVisible;
    }

    public void setPermFortnightlyPayrunVisible(boolean value)
    {
        this.permFortnightlyPayrunVisible = value;
    }

    public boolean isPermMonthlyPayrunVisible()
    {
        return permMonthlyPayrunVisible;
    }

    public void setPermMonthlyPayrunVisible(boolean value)
    {
        this.permMonthlyPayrunVisible = value;
    }

    public boolean isPermAnnuallyPayrunVisible()
    {
        return permAnnuallyPayrunVisible;
    }

    public void setPermAnnuallyPayrunVisible(boolean value)
    {
        this.permAnnuallyPayrunVisible = value;
    }

    public PaymentRateType getCasualRateType()
    {
        return PaymentRateType.getById(casualRateTypeId);
    }

    public void setCasualRateTypeId(int value)
    {
        this.casualRateTypeId = value;
    }

    public PaymentRateType getPermanentRateType()
    {
        return PaymentRateType.getById(permanentRateTypeId);
    }

    public void setPermanentRateType(int value)
    {
        this.permanentRateTypeId = value;
    }

    public boolean isValid()
    {
        Validator validator = Validator.Build();
        if (this.weeklyPayrunCasualVisible)
        {
            validator.notZero(this.weeklyPayrunDays);
        }
        if (this.fortnightlyPayrunCasualVisible)
        {
            validator.notZero(this.fortnightlyPayrunDays);
        }
        if (this.monthlyPayrunCasualVisible)
        {
            validator.notZero(this.monthlyPayrunDays);
        }

        return validator.isValid();
    }
}
