package au.com.traveller.payrun.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.traveller.payrun.R
import au.com.traveller.payrun.enums.EmploymentType
import au.com.traveller.payrun.enums.PaymentFrequency
import au.com.traveller.payrun.interfaces.DashboardItemListener
import au.com.traveller.payrun.managers.PayrunManager
import au.com.traveller.payrun.models.PaymentInput
import au.com.traveller.payrun.models.PayrunInput
import au.com.traveller.payrun.policies.DashboardPolicy
import au.com.traveller.payrun.utils.LocalStorageUtil
import au.com.traveller.payrun.utils.ViewUtil
import org.jetbrains.anko.find
import java.math.BigDecimal

class DashboardAdapter(private val _gridItems: List<PaymentInput>?, private val _dashboardItemListener: DashboardItemListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return if (this._gridItems != null) this._gridItems.size else 0
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class DashboardItemViewHolder(_root: View, val _listener: DashboardItemListener) : RecyclerView.ViewHolder(_root) {
        var tag: PaymentInput? = null

        val txtRate: TextView = _root.find<TextView>(R.id.dashboard_rate)
        val lblRateCaption: TextView = _root.findViewById(R.id.dashboard_rate_caption) as TextView
        val lblFinYearCaption: TextView = _root.findViewById(R.id.dashboard_fy_caption) as TextView
        val groupPayrunRows: ViewGroup = _root.findViewById(R.id.dashboard_payrun_rows) as ViewGroup
        val btnEdit: ImageButton = _root.findViewById(R.id.btn_edit_annual_input) as ImageButton
        val btnFavouriteUnpin: ImageButton = _root.findViewById(R.id.btn_favourite_payment_input) as ImageButton

        init {
            btnEdit.setOnClickListener { _listener.onPaymentInputEdit(tag) }
            btnFavouriteUnpin.setOnClickListener { _listener.onPaymentInputPinned(tag) }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // create a new DashboardItemViewHolder
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.part_dashboard_widget, parent, false)

        // set the view's size, margins, paddings and layout parameters
        return DashboardItemViewHolder(v, _dashboardItemListener)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        // get the menu item
        val paymentInput = getItem(position)
        val appSettings = LocalStorageUtil.retrieveSettings()

        if (viewHolder is DashboardItemViewHolder) {

            // bind values
            viewHolder.txtRate.text = ViewUtil.formatBigDecimalForLabel(DashboardPolicy.getRateAmount(paymentInput))
            viewHolder.lblRateCaption.text = DashboardPolicy.getRateCaption(viewHolder.itemView.context, paymentInput)
            viewHolder.lblFinYearCaption.text = paymentInput!!.financialYear.key

            // CLEAR ALL VIEWS FROM groupPayrunRows
            viewHolder.groupPayrunRows.removeAllViews()

            // SHORT PAYRUN (week)
            if (DashboardPolicy.showShortPreviewPayrun(appSettings, paymentInput.employmentType)) {
                viewHolder.groupPayrunRows.addView(getDashboardItemRowView(viewHolder.itemView.context, paymentInput, appSettings.weeklyPayrunDays, PaymentFrequency.WEEKLY))
            }

            // MEDIUM PAYRUN (fortnight)
            if (DashboardPolicy.showMediumPreviewPayrun(appSettings, paymentInput.employmentType)) {
                viewHolder.groupPayrunRows.addView(getDashboardItemRowView(viewHolder.itemView.context, paymentInput, appSettings.fortnightlyPayrunDays, PaymentFrequency.FORTNIGHTLY))
            }

            // LONG PAYRUN (monthly)
            if (DashboardPolicy.showLongPreviewPayrun(appSettings, paymentInput.employmentType)) {
                viewHolder.groupPayrunRows.addView(getDashboardItemRowView(viewHolder.itemView.context, paymentInput, appSettings.monthlyPayrunDays, PaymentFrequency.MONTHLY))
            }

            // VERY LONG PAYRUN (annually)
            if (DashboardPolicy.showVeryLongPreviewPayrun(appSettings, paymentInput.employmentType)) {
                viewHolder.groupPayrunRows.addView(getDashboardItemRowView(viewHolder.itemView.context, paymentInput, appSettings.annuallyPayrunDays, PaymentFrequency.ANNUALLY))
            }

            // set this so the onItemClicked handler can determine which item has been clicked
            viewHolder.tag = paymentInput

            this.initButtons(viewHolder)
        }
    }

    private fun initButtons(vh: DashboardItemViewHolder) {
        vh.btnFavouriteUnpin.setImageResource(if ((vh.tag as PaymentInput).isFavourite) R.mipmap.icon_favourite_on_128x128 else R.mipmap.icon_favourite_off_128x128)
        vh.btnFavouriteUnpin.setOnClickListener { vh._listener.onPaymentInputPinned(vh.tag) }
    }

    fun getItem(position: Int): PaymentInput? {
        if (this._gridItems != null) {
            return this._gridItems[position]
        }

        return null
    }

    private fun getDashboardItemRowView(context: Context, paymentInput: PaymentInput, payrunDays: BigDecimal, frequency: PaymentFrequency): View {
        val v = LayoutInflater.from(context).inflate(R.layout.part_dashboard_row, null, false)

        val payrunInput = PayrunInput(paymentInput.financialYear, frequency, paymentInput.superRate, paymentInput.gstRate, paymentInput.taxScheduleScale)
        if (paymentInput.employmentType == EmploymentType.CASUAL) {
            val payrunIncome = payrunDays.multiply(paymentInput.dailyRateEntered)
            payrunInput.setCasualValues(paymentInput.rateIncludesSuper(), payrunDays, payrunIncome)
        } else {
            payrunInput.setPermanentValues(paymentInput.rateIncludesSuper(), paymentInput.annualIncome)
        }

        val payrunManager = PayrunManager()
        val po = payrunManager.getPayrunOutput(payrunInput)

        val txtRowPayrunLength: TextView
        val txtRowPayrunLengthCaption: TextView
        val txtRowPayrunValue: TextView
        val btnRowShowPayrun: ImageButton

        txtRowPayrunLength = v.findViewById(R.id.dashboard_row_payrun_length) as TextView
        txtRowPayrunLengthCaption = v.findViewById(R.id.dashboard_row_payrun_length_caption) as TextView
        txtRowPayrunValue = v.findViewById(R.id.dashboard_row_payrun_value) as TextView
        btnRowShowPayrun = v.findViewById(R.id.btn_show_payrun_details) as ImageButton

        txtRowPayrunLength.text = po.payrunLengthDescription
        if (paymentInput.employmentType == EmploymentType.CASUAL) {
            txtRowPayrunLengthCaption.text = po.payrunLengthCaption
            txtRowPayrunLengthCaption.visibility = View.VISIBLE
        } else {
            txtRowPayrunLengthCaption.visibility = View.GONE
        }
        txtRowPayrunValue.text = ViewUtil.formatBigDecimalForLabel(po.netPay)

        btnRowShowPayrun.setOnClickListener { _dashboardItemListener.onPaymentInputPayrun(paymentInput, payrunDays, frequency) }

        return v
    }
}