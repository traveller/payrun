package au.com.traveller.payrun.repositories

import au.com.traveller.payrun.enums.FinancialYear
import au.com.traveller.payrun.knowledge.PaygAnnualSchedule
import au.com.traveller.payrun.knowledge.payg.PaygAnnualSchedule201415
import au.com.traveller.payrun.models.AnnualPaygSchedule
import java.math.BigDecimal

class PaygAnnualScheduleRepoImpl : PaygAnnualScheduleRepo {

    override fun getPaygSchedule(financialYear: FinancialYear): PaygAnnualSchedule {
        return when (financialYear) {
            FinancialYear.FY_2014_15,
            FinancialYear.FY_2015_16 -> PaygAnnualSchedule201415(initSchedule2014_15())
            FinancialYear.FY_2016_17 -> PaygAnnualSchedule201415(initSchedule2016_17())
            FinancialYear.FY_2017_18 -> PaygAnnualSchedule201415(initSchedule2017_18())
            FinancialYear.FY_2018_19 -> PaygAnnualSchedule201415(initSchedule2018_19())
            else -> PaygAnnualSchedule201415(initEmptyTaxSchedule())
        }
    }

    /**
     * Uses ATO for 2014/15
     *
     * @return AnnualPaygSchedule
     */
    private fun initSchedule2014_15(): AnnualPaygSchedule {
        return AnnualPaygSchedule(
                arrayOf(BigDecimal(18200), BigDecimal(37000), BigDecimal(80000), BigDecimal(180000)),
                arrayOf(BigDecimal(0.19), BigDecimal(0.325), BigDecimal(0.37), BigDecimal(0.45))
        )
    }

    /**
     * Rates changed for 2016/17 effective on 1st Oct 2016.<br></br>
     *
     * @return AnnualPaygSchedule
     */
    private fun initSchedule2016_17(): AnnualPaygSchedule {
        return AnnualPaygSchedule(
                arrayOf(BigDecimal(18200), BigDecimal(37000), BigDecimal(87000), BigDecimal(180000)),
                arrayOf(BigDecimal(0.19), BigDecimal(0.325), BigDecimal(0.37), BigDecimal(0.47))
        )
    }

    /**
     * Rates changed for 2017/18 effective on 1st Jul 2017.<br></br>
     *
     * @return AnnualPaygSchedule
     */
    private fun initSchedule2017_18(): AnnualPaygSchedule {
        return AnnualPaygSchedule(
                arrayOf(BigDecimal(18200), BigDecimal(37000), BigDecimal(87000), BigDecimal(180000)),
                arrayOf(BigDecimal(0.19), BigDecimal(0.325), BigDecimal(0.37), BigDecimal(0.45))
        )
    }

    /**
     * Rates changed for 2018/19 effective on 1st Jul 2018.<br></br>
     *
     * @return AnnualPaygSchedule
     */
    private fun initSchedule2018_19(): AnnualPaygSchedule {
        return AnnualPaygSchedule(
                arrayOf(BigDecimal(18200), BigDecimal(37000), BigDecimal(90000), BigDecimal(180000)),
                arrayOf(BigDecimal(0.19), BigDecimal(0.325), BigDecimal(0.37), BigDecimal(0.45))
        )
    }

    private fun initEmptyTaxSchedule(): AnnualPaygSchedule {
        return AnnualPaygSchedule(
                arrayOf(BigDecimal(0.00)),
                arrayOf(BigDecimal(0.00))
        )
    }
}