package au.com.traveller.payrun.utils;

import java.math.BigDecimal;

public class Rounding {
    public static BigDecimal roundHalfUp(BigDecimal value) {
        return roundHalfUp(value, 0);
    }

    public static BigDecimal roundHalfUp(BigDecimal value, int scale) {
        if (Rounding.notNull(value)) {
            return value.setScale(scale, BigDecimal.ROUND_HALF_UP);
        }
        return null;
    }

//    public static BigDecimal roundDown(BigDecimal value) {
//        if (Rounding.notNull(value)) {
//            return value.setScale(0, BigDecimal.ROUND_DOWN);
//        }
//        return null;
//    }

    public static BigDecimal floor(BigDecimal value, int scale) {
        if (Rounding.notNull(value)) {
            return value
                    .setScale(0, BigDecimal.ROUND_FLOOR)
                    .setScale(scale, BigDecimal.ROUND_FLOOR);
        }
        return null;
    }

//    public static BigDecimal roundUp(BigDecimal value) {
//        if (Rounding.notNull(value)) {
//            return value.setScale(0, BigDecimal.ROUND_UP);
//        }
//        return null;
//    }

    private static boolean notNull(BigDecimal value) {
        return value != null;
    }
}
