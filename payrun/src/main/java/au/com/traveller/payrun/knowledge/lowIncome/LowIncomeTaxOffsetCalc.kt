package au.com.traveller.payrun.knowledge.lowIncome

import au.com.traveller.payrun.knowledge.LowIncomeTaxOffset
import au.com.traveller.payrun.utils.Rounding
import java.math.BigDecimal


class LowIncomeTaxOffsetCalc(private val thresholds: LowIncomeThresholds) : LowIncomeTaxOffset {

    override fun getAmount(taxableIncome: BigDecimal): BigDecimal {
        val result = when (taxableIncome) {
            in BigDecimal.ZERO..thresholds.offsetThresholdMin -> {
                thresholds.maxTaxOffset
            }
            in thresholds.offsetThresholdMin..thresholds.offsetThresholdMax -> {
                // CALCULATE OFFSET
                val lowIncomePortion = taxableIncome.subtract(thresholds.offsetThresholdMin)
                val portionTax = lowIncomePortion.multiply(thresholds.taxDecreaseRate)
                thresholds.maxTaxOffset.subtract(portionTax)
            }
            else -> BigDecimal.ZERO
        }

        return Rounding.roundHalfUp(result, 2)
    }
}