package au.com.traveller.payrun.tutorial;

import android.app.Activity;

import au.com.traveller.payrun.enums.TutorialStep;
import tourguide.tourguide.TourGuide;

public interface TutorialRepo
{
    boolean hasCompleted();
    void markCompleted();
    TourGuide getNextStep(Activity activity, TutorialStep nextStep);
}
