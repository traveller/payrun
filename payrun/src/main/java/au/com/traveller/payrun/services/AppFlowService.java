package au.com.traveller.payrun.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.math.BigDecimal;

import au.com.traveller.payrun.activities.PaymentInputActivity;
import au.com.traveller.payrun.activities.PayrunActivity;
import au.com.traveller.payrun.enums.EmploymentType;
import au.com.traveller.payrun.enums.PaymentFrequency;
import au.com.traveller.payrun.models.PaymentInput;

public class AppFlowService
{
    public static void showPaymentInputForm(Context context, PaymentInput paymentInput, EmploymentType employmentType)
    {
        Intent intent = new Intent(context, PaymentInputActivity.class);
        if (paymentInput != null)
        {
            intent.putExtra(PayrunActivity.PAYMENT_INPUT_ID_KEY, paymentInput.getId());
        }
        else
        {
            intent.putExtra(PayrunActivity.EMPLOYMENT_TYPE_ID_KEY, employmentType.getId());
        }
        context.startActivity(intent);
    }

    public static void showPayrunDetails(Context context, PaymentInput paymentInput, BigDecimal payrunDays, PaymentFrequency frequency)
    {
        Intent intent = new Intent(context, PayrunActivity.class);
        Bundle extra = new Bundle();

        if (paymentInput != null)
        {
            extra.putString(PayrunActivity.PAYMENT_INPUT_ID_KEY, paymentInput.getId());
        }
        else
        {
            FabricAnswersService.payrunWarning("[AppFlowService] paymentInput=null");
            extra.putString(PayrunActivity.PAYMENT_INPUT_ID_KEY, null);
        }

        if (payrunDays != null)
        {
            extra.putDouble(PayrunActivity.PAYRUN_DAYS_KEY, payrunDays.doubleValue());
        }
        else
        {
            FabricAnswersService.payrunWarning("[AppFlowService] payrunDays=null");
            extra.putDouble(PayrunActivity.PAYRUN_DAYS_KEY, -1);
        }

        if (frequency != null && frequency != PaymentFrequency.NONE)
        {
            String frequencyName = frequency.getFrequencyName();
            extra.putString(PayrunActivity.PAYRUN_FREQUENCY_KEY, frequencyName);
        }
        else
        {
            FabricAnswersService.payrunWarning(String.format("[AppFlowService] frequency=%s", frequency));
            String frequencyName = PaymentFrequency.NONE.getFrequencyName();
            extra.putString(PayrunActivity.PAYRUN_FREQUENCY_KEY, frequencyName);
        }

        FabricAnswersService.payrunInfo(String.format("[AppFlowService.showPayrunDetails] PaymentInputId=[%s], PayrunDays=[%s], Frequency=[%s]",  paymentInput, payrunDays, frequency));

        intent.putExtra(PayrunActivity.PAYRUN_BUNDLE_KEY, extra);
        context.startActivity(intent);
    }
}
