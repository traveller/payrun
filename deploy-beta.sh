#!/bin/bash
# assemble beta
# upload to Fabric.Crashlytics.Beta using
#   * beta_release_notes.txt
#   * beta_distribution_emails.txt

# load bash_aliases
shopt -s expand_aliases
source ~/.bash_aliases

#./gradlew assembleBeta -x lint crashlyticsUploadDistributionBeta
gradle assembleBeta -x lint crashlyticsUploadDistributionBeta
